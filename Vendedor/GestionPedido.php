 <?php
 if(!isset($_SESSION)){
 session_start();
 }
   include '../src/conexionBD.php';
  if($_POST){
  $_SESSION['id_Cliente'] = $_POST['txt_id_cliente'];
  }

   $carrito = $_SESSION['carrito'];
   $sql_categoria = "SELECT * FROM categoria WHERE ID_CATEGORIA <>  13";
   $data_categoria = $db->query($sql_categoria);
 ?>
 <!DOCTYPE html>
 <html>
 			<head>
     			<meta charset="utf-8">
 			    <meta http-equiv="x-ua-compatible" content="ie=edge">
     			<meta name="viewport" content="width=device-width, initial-scale=1.0">
     			<title>Mary's Floreria</title>
     			<link rel="stylesheet" href="../css/foundation.css">
     			<link rel="stylesheet" href="../css/app.css" >
     			<link rel="stylesheet" href="../css/style.css" >
     			<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
 			</head>
 <body class="rg-body">
 	<div class="row fullWidth" style="height:100%">
 		<div class="columns small-12 medium-3 large-3 content-left sP">
 				<?php
 					include('menu.php');
 				?>

 		</div>
 		<div class="columns small-12 medium-9 large-9 content-right sP">
 				<div id="saludo-inicio" class="columns small-12 large-8 medium-8" style="padding:20px">
 			<span>

 			<?php

 					?>
 				</span>
 				</div>
 				<div id="saludo-inicio" class="columns small-12 large-4 medium-4" style="text-align:right; padding:20px">
 				<a id="cerrarSesion" href="../login.php">Cerrar sesion</a>
        <br><br>
        <a id="cerrarSesion" href="VerCarrito.php">
          Carrito
          <i class="material-icons">shopping_cart</i>
          <?php echo count($carrito) ?>
        </a>

 				</div>
 			<div class="rg-container" style="padding:20px;margin-top:80px">
        <h2>Categorías : </h2>
        <br>
        <?php
        while($fila = mysqli_fetch_assoc($data_categoria)){
        ?>
          <div class="columns small-12 medium-3 large-3" onclick="buscarPorCategoria(<?php echo $fila['ID_CATEGORIA']?>)">
            <img style="height:350px" src="../img/Productos/<?php echo $fila['IMG']?>" alt="<?php echo $fila['NOMBRECATEGORIA']?>" />
            <h5><?php echo $fila['NOMBRECATEGORIA']?></h5>
          </div>

          <?php
        }
          ?>

 			</div>


 		</div>

 	</div>


 <script src="../js/vendor/jquery.js"></script>
 <script src="../dist/sweetalert-dev.js"></script>
 <link rel="stylesheet" href="../dist/sweetalert.css">
 <script>
 (function($){
 $(document).ready(function(){

 $('#cssmenu li.active').addClass('open').children('ul').show();
 	$('#cssmenu li.has-sub>a').on('click', function(){
 		$(this).removeAttr('href');
 		var element = $(this).parent('li');
 		if (element.hasClass('open')) {
 			element.removeClass('open');
 			element.find('li').removeClass('open');
 			element.find('ul').slideUp(200);
 		}
 		else {
 			element.addClass('open');
 			element.children('ul').slideDown(200);
 			element.siblings('li').children('ul').slideUp(200);
 			element.siblings('li').removeClass('open');
 			element.siblings('li').find('li').removeClass('open');
 			element.siblings('li').find('ul').slideUp(200);
 		}
 	});

 });
 })(jQuery);
 function buscarPorCategoria(e){
     location.replace("GestionPedido_Categoria.php?id="+e)
 }
 </script>
 </body>
 </html>
