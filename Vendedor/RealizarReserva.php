<?php
if(!isset($_SESSION)){
session_start();
}
include '../src/conexionBD.php';
$carrito =  $_SESSION['carrito'];
// Es su id de persona
$id_Cliente =  $_SESSION['id_Cliente'];
$sql_cliente = "SELECT * FROM persona WHERE ID_PERSONA = ".$id_Cliente;
$data_cliente = $db->query($sql_cliente);
while($fila = mysqli_fetch_assoc($data_cliente)){
	$nombre = $fila['NOMBRE']. " ".$fila['APELLIDOPAT']." ".$fila['APELLIDOMAT'];
}
$sql_distritos = "SELECT ID_COSXDIS, NOMBREDISTRITO, COSTO  FROM distrito td, costo_distrito tcd WHERE td.ID_DISTRITO = tcd.ID_DISTRITO";
$data_distrito = $db->query($sql_distritos);
?>
<!DOCTYPE html>
<html>
			<head>
    			<meta charset="utf-8">
			    <meta http-equiv="x-ua-compatible" content="ie=edge">
    			<meta name="viewport" content="width=device-width, initial-scale=1.0">
    			<title>Mary's Floreria</title>
    			<link rel="stylesheet" href="../css/foundation.css">
    			<link rel="stylesheet" href="../css/app.css" >
    			<link rel="stylesheet" href="../css/style.css" >
    			<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
			</head>
<body class="rg-body">
	<div class="row fullWidth" style="height:100%">
		<div class="columns small-12 medium-3 large-3 content-left sP">
				<?php
					include('menu.php');
				?>

		</div>
		<div class="columns small-12 medium-9 large-9 content-right sP">
				<div id="saludo-inicio" class="columns small-12 large-8 medium-8" style="padding:20px">
			<span>

			<?php

					?>
				</span>
				</div>
				<div id="saludo-inicio" class="columns small-12 large-4 medium-4" style="text-align:right; padding:20px">
				<a id="cerrarSesion" href="../login.php">Cerrar sesion</a>
				</div>
			<div class="rg-container" style="padding:20px;margin-top:80px">

        <center>
          <h2>Registrar Pedidos</h2>
        </center>

          <h3>Datos del cliente: </h3>

          <h5>Nombre : </h5>
          <input type="text" name="name" value="<?php echo $nombre?>" disabled>
          <br>
          <table>
  <tr>
    <th>Nombre</th>
    <th>Cantidad</th>
    <th>Precio Unitario / con IGV</th>
    <th>Total</th>
    <th>Eliminar</th>
  </tr>



  <?php
$cont_total = 0;
  foreach ($carrito as &$value) {
    $key = array_search($value,$carrito);
    $tmp_sql = "SELECT * FROM producto WHERE ID_PRODUCTO = ".$key;
    $data_tmp = $db->query($tmp_sql);
    while($fila = mysqli_fetch_assoc($data_tmp)){
  ?>
  <tr>
    <?php
    $cont_total+= $fila['PRECIOPRODUCTO'] * $value;
    ?>
    <td style="text-align:center"><?php echo $fila['NOMBREPRODUCTO']?></td>
    <td style="text-align:center"><?php echo $value?></td>
    <td style="text-align:center"><?php echo $fila['PRECIOPRODUCTO']?></td>
    <td style="text-align:center"><?php echo $fila['PRECIOPRODUCTO'] * $value?></td>
    <td style="text-align:center"><a href="EliminarCarrito.php?id=<?php echo $key?>"><i class="material-icons">delete</i></a></td>
  </tr>
  <?php
}
}
  ?>
</table>


<div class="columns small-12 medium-6 large-6" style="float:right">
  <div class="columns small-12 medium-6 large-6">
    <p style="color:white;background-color:red;border:1px solid black;text-align:center">
      SUB-TOTAL
    </p>
    <p style="color:white;background-color:red;border:1px solid black;text-align:center">
      I.G.V
    </p>
    <p style="color:white;background-color:red;border:1px solid black;text-align:center">
      TOTAL
    </p>
  </div>
  <div class="columns small-12 medium-6 large-6">
    <p style="border:1px solid black;text-align:center;color:gray">
      <?php
        echo round($cont_total / 1.18,2);
       ?>
    </p>
    <p style="border:1px solid black;text-align:center;color:gray">
      <?php
        echo round($cont_total - ($cont_total / 1.18),2);
       ?>
    </p>
    <p style="border:1px solid black;text-align:center;color:gray">
      <?php
      echo $cont_total;
       ?>
    </p>

  </div>
<!--<a style="float:right;margin-left:10px" onclick="realizarPedido()" href="#" class="rg_btn_ver_detalle">Realizar pedido</a>
<a style="float:right;margin-left:10px"  onclick="realizarReserva()" href="#" class="rg_btn_ver_detalle">Realizar reserva</a>-->


  </div>
	<div class="clearfix"></div>
<div class="columns small-12 medium-6 large-6">
		<h5>Fecha : </h5>
		<input min="2016-10-13" type="datetime-local" name="name" value="" >
		<h5>Mensaje :</h5>
		<textarea name="name" rows="8" cols="40"></textarea>
</div>

<div class="columns small-12 medium-6 large-6">
	<h5>Datos del receptor (Delivery)</h5>
	<input type="text" name="name" value="">
	<h5>Dirección de envío</h5>
	<input type="text" name="name" value="">
	<h5>Distrito</h5>
	<select class="" name="">
		<?php
		while($fila = mysqli_fetch_assoc($data_distrito)){
		?>
		<option value="<?php echo $fila['ID_COSXDIS']?>"><?php echo $fila['NOMBREDISTRITO'] . " S/.".$fila['COSTO']?></option>
		<?php
	}
		?>
	</select>

</div>
<div class="clearfix"></div>
<a style="float:right;margin-left:10px"  onclick="realizarReserva()" href="#" class="rg_btn_ver_detalle">Confirmar reserva</a>
			</div>
    </div>


	</div>


<script src="../js/vendor/jquery.js"></script>
<script src="../dist/sweetalert-dev.js"></script>
<link rel="stylesheet" href="../dist/sweetalert.css">
<script>
(function($){
$(document).ready(function(){

$('#cssmenu li.active').addClass('open').children('ul').show();
	$('#cssmenu li.has-sub>a').on('click', function(){
		$(this).removeAttr('href');
		var element = $(this).parent('li');
		if (element.hasClass('open')) {
			element.removeClass('open');
			element.find('li').removeClass('open');
			element.find('ul').slideUp(200);
		}
		else {
			element.addClass('open');
			element.children('ul').slideDown(200);
			element.siblings('li').children('ul').slideUp(200);
			element.siblings('li').removeClass('open');
			element.siblings('li').find('li').removeClass('open');
			element.siblings('li').find('ul').slideUp(200);
		}
	});

});
})(jQuery);
</script>
<script type="text/javascript">
  function realizarPedido(){
		if(<?php echo $cont_total?> != 0){
    swal({   title: "",   text: "¿Desea confirmar pedido?",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Aceptar",   closeOnConfirm: false }, function(){   swal("", "Se generó la boleta", "success");location.replace("GenerarBoleta.php?id_cliente=<?php echo $_SESSION['id_Cliente']?>&monto=<?php echo $cont_total?>"); });
	}else{
		sweetAlert("Oops...", "Debe comprar almenos un arreglo para esta opción", "error");
	}
  }
	function realizarReserva(){
		location.replace("RealizarReserva.php");
	}


</script>
</body>
</html>
