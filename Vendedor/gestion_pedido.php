<?php
if(!isset($_SESSION)){
session_start();
}
unset($_SESSION['carrito']);
$id_Persona = $_SESSION['id_Persona'];
include '../src/conexionBD.php';
?>
<!DOCTYPE html>
<html>
			<head>
    			<meta charset="utf-8">
			    <meta http-equiv="x-ua-compatible" content="ie=edge">
    			<meta name="viewport" content="width=device-width, initial-scale=1.0">
    			<title>Mary's Floreria</title>
    			<link rel="stylesheet" href="../css/foundation.css">
    			<link rel="stylesheet" href="../css/app.css" >
    			<link rel="stylesheet" href="../css/style.css" >
    			<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
			</head>
<body class="rg-body">
	<div class="row fullWidth" style="height:100%">
		<div class="columns small-12 medium-3 large-3 content-left sP">
				<?php
					include('menu.php');
				?>

		</div>
		<div class="columns small-12 medium-9 large-9 content-right sP">
				<div id="saludo-inicio" class="columns small-12 large-8 medium-8" style="padding:20px">
			<span>

			<?php

					?>
				</span>
				</div>
				<div id="saludo-inicio" class="columns small-12 large-4 medium-4" style="text-align:right; padding:20px">
				<a id="cerrarSesion" href="../login.php">Cerrar sesion</a>
				</div>
			<div class="rg-container" style="padding:20px;margin-top:80px;">

          <h2>Buscar cliente</h2>
          <br><br>
          <center class="container">


              <h6>Ingrese DNI</h6>
              <form id="formulario" action="GestionPedido.php" method="post" onsubmit="return false;">
                <input type="text" id="txt_dni"  value="">
                <input id="txt_id_cliente" type="hidden" name="txt_id_cliente" value="">
              </form>
              <input class="rg_btn_ver_detalle" onclick="buscarCliente()" type="button" name="name" value="Buscar">
              <input class="rg_btn_ver_detalle" onclick="modalRegistrarCliente.open()"  type="button" name="name" value="Registrar Cliente">
							<input class="rg_btn_ver_detalle" onclick="modalRegistrarClienteJuridico.open()"  type="button" name="name" value="Registrar Jurídico">
          </center>

			</div>


		</div>

	</div>


	<script src="../js/vendor/jquery.js"></script>
	<script src="../js/vendor/foundation.js"></script>
	<script src="../js/vendor/foundation.min.js"></script>
	<script src="../dist/sweetalert-dev.js"></script>
	<script src="../js/stupidtable.js?dev"></script>
	<link rel="stylesheet" href="../dist/sweetalert.css">
<script>
(function($){
$(document).ready(function(){

$('#cssmenu li.active').addClass('open').children('ul').show();
	$('#cssmenu li.has-sub>a').on('click', function(){
		$(this).removeAttr('href');
		var element = $(this).parent('li');
			element.removeClass('open');
			element.find('li').removeClass('open');
      if (element.hasClass('open')) {
			element.find('ul').slideUp(200);
		}
		else {
			element.addClass('open');
			element.children('ul').slideDown(200);
			element.siblings('li').children('ul').slideUp(200);
			element.siblings('li').removeClass('open');
			element.siblings('li').find('li').removeClass('open');
			element.siblings('li').find('ul').slideUp(200);
		}
	});

});
})(jQuery);
</script>

<script>
function buscarCliente(){
  $.post('BuscarCliente.php', {txt_dni: $("#txt_dni").val()}, function(data2, textStatus, xhr) {
  json_resultado2 = JSON.parse(data2);
  if(json_resultado2.resultado == 0){
      sweetAlert("Error", "Cliente aun no se encuentra registrado", "error");
	}else{
      $("#txt_id_cliente").val(json_resultado2.resultado);
      document.getElementById("formulario").submit();
  }

});

}



</script>

<div id="modalRegistrarCliente" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
		<a onclick="modalRegistrarCliente.close()" class="close-reveal-modal" aria-label="Close">&#215;</a>
		<form id="formularioCliente"  action="RegistrarClienteNatural.php" method="post">
			<h2 id="modalTitle">Datos personales</h2>
				<div  class="row">
					<div class="columns small-12 medium-6 large-6">
								<h5>Apellido Paterno</h5>
								<input type="text" value="" id="txt_apeP" name="txt_apeP" onkeypress="return noSeaNumero(event);">
								<h5>Apellido Materno</h5>
								<input type="text"  value="" id="txt_apeM" name="txt_apeM" onkeypress="return noSeaNumero(event);">
								<h5>Nombres</h5>
								<input type="text"  value="" id="txt_nombres" name="txt_nombres" onkeypress="return noSeaNumero(event);">
					</div>
					<div class="columns small-12 medium-6 large-6">
								<h5>Tipo de documento</h5>
								<select class="rg_select_doc" name="txt_tipoDoc" id="txt_tipoDoc" >
									<option value="DNI">DNI</option>
									<option value="Pasaporte">Pasaporte</option>
								</select>
								<h5>Número Doc</h5>
								<input type="text"  value="" id="txt_Doc" name="txt_doc" onkeypress="return siSeaNumero(event);">
					</div>
				</div>
				<h2 id="modalTitle">Datos de contacto</h2>
				<div  class="row">
					<div class="columns small-12 medium-6 large-6">
						<h5>Teléfono / Celular</h5>
						<input type="text"  value="" id="txt_tel" name="txt_tel" onkeypress="return siSeaNumero(event);">
						<h5>E-Mail</h5>
						<input type="text"  value="" id="txt_email" name="txt_email">
					</div>
					<div class="columns small-12 medium-6 large-6">
						<h5>Dirección</h5>
						<input type="text"  value="" id="txt_dir" name="txt_dir">

					</div>
				</div>
				<div class="container" style="padding:30px">
					<input style="float:right;background-color:blue !important" type="button"  onclick="modalRegistrarCliente.close()" value="Cancelar" class="btn_buscar">
					<input style="float:right;margin-left:10px;margin-right:10px" type="button"  onclick="validarClienteNatural()" value="Registrar" class="btn_buscar">
				</div>
		</form>
</div>

<div id="modalRegistrarClienteJuridico" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
		<a onclick="modalRegistrarClienteJuridico.close()" class="close-reveal-modal" aria-label="Close">&#215;</a>
	<form id="formulario_juridico" class="" action="RegistrarClienteJuridico.php" method="post">

			<h2 id="modalTitle">Datos de Empresa</h2>
				<div  class="row">
					<div class="columns small-12 medium-6 large-6">
								<h5>RUC</h5>
								<input type="text"  value="" id="txt_ruc" name="txt_ruc"  onkeypress="return siSeaNumero(event);" >
								<h5>Teléfono</h5>
								<input type="text"  value="" id="txt_tel_j" name="txt_tel_j" onkeypress="return siSeaNumero(event);">
								<h5>Domicilio Fiscal</h5>
								<input type="text"  value="" id="txt_dom_fisc" name="txt_dom_fisc">
					</div>
					<div class="columns small-12 medium-6 large-6">
								<h5>Razón Social</h5>
								<input type="text"  value="" id="txt_razon_social" name="txt_razon_social">
								<h5>Email</h5>
								<input type="text"  value="" id="txt_email_j" name="txt_email_j">

					</div>
				</div>
				<h2 id="modalTitle">Datos del Representante Legal</h2>
				<div  class="row">
					<div class="columns small-12 medium-6 large-6">
						<h5>Apellido Paterno</h5>
						<input type="text"  value="" id="txt_ape_p_j" name="txt_ape_p_j">
						<h5>Apellido Materno</h5>
						<input type="text"  value="" id="txt_ape_m_j" name="txt_ape_m_j">
						<h5>Nombres</h5>
						<input type="text"  value="" id="txt_nombre_j" name="txt_nombre_j">
					</div>
					<div class="columns small-12 medium-6 large-6">
						<h5>Tipo de Documento</h5>
														<select class="rg_select_doc" name="txt_tipo_doc_j" id="txt_tipo_doc_j" >
															<option value="DNI">DNI</option>
															<option value="Pasaporte">Pasaporte</option>
														</select>
														<h5>Número Doc</h5>
														<input type="text"  value="" id="txt_doc_j" name="txt_doc_j">
					</div>
					<div class="container" style="padding:30px">
						<input style="float:right;background-color:blue !important" type="button"  onclick="modalRegistrarClienteJuridico.close()" value="Cancelar" class="btn_buscar">
						<input style="float:right;margin-left:10px;margin-right:10px" type="button"  onclick="validarClienteJuridico()" value="Registrar" class="btn_buscar">
					</div>
				</div>
		</form>
</div>



<style media="screen">

	#modalRegistrarCliente{
		height:100%;
		padding:10px;
	}
	#modalRegistrarClienteJuridico{
		height:100%;
		padding:10px;

	}
</style>

<script type="text/javascript">
$(document).foundation();
var modalRegistrarCliente = new Foundation.Reveal($('#modalRegistrarCliente'));
var modalRegistrarClienteJuridico = new Foundation.Reveal($('#modalRegistrarClienteJuridico'));
</script>

<script type="text/javascript">
function validarClienteNatural(){
	var txt_apeP = $("#txt_apeP").val();
	var txt_apeM = $("#txt_apeM").val();
	var txt_nombres = $("#txt_nombres").val();
	var txt_tipoDoc = $("#txt_tipoDoc").val();
	var txt_Doc = $("#txt_Doc").val();
	var txt_tel = $("#txt_tel").val();
	var txt_email = $("#txt_email").val();
	var txt_dir = $("#txt_dir").val();
	if(txt_apeP == ""){
		sweetAlert("Error", "Escriba apellido paterno del cliente", "error");
	}else if(txt_apeM == ""){
		sweetAlert("Error", "Escriba apellido materno del cliente", "error");
	}else if(txt_nombres == ""){
		sweetAlert("Error", "Escriba nombres materno del cliente", "error");
	}else if(txt_tel == ""){
		sweetAlert("Error", "Escriba teléfono del cliente", "error");
	}else if(txt_tel.length != 7 && txt_tel.length!= 9){
		sweetAlert("Error", "Escriba número correcto", "error");
	}else if(txt_dir == ""){
		sweetAlert("Error", "Escriba dirección del cliente", "error");
	}else if(txt_Doc == ""){
		sweetAlert("Error", "Escriba documento del cliente", "error");
	}else if(txt_tipoDoc == "DNI" && txt_Doc.length == 8  ){


		$.post('../Administrador/VerificarDocumento.php', {txt_doc: txt_Doc}, function(data2, textStatus, xhr) {
							json_resultado2 = JSON.parse(data2);
							if(json_resultado2.resultado == 1){
									sweetAlert("Error", "Documento ya se encuentra registrado", "error");
							}else{
									if(validarCorreo(txt_email)){
									swal("¡Hecho!", "Se registró el cliente", "success")
									setTimeout(function() {document.getElementById("formularioCliente").submit();},1500);
								}else{
									sweetAlert("Error", "Ingrese correo válido", "error");
								}

							}
						});

	}else if(txt_tipoDoc == "Pasaporte" && txt_Doc.length == 12  ){

		$.post('../Administrador/VerificarDocumento.php', {txt_doc: txt_Doc}, function(data2, textStatus, xhr) {
							json_resultado2 = JSON.parse(data2);
							if(json_resultado2.resultado == 1){
									sweetAlert("Error", "Documento ya se encuentra registrado", "error");
							}else{
								if(validarCorreo(txt_email)){

									swal("¡Hecho!", "Se registró el cliente", "success")
									setTimeout(function() {document.getElementById("formularioCliente").submit();},1500);

							}else{
								sweetAlert("Error", "Ingrese correo válido", "error");
							}
							}
						});

	}else{
		sweetAlert("Error", "Escriba documento válido", "error");
	}
	}



function validarClienteJuridico(){
	var txt_ruc = $("#txt_ruc").val();
	var txt_tel_j = $("#txt_tel_j").val();
	var txt_dom_fisc = $("#txt_dom_fisc").val();
	var txt_razon_social = $("#txt_razon_social").val();
	var txt_email_j = $("#txt_email_j").val();
	var txt_ape_p_j = $("#txt_ape_p_j").val();
	var txt_ape_m_j = $("#txt_ape_m_j").val();
	var txt_nombre_j = $("#txt_nombre_j").val();
	var txt_doc_j = $("#txt_doc_j").val();
	var txt_tipo_doc_j = $("#txt_tipo_doc_j").val();


	var flag = false;

	if(txt_ruc == ""){
		sweetAlert("Error", "Escriba ruc", "error");
	}else if(txt_ruc.length != 11){
		sweetAlert("Error", "Escriba ruc válido", "error");
	}else{



			$.post('VerificarRUC.php', {txt_ruc: txt_ruc}, function(data2, textStatus, xhr) {
								json_resultado2 = JSON.parse(data2);
								if(json_resultado2.resultado != 0){
										sweetAlert("Error", "RUC Ya existe ("+json_resultado2.resultado+")", "error");
								}else{


									if(txt_tel_j == ""){
										sweetAlert("Error", "Ingrese teléfono de la empresa", "error");
									}else if(txt_tel_j.length != 7 && txt_tel_j.length!= 9){
										sweetAlert("Error", "Ingrese teléfono válido", "error");
									}else if(txt_dom_fisc == ""){
										sweetAlert("Error", "Ingrese domicilio fiscal", "error");
									}else if(txt_razon_social == ""){
										sweetAlert("Error", "Ingrese razón social", "error");
									}else if(txt_email_j == ""){
										sweetAlert("Error", "Ingrese email de la empresa", "error");
									}else if(txt_ape_p_j == ""){
										sweetAlert("Error", "Ingrese apellido paterno del representante", "error");
									}else if(txt_ape_m_j == ""){
										sweetAlert("Error", "Ingrese apellido materno del representante", "error");
									}else if(txt_nombre_j == ""){
										sweetAlert("Error", "Ingrese nombre del representante", "error");
									}else if(txt_doc_j == ""){
										sweetAlert("Error", "Ingrese documento del representante", "error");
									}else if(txt_doc_j.length > 12){
										sweetAlert("Error", "Ingrese documento válido", "error");
									}else{
											swal("¡Hecho!", "Se registró el cliente jurídico", "success")
										setTimeout(function() {document.getElementById("formulario_juridico").submit();},1500);

									}
								}
							});

	}
}

</script>

<script type="text/javascript">
function siSeaNumero(e) {
k = (document.all) ? e.keyCode : e.which;
if (k==8 || k==0) return true;
patron = /\d/;
n = String.fromCharCode(k);
return patron.test(n);
}

	function noSeaNumero(e) {
k = (document.all) ? e.keyCode : e.which;
if (k==8 || k==0) return true;
patron = /\D/;
n = String.fromCharCode(k);
return patron.test(n);

}
function validarCorreo(email) {
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
}

$(function() {
    $('input').keyup(function() {
        this.value = this.value.toLocaleUpperCase();
    });
});

</script>


</body>
</html>
