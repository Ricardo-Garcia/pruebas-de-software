<?php
if(!isset($_SESSION)){
session_start();
}
$sql_prod = "SELECT * FROM producto WHERE ID_PRODUCTO = ".$_GET['id'];
include '../src/conexionBD.php';
$data_prod = $db->query($sql_prod);
while ($fila = mysqli_fetch_assoc($data_prod)) {
  $producto = array(
    'nombre'=> $fila['NOMBREPRODUCTO'],
    'precio'=> $fila['PRECIOPRODUCTO'],
    'stock'=> $fila['STOCK'],
    'descuento'=> $fila['DESCUENTO'],
    'descripcion'=> $fila['DESCRIPCION'],
    'imagen'=> $fila['IMAGEN'],
    );
}






?>
<!DOCTYPE html>
<html>
			<head>
    			<meta charset="utf-8">
			    <meta http-equiv="x-ua-compatible" content="ie=edge">
    			<meta name="viewport" content="width=device-width, initial-scale=1.0">
    			<title>Mary's Floreria</title>
    			<link rel="stylesheet" href="../css/foundation.css">
    			<link rel="stylesheet" href="../css/app.css" >
    			<link rel="stylesheet" href="../css/style.css" >
    			<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
			</head>
<body class="rg-body">
	<div class="row fullWidth" style="height:100%">
		<div class="columns small-12 medium-3 large-3 content-left sP">
				<?php
					include('menu.php');
				?>

		</div>
		<div class="columns small-12 medium-9 large-9 content-right sP">
				<div id="saludo-inicio" class="columns small-12 large-8 medium-8" style="padding:20px">
			<span>


				</span>
				</div>
				<div id="saludo-inicio" class="columns small-12 large-4 medium-4" style="text-align:right; padding:20px">
				<a id="cerrarSesion" href="../login.php">Cerrar sesion</a>
				</div>
			<div class="rg-container" style="padding:20px;margin-top:80px">

            <div class="columns small-12 medium-6 large-6">
                <h4>
                <?php echo $producto['nombre'] ?>
                </h4>
                <img style="height:320px" src="../img/Productos/<?php echo $producto['imagen']?>" alt="" />
            </div>
            <div class="columns small-12 medium-6 large-6">
              <p>
              <h6>
                <?php
                echo $producto['descripcion'];
                 ?>
               </h6>
               <h5>Contiene : </h5>
               <ul>
              <?php
                $sql_articulos = "SELECT * FROM producto_articulo tpa, producto tp
WHERE tpa.ID_PRODUCTOPADRE = 1
AND tp.ID_PRODUCTO = tpa.ID_PRODUCTOHIJO";
                $data_articulos = $db->query($sql_articulos);

                while ($fila = mysqli_fetch_assoc($data_articulos)) {
                  ?>
                  <li><?php echo $fila['CANTIDADARTICULO']." ".$fila['NOMBREPRODUCTO']?> </li>

                  <?php
                }
               ?>
             </ul>
             </p>


               <h6>Cantidad (Stock : <?php echo $producto['stock'] ?>): </h6>
               <input id="txt_cantidad" onkeypress="return siSeaNumero(event);" type="text" name="name" value="">
               <input onclick="validar()" type="button" name="name" value="Agregar al carrito" class="rg_btn_ver_detalle">
               <input onclick="regresar()" type="button" name="name" value="Regresar" class="rg_btn_ver_detalle">
            </div>


			</div>


		</div>

	</div>


<script src="../js/vendor/jquery.js"></script>
<script src="../dist/sweetalert-dev.js"></script>
<link rel="stylesheet" href="../dist/sweetalert.css">
<script type="text/javascript">
  function regresar(){
     location.replace("GestionPedido.php");
  }
</script>
<script>
(function($){
$(document).ready(function(){

$('#cssmenu li.active').addClass('open').children('ul').show();
	$('#cssmenu li.has-sub>a').on('click', function(){
		$(this).removeAttr('href');
		var element = $(this).parent('li');
		if (element.hasClass('open')) {
			element.removeClass('open');
			element.find('li').removeClass('open');
			element.find('ul').slideUp(200);
		}
		else {
			element.addClass('open');
			element.children('ul').slideDown(200);
			element.siblings('li').children('ul').slideUp(200);
			element.siblings('li').removeClass('open');
			element.siblings('li').find('li').removeClass('open');
			element.siblings('li').find('ul').slideUp(200);
		}
	});

});
})(jQuery);
</script>
<script>
  function validar(){
    var cant = $("#txt_cantidad").val();
    if(cant == ""){
      sweetAlert("Error", "Ingrese cantidad", "error");
    }else if(cant > <?php echo $producto['stock'] ?>){
      sweetAlert("Error", "Cantidad supera el stock del producto", "error");
    }else if(cant == 0){
      sweetAlert("Error", "Cantidad tiene que ser mayor a 0", "error");
    }else{
      swal("Hecho", "Se agrego el arreglo al carrito", "success");
      location.replace("AgregarCarrito.php?id=<?php echo $_GET['id'] ?>&cant="+cant);
    }
  }
  function siSeaNumero(e) {
  k = (document.all) ? e.keyCode : e.which;
  if (k==8 || k==0) return true;
  patron = /\d/;
  n = String.fromCharCode(k);
  return patron.test(n);
  }

</script>
</body>
</html>
