<?php
if(!isset($_SESSION)){
session_start();
}
include '../src/conexionBD.php';
$carrito =  $_SESSION['carrito'];
// Es su id de persona
$id_Cliente =  $_SESSION['id_Cliente'];
$sql_cliente = "SELECT * FROM persona WHERE ID_PERSONA = ".$id_Cliente;
$data_cliente = $db->query($sql_cliente);
while($fila = mysqli_fetch_assoc($data_cliente)){
	$nombre = $fila['NOMBRE']. " ".$fila['APELLIDOPAT']." ".$fila['APELLIDOMAT'];
}
$sql_distritos = "SELECT ID_COSXDIS, NOMBREDISTRITO, COSTO  FROM distrito td, costo_distrito tcd WHERE td.ID_DISTRITO = tcd.ID_DISTRITO";
$data_distrito = $db->query($sql_distritos);
unset($_SESSION['txt_fecha']);
unset($_SESSION['txt_mensaje']);
unset($_SESSION['receptor']);
unset($_SESSION['direccion_envio']);
unset($_SESSION['distrito_envio']);
unset($_SESSION['pago_total']);
?>
<!DOCTYPE html>
<html>
			<head>
    			<meta charset="utf-8">
			    <meta http-equiv="x-ua-compatible" content="ie=edge">
    			<meta name="viewport" content="width=device-width, initial-scale=1.0">
    			<title>Mary's Floreria</title>
    			<link rel="stylesheet" href="../css/foundation.css">
    			<link rel="stylesheet" href="../css/app.css" >
    			<link rel="stylesheet" href="../css/style.css" >
					<link rel="stylesheet" href="http://foundation.zurb.com/sites/docs/v/5.5.3/assets/css/docs.css" >
    			<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
			</head>
<body class="rg-body">
	<div class="row fullWidth" style="height:100%">
		<div class="columns small-12 medium-3 large-3 content-left sP">
				<?php
					include('menu.php');
				?>

		</div>
		<div class="columns small-12 medium-9 large-9 content-right sP">
				<div id="saludo-inicio" class="columns small-12 large-8 medium-8" style="padding:20px">
			<span>

			<?php

					?>
				</span>
				</div>
				<div id="saludo-inicio" class="columns small-12 large-4 medium-4" style="text-align:right; padding:20px">
				<a id="cerrarSesion" href="../login.php">Cerrar sesion</a>
				</div>
			<div class="rg-container" style="padding:20px;margin-top:80px">

        <center>
					<a style="float:right;margin-left:10px"  onclick="regresarCatalogo()" data-reveal-id="firstModal" href="#" class="rg_btn_ver_detalle">Regresar catálogo</a>
          <h2>Registrar Pedidos</h2>
        </center>

          <h3>Datos del cliente: </h3>

          <h5>Nombre : </h5>
          <input type="text" name="name" value="<?php echo $nombre?>" disabled>
          <br>
          <table  id="tabla_pedidos" class="rg-table">
  <tr>
    <th style="text-align:center">Nombre</th>
    <th style="text-align:center">Cantidad</th>
    <th style="text-align:center">Precio Unitario / con IGV</th>
    <th style="text-align:center">Total</th>
  </tr>



  <?php
$cont_total = 0;

		$llaves = array_keys($carrito);


foreach ($llaves as &$valor) {
    $tmp_sql = "SELECT * FROM producto WHERE ID_PRODUCTO = ".$valor;
    $data_tmp = $db->query($tmp_sql);
    while($fila = mysqli_fetch_assoc($data_tmp)){
  ?>
  <tr>
    <?php
    $cont_total+= $fila['PRECIOPRODUCTO'] * $carrito[$valor];
    ?>
    <td style="text-align:center"><?php echo $fila['NOMBREPRODUCTO']?></td>
    <td style="text-align:center"><?php echo $carrito[$valor]?></td>
    <td style="text-align:center"><?php echo $fila['PRECIOPRODUCTO']?></td>
    <td style="text-align:center"><?php echo $fila['PRECIOPRODUCTO'] * $carrito[$valor]?></td>
  </tr>
  <?php
}
}

  ?>
</table>





<div class="columns small-12 medium-6 large-6" style="float:right">
  <div class="columns small-12 medium-6 large-6">
    <p style="color:white;background-color:#b83135;border:1px solid black;text-align:center">
      SUB-TOTAL
    </p>
    <p style="color:white;background-color:#b83135;border:1px solid black;text-align:center">
      I.G.V
    </p>
    <p style="color:white;background-color:#b83135;border:1px solid black;text-align:center">
      TOTAL
    </p>
  </div>
  <div class="columns small-12 medium-6 large-6">
    <p style="border:1px solid black;text-align:center;color:gray">
      <?php
        echo round($cont_total / 1.18,2);
       ?>
    </p>
    <p style="border:1px solid black;text-align:center;color:gray">
      <?php
        echo round($cont_total - ($cont_total / 1.18),2);
       ?>
    </p>
    <p style="border:1px solid black;text-align:center;color:gray">
      <?php
      echo $cont_total;
       ?>
    </p>

  </div>
<p><a style="float:right;margin-left:10px" onclick="imprimir()" href="#" class="rg_btn_ver_detalle">Imprimir recibo</a></p>
<script type="text/javascript">
function imprimir(){
	var win = window.open("BoletaPago.php?id=<?php echo $_GET['id']?>", '_blank');
	win.focus();
}
</script>





  </div>


			</div>
    </div>


	</div>


<script src="../js/vendor/jquery.js"></script>
<script src="../dist/sweetalert-dev.js"></script>
<script src="http://foundation.zurb.com/sites/docs/v/5.5.3/assets/js/all.js"></script>

<link rel="stylesheet" href="../dist/sweetalert.css">
<script>
(function($){
$(document).ready(function(){

$('#cssmenu li.active').addClass('open').children('ul').show();
	$('#cssmenu li.has-sub>a').on('click', function(){
		$(this).removeAttr('href');
		var element = $(this).parent('li');
		if (element.hasClass('open')) {
			element.removeClass('open');
			element.find('li').removeClass('open');
			element.find('ul').slideUp(200);
		}
		else {
			element.addClass('open');
			element.children('ul').slideDown(200);
			element.siblings('li').children('ul').slideUp(200);
			element.siblings('li').removeClass('open');
			element.siblings('li').find('li').removeClass('open');
			element.siblings('li').find('ul').slideUp(200);
		}
	});

});
})(jQuery);
</script>
<script type="text/javascript">
function realizarPedidoAhora(){
	if(<?php echo $cont_total?> != 0){
	swal({   title: "",   text: "¿Desea confirmar pedido?",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Aceptar",   closeOnConfirm: false }, function(){   swal("", "Se generó la boleta", "success");location.replace("GenerarBoleta.php?id_cliente=<?php echo $_SESSION['id_Cliente']?>&monto=<?php echo $cont_total?>"); });
}else{
	sweetAlert("Oops...", "Debe comprar almenos un arreglo para esta opción", "error");
}
}
  function realizarPedido(){
		if(<?php echo $cont_total?> != 0){
    swal({   title: "",   text: "¿Desea confirmar pedido?",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Aceptar",   closeOnConfirm: false }, function(){   swal("", "Se generó la boleta", "success");location.replace("GenerarBoleta.php?id_cliente=<?php echo $_SESSION['id_Cliente']?>&monto=<?php echo $cont_total?>"); });
	}else{
		sweetAlert("Oops...", "Debe comprar almenos un arreglo para esta opción", "error");
	}
  }

	function prerealizarReserva(){
		if(<?php echo $cont_total?> != 0){
			$('#firstModal').foundation('reveal', 'open');
		}else{
			sweetAlert("Oops...", "Debe comprar almenos un arreglo para esta opción", "error");

		}

	}

	function regresarCatalogo(){
		location.replace("GestionPedido.php");
	}
	function realizarReserva(){
		if(<?php echo $cont_total?> != 0){
			location.replace("RealizarReserva.php");
		}else{
			sweetAlert("Oops...", "Debe comprar almenos un arreglo para esta opción", "error");
		}

	}

function aceptarReserva(){
	var txt_fecha = $("#txt_fecha").val();
	if(txt_fecha == ""){
	sweetAlert("Error", "Ingrese fecha de reserva "+txt_fecha, "error");
}else{

	document.getElementById("txt_tipo").value = "1";
	document.getElementById("txt_comodin_fecha").value = $("#txt_fecha").val();
	document.getElementById("txt_comodin_mensaje").value = $("#txt_mensaje").val();


	swal("¡Hecho!", "Se generó la reserva", "success");
	setTimeout(function() {$("#formulario").submit();},1500);

}
	}

function abrirDelivery(){
	var txt_fecha = $("#txt_fecha").val();
	if(txt_fecha == ""){
		sweetAlert("Error", "Ingrese fecha de reserva ", "error");
	}else{
	 	$('#secondModal').foundation('reveal', 'open');
 	}
}


function realizarPedido(){
	$('#secondModal').foundation('reveal', 'open');
}

function aceptarDelivery(){
	var txt_dato_receptor = $("#txt_dato_receptor").val();
	var txt_direccion_envio = $("#txt_direccion_envio").val();
	var txt_distrito = $('input[name=txt_distrito]:checked').val();

	if(txt_dato_receptor == ""){
		sweetAlert("Error", "Ingrese datos del receptor", "error");
	}else if(txt_direccion_envio == ""){
		sweetAlert("Error", "Ingrese dirección de envío", "error");
	}else if(txt_distrito == null){
		sweetAlert("Error", "Elija distrito", "error");
	}else{

		document.getElementById("txt_comodin_fecha").value = $("#txt_fecha").val();
		document.getElementById("txt_comodin_mensaje").value = $("#txt_mensaje").val();

		document.getElementById("txt_comodin_receptor").value= $("#txt_dato_receptor").val();
		document.getElementById("txt_comodin_direccion").value= $("#txt_direccion_envio").val();
		document.getElementById("txt_comodin_distrito").value= txt_distrito;


		swal("¡Hecho!", "Se generó la reserva", "success");
		setTimeout(function() {$("#formulario").submit();},1500);
	}

}
</script>





<!-- Triggers the modals -->

<form id="formulario" method="post" action="GuardarPago.php">
	<input type="hidden" name="txt_tipo" id="txt_tipo" value="">
	<input type="hidden" name="txt_fecha" id="txt_comodin_fecha" value="">
	<input type="hidden" name="txt_mensaje" id="txt_comodin_mensaje" value="">
	<input type="hidden" name="txt_dato_receptor" id="txt_comodin_receptor" value="">
	<input type="hidden" name="txt_direccion_envio" id="txt_comodin_direccion" value="">
	<input type="hidden" name="txt_distrito" id="txt_comodin_distrito" value="">


<!-- Reveal Modals begin -->
<div id="firstModal" class="reveal-modal" data-reveal aria-labelledby="firstModalTitle" aria-hidden="true" role="dialog">
  <center><h2 id="firstModalTitle">Fecha de reserva</h2></center>
  <div class="columns small-12 medium-12 large-12">
  		<h3>Fecha : </h3>
			<input id="txt_fecha" type="datetime-local">
			<h3>Mensaje : </h3>
			<textarea id="txt_mensaje" rows="8" cols="40"></textarea>
  </div>
	<div class="columns small-12 medium-3 large-3 pull-right">
	<p><a href="#" onclick="aceptarReserva()"  class="secondary button">Aceptar</a></p>
	</div>
	<div class="columns small-12 medium-3 large-3 pull-right">
	<p><a href="#" onclick="abrirDelivery()" class="secondary button">Delivery</a></p>
	</div>


  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
<div id="secondModal" class="reveal-modal" data-reveal aria-labelledby="secondModalTitle" aria-hidden="true" role="dialog">
  <h2 id="secondModalTitle"></h2>
  <div class="columns small-12 medium-12-large-12">
  	<h3>Datos del receptor : </h3>
		<input id="txt_dato_receptor" type="text" name="txt_dato_receptor" value="">
		<h3>Dirección de envío : </h3>
		<input id="txt_direccion_envio" type="text" name="" value="">
  </div>
	<center>
		<h1>Deliver</h1>
		<h3>Horario de envio de 09:00 am a 09:00 pm - Lunes a Domi</h3>
	</center>
	<div class="columns small-12 medium-6 large-6 pull-left">
		<table id="tabla-distrito" class="rg-table">
			<thead>
				<tr>
					<th>Nombre distrito</th>
					<th>Costo </th>
					<th>Elegir</th>
				</tr>
			</thead>
				<tfoot>
					<tr style="font-size:0.7em;display:none">
						<th>Nombre distrito</th>
						<th>Costo </th>
						<th>Elegir</th>
					</tr>
				</tfoot>


<tbody>
<?php
while($fila = mysqli_fetch_assoc($data_distrito)){
 ?>
<tr>
	<td><?php echo utf8_decode($fila['NOMBREDISTRITO'])?></td>
	<td><?php echo $fila['COSTO']?></td>
	<td><input type="radio" name="txt_distrito" value="<?php echo $fila['ID_COSXDIS']?>"></td>
</tr>
<?php
}
 ?>
</tbody>


</table>

	</div>
	<div class="clearfix"></div>
	<a class="rg_btn_ver_detalle" onclick="aceptarDelivery()" style="float:right" href="#">Aceptar</a>
  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>


</form>
<!-- Reveal Modals end -->
<script>
	 $(document).foundation();
 </script>
<style>
	h1,h2,h3,h4,h5,h6{
			color:#b83135 !important;
	}

	#tabla-distrito {
	    width: 100%;
	    display:block;
	}
	#tabla-distrito thead {
	    display: inline-block;
	    width: 100%;
	    height: 20px;
	}
	#tabla-distrito tbody {
	    height: 200px;
	    display: inline-block;
	    width: 100%;
	    overflow: auto;
	}

#secondModal{
	top:40px !important;
}

</style>





</body>
</html>
