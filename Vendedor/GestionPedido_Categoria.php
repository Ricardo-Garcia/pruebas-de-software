<?php
if(!isset($_SESSION)){
session_start();
}

include '../src/conexionBD.php';
if($_GET){
  $sql_searchby_cat = "SELECT * FROM categoria_producto tc, producto tp WHERE ID_CATEGORIA = ".$_GET['id']." AND tp.ID_PRODUCTO = tc.ID_PRODUCTO AND tp.TIPOPRODUCTO = 'ARREGLO' AND tp.STOCK > 0";
  $data_by_cat = $db->query($sql_searchby_cat);
}

$carrito = $_SESSION['carrito'];
$sql_categoria = "SELECT * FROM categoria WHERE ID_CATEGORIA <>  13";
$data_categoria = $db->query($sql_categoria);
?>
<!DOCTYPE html>
<html>
     <head>
         <meta charset="utf-8">
         <meta http-equiv="x-ua-compatible" content="ie=edge">
         <meta name="viewport" content="width=device-width, initial-scale=1.0">
         <title>Mary's Floreria</title>
         <link rel="stylesheet" href="../css/foundation.css">
         <link rel="stylesheet" href="../css/app.css" >
         <link rel="stylesheet" href="../css/style.css" >
         <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
     </head>
<body class="rg-body">
 <div class="row fullWidth" style="height:100%">
   <div class="columns small-12 medium-3 large-3 content-left sP">
       <?php
         include('menu.php');
       ?>

   </div>
   <div class="columns small-12 medium-9 large-9 content-right sP">
       <div id="saludo-inicio" class="columns small-12 large-8 medium-8" style="padding:20px">
     <span>

     <?php

         ?>
       </span>
       </div>
       <div id="saludo-inicio" class="columns small-12 large-4 medium-4" style="text-align:right; padding:20px">
       <a id="cerrarSesion" href="../login.php">Cerrar sesion</a>
       <br><br>
       <a id="cerrarSesion" href="VerCarrito.php">
         Carrito
         <i class="material-icons">shopping_cart</i>
         <?php echo count($carrito) ?>
       </a>

       </div>
     <div class="rg-container" style="padding:20px;margin-top:80px">
       <div class="columns small-12 medium-7 large-7">
         <h2>Búsqueda por categoría:</h2>
       </div>
       <div class="columns small-12 medium-5 large-5">
         <input type="text"  onkeypress="buscarProd(this)">
       </div>
       <div class="clearfix"></div>
       <?php
       while($fila = mysqli_fetch_assoc($data_by_cat)){
       ?>
         <center class="item columns small-12 medium-4 large-4" data-nombre="<?php echo $fila['NOMBREPRODUCTO']?>">
           <div onclick="elegirProducto(<?php echo $fila['ID_PRODUCTO']?>)">
           <img style="height:250px" src="../img/Productos/<?php echo $fila['IMAGEN']?>" alt="<?php echo $fila['NOMBRECATEGORIA']?>" />
           <h5><?php echo $fila['NOMBREPRODUCTO']?></h5>
           <h6>S/. <?php echo $fila['PRECIOPRODUCTO']?></h6>
         </div>
           <input type="button" name="name" onclick="agregarAlCarrito(<?php echo $fila['STOCK']?>,<?php echo $fila['ID_PRODUCTO']?>)" value="Agregar al carrito" class="rg_btn_ver_detalle" style="border-radius:20px">
         </center>

         <?php
       }
         ?>

     </div>
     <div class="clearfix">

     </div>
     <br><br>
     <a style="float:right;" class="rg_btn_ver_detalle" href="GestionPedido.php">Regresar</a>

   </div>

 </div>


<script src="../js/vendor/jquery.js"></script>
<script src="../dist/sweetalert-dev.js"></script>
<link rel="stylesheet" href="../dist/sweetalert.css">
<script>
(function($){
$(document).ready(function(){

$('#cssmenu li.active').addClass('open').children('ul').show();
 $('#cssmenu li.has-sub>a').on('click', function(){
   $(this).removeAttr('href');
   var element = $(this).parent('li');
   if (element.hasClass('open')) {
     element.removeClass('open');
     element.find('li').removeClass('open');
     element.find('ul').slideUp(200);
   }
   else {
     element.addClass('open');
     element.children('ul').slideDown(200);
     element.siblings('li').children('ul').slideUp(200);
     element.siblings('li').removeClass('open');
     element.siblings('li').find('li').removeClass('open');
     element.siblings('li').find('ul').slideUp(200);
   }
 });

});
})(jQuery);
function elegirProducto(e){
  location.replace("ElegirProducto.php?id="+e);
}
</script>
<script type="text/javascript">
  function agregarAlCarrito(stock,id){
      location.replace("AgregarCarrito.php?id="+id+"&cant=1");
  }
</script>
<script>
  function buscarProd(e){
    var texto = $(e).val();
    texto = texto.toUpperCase();
    if(texto == ''){
      $(".item").show();
    }else{
      $(".item").hide();
      $('*[data-nombre^='+texto+']').show();
    }
  }
</script>





</body>
</html>
