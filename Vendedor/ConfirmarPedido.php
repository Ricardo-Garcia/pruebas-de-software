<?php
if(!isset($_SESSION)){
session_start();
}
include '../src/conexionBD.php';
$carrito =  $_SESSION['carrito'];
// Es su id de persona
$id_Cliente =  $_SESSION['id_Cliente'];
$sql_cliente = "SELECT * FROM persona WHERE ID_PERSONA = ".$id_Cliente;
$data_cliente = $db->query($sql_cliente);
while($fila = mysqli_fetch_assoc($data_cliente)){
	$nombre = $fila['NOMBRE']. " ".$fila['APELLIDOPAT']." ".$fila['APELLIDOMAT'];
}
$sql_distritos = "SELECT ID_COSXDIS, NOMBREDISTRITO, COSTO  FROM distrito td, costo_distrito tcd WHERE td.ID_DISTRITO = tcd.ID_DISTRITO";
$data_distrito = $db->query($sql_distritos);
$id_distrito = $_SESSION['distrito_envio'];
if($id_distrito){
$sql_costo = "SELECT ID_COSXDIS, NOMBREDISTRITO, COSTO  FROM distrito td, costo_distrito tcd WHERE td.ID_DISTRITO = tcd.ID_DISTRITO AND tcd.ID_COSXDIS = ".$id_distrito;
$data_costo = $db->query($sql_costo);

while($fila = mysqli_fetch_assoc($data_costo)){
	$detalle = array(
		'id'=> $fila['ID_COSXDIS'],
		'nombre'=> $fila['NOMBREDISTRITO'],
		'costo'=> $fila['COSTO']
		);
}
}
?>
<!DOCTYPE html>
<html>
			<head>
    			<meta charset="utf-8">
			    <meta http-equiv="x-ua-compatible" content="ie=edge">
    			<meta name="viewport" content="width=device-width, initial-scale=1.0">
    			<title>Mary's Floreria</title>
    			<link rel="stylesheet" href="../css/foundation.css">
    			<link rel="stylesheet" href="../css/app.css" >
    			<link rel="stylesheet" href="../css/style.css" >

    			<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
			</head>
<body class="rg-body">
	<div class="row fullWidth" style="height:100%">
		<div class="columns small-12 medium-3 large-3 content-left sP">
				<?php
					include('menu.php');
				?>

		</div>
		<div class="columns small-12 medium-9 large-9 content-right sP">
				<div id="saludo-inicio" class="columns small-12 large-8 medium-8" style="padding:20px">
			<span>

			<?php

					?>
				</span>
				</div>
				<div id="saludo-inicio" class="columns small-12 large-4 medium-4" style="text-align:right; padding:20px">
				<a id="cerrarSesion" href="../login.php">Cerrar sesion</a>
				</div>
			<div class="rg-container" style="padding:20px;margin-top:80px">

        <center>
          <h2>Registrar Pedidos</h2>
        </center>

          <h3>Datos del cliente: </h3>

          <h5>Nombre : </h5>
          <input type="text" name="name" value="<?php echo $nombre?>" disabled>
          <br>

          <table class="rg-table">
  <tr>
    <th>Nombre</th>
    <th>Cantidad</th>
    <th>Precio Unitario / con IGV</th>
    <th>Total</th>
    <!--<th>Eliminar</th>-->
  </tr>



  <?php
$cont_total = 0;

$llaves = array_keys($carrito);

foreach ($llaves as &$valor) {
		$tmp_sql = "SELECT * FROM producto WHERE ID_PRODUCTO = ".$valor;
		$data_tmp = $db->query($tmp_sql);
		while($fila = mysqli_fetch_assoc($data_tmp)){
  ?>
  <tr>
    <?php
    $cont_total+= $fila['PRECIOPRODUCTO'] * $carrito[$valor];
    ?>
		<td style="text-align:center"><?php echo $fila['NOMBREPRODUCTO']?></td>
    <td style="text-align:center"><?php echo $carrito[$valor]?></td>
    <td style="text-align:center"><?php echo $fila['PRECIOPRODUCTO']?></td>
    <td style="text-align:center"><?php echo $fila['PRECIOPRODUCTO'] * $carrito[$valor]?></td>
    <!--<td style="text-align:center"><a href="EliminarCarrito.php?id=<?php //echo $key?>"><i class="material-icons">delete</i></a></td>-->
  </tr>
  <?php
}
}

  ?>
</table>


<div class="columns small-12 medium-6 large-6">
	<h2>Datos del pedido</h2>
	<h4>Fecha del pedido : </h4>
	<input disabled type="datetime-local" name="" value="<?php echo $_SESSION['txt_fecha'];?>">
	<h4>Mensaje :</h4>
	<textarea disabled name="name" rows="8" cols="40"><?php echo $_SESSION['txt_mensaje']?></textarea>
	<?php if($_SESSION['receptor']){?>
	<h4>Receptor : </h4>
	<input disabled type="text" name="name" value="<?php echo $_SESSION['receptor']?>">
	<h4>Dirección Envío : </h4>
	<input disabled type="text" name="name" value="<?php echo $_SESSION['direccion_envio'] ?>">
	<h4>Distrito : </h4>
	<input disabled type="text" name="name" value="<?php echo $detalle['nombre']?>">
	<h4>Costo : </h4>
	<input disabled type="text" name="name" value="<?php echo $detalle['costo']?>">


	<?php } ?>
</div>

<div class="columns small-12 medium-6 large-6">
  <div class="columns small-12 medium-6 large-6">
    <p style="color:white;background-color:#b83135;border:1px solid black;text-align:center">
      SUB-TOTAL
    </p>
    <p style="color:white;background-color:#b83135;border:1px solid black;text-align:center">
      I.G.V
    </p>
    <p style="color:white;background-color:#b83135;border:1px solid black;text-align:center">
      TOTAL
    </p>
  </div>
  <div class="columns small-12 medium-6 large-6">
    <p style="border:1px solid black;text-align:center;color:gray">
      <?php
        echo round($cont_total / 1.18,2);
       ?>
    </p>
    <p style="border:1px solid black;text-align:center;color:gray">
      <?php
        echo round($cont_total - ($cont_total / 1.18),2);
       ?>
    </p>
    <p style="border:1px solid black;text-align:center;color:gray">
      <?php
      echo $cont_total;
       ?>
    </p>


	<p>
		<a style="float:right;margin-left:10px"  onclick="confirmarReserva()" href="#" class="rg_btn_ver_detalle">Realizar reserva</a>
	</p>
	<p>
		<br><br><br>
		<a style="float:right;margin-left:10px"   href="VerCarrito.php" class="rg_btn_ver_detalle">Modificar reserva</a>
	</p>

  </div>
	<div class="clearfix">
	</div>

  </div>
<div class="clearfix"></div>
<?php
if($_SESSION['receptor']){
	?>
	<br>
	<h2>Costo final incluido delivery</h2>
	<table class="rg-table">
		<tr>
			<th>Cod-Pedido</th>
			<th>Sub-Total</th>
			<th>I.G.V</th>
			<th>Total</th>
		</tr>
		<tr>
			<td>Pedido</td>
			<td>S/. <?php echo round($cont_total / 1.18,2); ?> </td>
			<td>S/. <?php echo round($cont_total - ($cont_total / 1.18),2); ?> </td>
			<td>S/. <?php echo $cont_total; ?> </td>
		</tr>
		<tr>
			<td>Delivery</td>
			<td>S/. <?php echo round($detalle['costo'] / 1.18,2);?></td>
			<td>S/. <?php echo round($detalle['costo'] - ($detalle['costo'] / 1.18),2); ?> </td>
			<td>S/. <?php echo $detalle['costo'] ?></td>
		</tr>
		<tr>
			<td style="text-align:right !important" colspan="3"><strong>Monto total</strong></td>
			<td style="font-weight:bold"><?php echo $cont_total + $detalle['costo']?></td>
		</tr>
	</table>
	<?
}

$_SESSION['pago_total'] = $cont_total + $detalle['costo'];

 ?>






			</div>
    </div>


	</div>


<script src="../js/vendor/jquery.js"></script>
<script src="../dist/sweetalert-dev.js"></script>
<script src="http://foundation.zurb.com/sites/docs/v/5.5.3/assets/js/all.js"></script>
<link rel="stylesheet" href="../dist/sweetalert.css">
<script>
(function($){
$(document).ready(function(){

$('#cssmenu li.active').addClass('open').children('ul').show();
	$('#cssmenu li.has-sub>a').on('click', function(){
		$(this).removeAttr('href');
		var element = $(this).parent('li');
		if (element.hasClass('open')) {
			element.removeClass('open');
			element.find('li').removeClass('open');
			element.find('ul').slideUp(200);
		}
		else {
			element.addClass('open');
			element.children('ul').slideDown(200);
			ele	ment.siblings('li').children('ul').slideUp(200);
			element.siblings('li').removeClass('open');
			element.siblings('li').find('li').removeClass('open');
			element.siblings('li').find('ul').slideUp(200);
		}
	});

});
})(jQuery);
</script>
<script type="text/javascript">
  function realizarPedido(){
		if(<?php echo $cont_total?> != 0){
    swal({   title: "",   text: "¿Desea confirmar pedido?",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Aceptar",   closeOnConfirm: false }, function(){   swal("", "Se generó la boleta", "success");location.replace("GenerarBoleta.php?id_cliente=<?php echo $_SESSION['id_Cliente']?>&monto=<?php echo $cont_total?>"); });
	}else{
		sweetAlert("Oops...", "Debe comprar almenos un arreglo para esta opción", "error");
	}
  }
	function realizarReserva(){
		if(<?php echo $cont_total?> != 0){
			location.replace("RealizarReserva.php");
		}else{
			sweetAlert("Oops...", "Debe comprar almenos un arreglo para esta opción", "error");
		}

	}

function aceptarReserva(){
	var txt_fecha = $("#txt_fecha").val();
	if(txt_fecha == ""){
	sweetAlert("Error", "Ingrese fecha de reserva "+txt_fecha, "error");
}else{
	swal("", "Se generó la reserva", "success");
	document.getElementById("txt_tipo").value = "1";
	$("#formulario").submit();
}
	}

function confirmarReserva(){
	swal({   title: "Realizará la reserva",   text: "¿Está seguro?",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Confirmar",   closeOnConfirm: false }, function(){
		swal("Hecho", "Se realizó la reserva", "success");
		setTimeout(function() {
			location.replace("CompraRealizada.php");
		},1500);

	});
}

function aceptarDelivery(){
}
</script>






<!-- Reveal Modals end -->
<script>
	 $(document).foundation();
 </script>
<style>



table tr,td,th{
	text-align: center !important;
}

</style>





</body>
</html>
