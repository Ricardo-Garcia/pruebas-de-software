<?php
if(!isset($_SESSION)){
session_start();
}
include '../src/conexionBD.php';

$id_Cliente = $_SESSION['id_Cliente'];


$txt_fecha = $_SESSION['txt_fecha'];
$txt_mensaje = $_SESSION['txt_mensaje'];
$txt_receptor = $_SESSION['receptor'];
$txt_direccion_envio = $_SESSION['direccion_envio'];
$txt_distrito_envio = $_SESSION['distrito_envio'];
$monto_total = $_SESSION['pago_total'];
$igv = round($monto_total / 1.18,2);
$sub_total = $monto_total - $igv;



$txt_fecha_entrega = date("Y-m-d H:i:s", strtotime($txt_fecha));

if($txt_distrito_envio){
  $sql = "INSERT INTO pedido (`ID_CLIENTE`, `ID_ESTADOPEDIDO`, `FECHAEMISION`, `FECHAENTREGA`, `SUBTOTAL`, `IGV`, `MONTOTOTAL`, `DIRECCIONENTREGA`, `DESCRIPCIONNOTA`, `ID_COSXDIS`, `ESTADO`, `MENSAJE_DISC`)
   VALUES ('".$id_Cliente."', '2', (SELECT DATE_FORMAT(sysdate(),'%Y-%m-%d %H:%i:%s') FROM DUAL), '".$txt_fecha_entrega."', '".$sub_total."', '".$igv."', '".$monto_total."', '".$txt_direccion_envio."', 'PRUEBA DE NOTA', '".$txt_distrito_envio."', '2', '".$txt_mensaje."')";
}else{
  $sql = "INSERT INTO pedido (`ID_CLIENTE`, `ID_ESTADOPEDIDO`, `FECHAEMISION`, `FECHAENTREGA`, `SUBTOTAL`, `IGV`, `MONTOTOTAL`, `DIRECCIONENTREGA`, `DESCRIPCIONNOTA`, `ESTADO`, `MENSAJE_DISC`)
   VALUES ('".$id_Cliente."', '2', (SELECT DATE_FORMAT(sysdate(),'%Y-%m-%d %H:%i:%s') FROM DUAL), '".$txt_fecha_entrega."', '".$sub_total."', '".$igv."', '".$monto_total."', '".$txt_direccion_envio."', 'PRUEBA DE NOTA', '2', '".$txt_mensaje."')";
}

$db->query($sql);

 $sql_max_persrona = "SELECT MAX(ID_PEDIDO) as max FROM pedido";
 $data_max = $db->query($sql_max_persrona);
 $max_pedido = 0;
 while($fila = mysqli_fetch_assoc($data_max)){
  $max_pedido = $fila['max'];
 }




 $carrito =  $_SESSION['carrito'];

$llaves = array_keys($carrito);
  foreach ($llaves as &$valor) {
    $key = array_search($value,$carrito);
    $tmp_sql = "SELECT * FROM producto WHERE ID_PRODUCTO = ".$valor;
    $data_tmp = $db->query($tmp_sql);
    while($fila = mysqli_fetch_assoc($data_tmp)){
         $sql_tmp = "INSERT INTO detalle_pedido (`ID_PEDIDO`, `ID_PRODUCTO`, `CANTIDAD`, `PRECIOTOTALPROD`) VALUES ('".$max_pedido."', '".$valor."', '".$carrito[$valor]."', '".$value * $fila['PRECIOPRODUCTO'] ."');";

         $db->query($sql_tmp);

         $sql_actual_stock = "SELECT STOCK FROM producto WHERE ID_PRODUCTO = ".$valor;
         $data_actual = $db->query($sql_actual_stock);
         $actual_stock = 0;
         while($fila2 = mysqli_fetch_assoc($data_actual)){
           $actual_stock = $fila2['STOCK'];
         }
         $modificar = $actual_stock - $carrito[$valor];
         $sql_alter = "UPDATE `producto` SET `STOCK`='".$modificar."' WHERE `ID_PRODUCTO`='".$valor."';";
         $db->query($sql_alter);
    }
  }




 $sql_comprobante_num = "SELECT LPAD(MAX(NUMCOMP)+1,6,'0') as num FROM comprobante";
 $data_comprobante_num = $db->query($sql_comprobante_num);
 while($fila4 = mysqli_fetch_assoc($data_comprobante_num)){
   $comprobante = $fila4['num'];
 }

 $now = new DateTime();
 $ahorita =  $now->format('Y-m-d H:i:s');
 $sql_comprobante = "INSERT INTO comprobante (`TIPOCOMPROBANTE`, `FECHACOMPROBANTE`, `ID_PEDIDO`, `SERIECOMP`, `NUMCOMP`) VALUES ('BOLETA', '.$ahorita.', '".$max_pedido."', 'B001', '".$comprobante."')";
 $db->query($sql_comprobante);

 $url = "PagoRealizado.php";
 header('Location: '.$url);

?>
