<?php
if(!isset($_SESSION)){
session_start();
}
$id_Persona = $_SESSION['id_Persona'];
include '../src/conexionBD.php';
$sql = "SELECT * FROM producto";
if($_POST){
  $sql = "SELECT * FROM categoria_producto tcp , producto tp
WHERE tcp.ID_CATEGORIA = ".$_POST['txt_categoria']."
AND tcp.ID_PRODUCTO = tp.ID_PRODUCTO";
}
$data = $db->query($sql);
$sql_categoria = "SELECT * FROM categoria";

$data_categoria = $db->query($sql_categoria);
?>

<!DOCTYPE html>
<html>
			<head>
    			<meta charset="utf-8">
			    <meta http-equiv="x-ua-compatible" content="ie=edge">
    			<meta name="viewport" content="width=device-width, initial-scale=1.0">
    			<title>Mary's Floreria</title>
    			<link rel="stylesheet" href="../css/foundation.css">
    			<link rel="stylesheet" href="../css/app.css" >
    			<link rel="stylesheet" href="../css/style.css" >
    			<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      			rel="stylesheet">
			</head>
<body class="rg-body">



	<div class="row fullWidth" style="height:100%">
		<div class="columns small-12 medium-3 large-3 content-left sP">
				<?php
					include('menu.php');
				?>

		</div>
		<div class="columns small-12 medium-9 large-9 content-right sP">
				<div id="saludo-inicio" class="columns small-12 large-8 medium-8" style="padding:20px">
			<span>

			<?php
				// Persona
					?>
				</span>
				</div>
				<div id="saludo-inicio" class="columns small-12 large-4 medium-4" style="text-align:right; padding:20px">
				<a id="cerrarSesion" href="../logout.php">Cerrar sesion</a>
				</div>
			<div class="rg-container" style="padding:20px;margin-top:80px">
				<div class="columns small-12 medium-4 large-4">
					<h4>Nombre de producto : </h4>
				</div>

				<div class="columns small-12 medium-4 large-4">
					<input id="btn_buscar" type="text" onkeypress="buscarProductoEnter(event)">
				</div>
				<div class="columns small-12 medium-2 large-2">
					<input onclick="buscarProducto()" class="rg-btn-search" type="button" name="name" value="Buscar">
				</div>
        <div class="clearfix">

        </div>
        <div class="columns small-12 medium-4 large-4">
      <h4>Categoría</h4>
        </div>
        <form class="" action="#" method="post">


        <div class="columns small-12 medium-4 large-4">
          <select class="" name="txt_categoria">
            <?php while($fila = mysqli_fetch_assoc($data_categoria)){  ?>
            <option <?php if($_POST['txt_categoria'] == $fila['ID_CATEGORIA']){ echo 'selected'; }?> value="<?php echo $fila['ID_CATEGORIA']?>"><?php echo $fila['NOMBRECATEGORIA']?></option>
            <?php } ?>
          </select>
        </div>
        <div class="columns small-12 medium-4 large-4">
            	<input  class="rg-btn-search" type="submit" name="name" value="Buscar por categoria">
        </div>
        </form>
				<h3><?php
				if($_GET){
					if($_GET['msg'] == 1){
						echo 'Se eliminó producto.';
					}else if($_GET['msg'] == 2){
						echo 'Se actualizó el producto';
					}
				}
				?></h3>

<table class="rg-table">
  <tr>
    <th>Nombre</th>
    <th>Precio</th>
    <th>Tipo de producto</th>
  <!--  <th>Imagen</th> -->
   <th>Stock</th>
    <th>Descripción</th>
  </tr>
  <?php
  while($fila = mysqli_fetch_assoc($data)){
  ?>
  	<tr data-id="<?php echo $fila['ID_PRODUCTO']?>" data-tipo="<?php echo $fila['TIPOPRODUCTO']?>" <?php if($fila['ESTADO'] === 'I'){?> style="background-color:#af2124;color:white" data-estado="I" <?php }else{ ?> data-estado="A" <?php }?> data-nombre="<?php echo utf8_encode($fila['NOMBREPRODUCTO'])?>">
    	<td><?php echo utf8_encode($fila['NOMBREPRODUCTO'])?></td>
    	<td><?php echo utf8_encode($fila['PRECIOPRODUCTO'])?></td>
    	<td><?php echo utf8_encode($fila['TIPOPRODUCTO'])?></td>
    	<!--<td><img src="../img/Productos/<?php //echo utf8_encode($fila['IMAGEN'])?>"></td>-->
     <td><?php echo utf8_encode($fila['STOCK'])?></td> 
    	<td><?php echo utf8_encode($fila['DESCRIPCION'])?></td>
  </tr>
  <?php
	}
  ?>
</table>


<!--
<div class="row">
	<div class="columns small-3 medium-3 large-3 ">
			<input class="rg-btn-primary" type="button" name="name" value="Registrar Producto" onclick="registrarProducto()">
	</div>
	<div class="columns small-3 medium-3 large-3 ">
			<input class="rg-btn-primary" type="button" name="name" value="Actualizar Producto" onclick="actualizarProducto()">
	</div>
	<div class="columns small-3 medium-3 large-3 ">
			<input class="rg-btn-primary" type="button" name="name" value="Bloquear Producto" onclick="bloquearProducto()">
	</div>
	<div class="columns small-3 medium-3 large-3 ">
			<input class="rg-btn-primary" type="button" name="name" value="Habilitar Producto" onclick="habilitarProducto()">
	</div>


</div>
-->

			</div>





		</div>
<!--
		<div class="columns small-3 medium-3 large-3 " style="float: right !important;position: fixed;right: -12%;bottom: 0;">
				<input class="rg-btn-primary" type="button" name="name" value="Subir" onclick="subir()">
		</div>
-->

	</div>




<script src="../js/vendor/jquery.js"></script>
<script src="../dist/sweetalert-dev.js"></script>
<link rel="stylesheet" href="../dist/sweetalert.css">
<script type="text/javascript">
	var cant_prod = $("tr").size()-1;

function subir(){
	$('html,body').scrollTop(0);
}
	function actualizarProducto(){
		var value = $("input[name=id_producto]:checked").val()

		if(value == null){
			sweetAlert("Error", "Elija el producto que desee actualizar", "error");
		}else{


			var tipo = $('*[data-id="'+value+'"]').data("tipo");
			window.location.replace('editar_'+tipo.toLowerCase()+'.php?id='+value);
		}
	}


	function habilitarProducto(){
		var value = $("input[name=id_producto]:checked").val()
		if(value == null){
			sweetAlert("Error", "Elija el producto que desee habilitar", "error");
		}else{
			var cont = 0;
			while(cont <= cant_prod){
				var id = $("tr").eq(cont).data('id');
				if(id == value){
					var nom = $("tr").eq(cont).data('nombre');
					var est = $("tr").eq(cont).data('estado');
				}
				cont++;
			}


			if(est == 'I'){
				swal({title: '¿Esta seguro?',   text: 'Habilitará el producto : '+nom,   type: 'warning',   showCancelButton: true, cancelButtonText: 'Cancelar',   confirmButtonColor: '#009688',   confirmButtonText: 'Habilitar',   closeOnConfirm: false }, function(){   swal('Hecho!', 'Se habilitó el producto', 'success');
					setTimeout(function() {window.location.replace('HabilitarProducto.php?id='+value);},1500);});
			}else{
				sweetAlert("Error", "Este producto ya se encuentra habilitado", "error");
			}

		}

	}

	function registrarProducto(){
		window.location = "registrar_producto.php";
	}
	function bloquearProducto(){
		var value = $("input[name=id_producto]:checked").val()
		if(value == null){
			sweetAlert("Error", "Elija el producto que desee bloquear", "error");
		}else{
			var cont = 0;
			while(cont <= cant_prod){
				var id = $("tr").eq(cont).data('id');
				if(id == value){
					var nom = $("tr").eq(cont).data('nombre');
					var est = $("tr").eq(cont).data('estado');
				}
				cont++;
			}
			if(est == 'A'){
			swal({title: '¿Esta seguro?',   text: 'Bloqueará el producto : '+nom,   type: 'warning',   showCancelButton: true, cancelButtonText: 'Cancelar',   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Bloquear',   closeOnConfirm: false }, function(){   swal('Hecho!', 'Se bloqueó el producto', 'success');
				setTimeout(function() {window.location.replace('BloquearProducto.php?id='+value);},1500);});
			}else{
				sweetAlert("Error", "Este producto ya se encuentra bloqueado", "error");
			}
				/**
				-- Metodo de modificación del producto value --
				**/
			}
	}
	function buscarProductoEnter(r){
		if (r.keyCode == 13) {
        buscarProducto();
    }
	}
	function buscarProducto(){
		var cont = 0;
		var ning = 0;
		var prod = $("#btn_buscar").val().toUpperCase();
		if(prod == ''){
		sweetAlert("Error", "Ingresa nombre del producto ", "error");
		}else{
			while(cont<cant_prod){
				if($(".rg-table tr").eq(++cont).data("nombre").toUpperCase().includes(prod)){
						$(".rg-table tr").eq(cont).show();
				}else{
						$(".rg-table tr").eq(cont).hide();
						ning++;
				}
			}
		}
		if(ning == cont){
			sweetAlert("Error", "Su búsqueda no coincide con ningún producto", "error");
			$(".rg-table tr").show();
		}
	}
</script>
<script>
(function($){
$(document).ready(function(){

$('#cssmenu li.active').addClass('open').children('ul').show();
	$('#cssmenu li.has-sub>a').on('click', function(){
		$(this).removeAttr('href');
		var element = $(this).parent('li');
		if (element.hasClass('open')) {
			element.removeClass('open');
			element.find('li').removeClass('open');
			element.find('ul').slideUp(200);
		}
		else {
			element.addClass('open');
			element.children('ul').slideDown(200);
			element.siblings('li').children('ul').slideUp(200);
			element.siblings('li').removeClass('open');
			element.siblings('li').find('li').removeClass('open');
			element.siblings('li').find('ul').slideUp(200);
		}
	});

});
})(jQuery);
</script>



</body>
</html>
