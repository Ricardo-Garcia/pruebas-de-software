<?php
if(!isset($_SESSION)){
session_start();
}
$id_Persona = $_SESSION['id_Persona'];
include '../src/conexionBD.php';
$sql = "SELECT * FROM
pedido tp , estado_pedido tep, cliente tc, persona tper
WHERE tp.ID_ESTADOPEDIDO = tep.ID_ESTADOPEDIDO
AND tp.ID_CLIENTE = tc.ID_CLIENTE
AND tper.ID_PERSONA = tc.ID_PERSONA";
$data = $db->query($sql);
?>
<!DOCTYPE html>
<html>
			<head>
    			<meta charset="utf-8">
			    <meta http-equiv="x-ua-compatible" content="ie=edge">
    			<meta name="viewport" content="width=device-width, initial-scale=1.0">
    			<title>Mary's Floreria</title>
    			<link rel="stylesheet" href="../css/foundation.css">
    			<link rel="stylesheet" href="../css/app.css" >
    			<link rel="stylesheet" href="../css/style.css" >
					<link rel="stylesheet" href="http://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" >

    			<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
			</head>
<body class="rg-body">
	<div class="row fullWidth" style="height:100%">
		<div class="columns small-12 medium-3 large-3 content-left sP">
				<?php
					include('menu.php');
				?>

		</div>
		<div class="columns small-12 medium-9 large-9 content-right sP">
				<div id="saludo-inicio" class="columns small-12 large-8 medium-8" style="padding:20px">
			<span>
				</span>
				</div>
				<div id="saludo-inicio" class="columns small-12 large-4 medium-4" style="text-align:right; padding:20px">
				<a id="cerrarSesion" href="../login.php">Cerrar sesion</a>
				</div>
			<div class="rg-container" style="padding:20px;margin-top:80px">

        <h2>Reporte de Ventas</h2>
        <?php
        if(!$_POST){
         ?>
        <form class="" action="detalle_reporte.php" method="post">
          <div class="columns small-12 medium-6 large-6">
            <h4>Fecha de Inicio</h4>
            <input id="txt_inicio" type="date" name="txt_inicio" value="">
          </div>
          <div class="columns small-12 medium-6 large-6">
            <h4>Fecha de Fin</h4>
            <input id="txt_fin" type="date" name="txt_fin" value="">
          </div>
					<div class="clearfix"></div>
					<div class="columns small-12 medium-4 large-4">
						<h6>Por nombres y apellidos (cliente)</h6>
						<input type="text" name="txt_nombre_cliente" value="">
					</div>
					<div class="columns small-12 medium-4 large-4">
						<h6>TIPO DE COMPROBANTE</h6>
						<select class="" name="txt_tipo_comprobante">
						  <option value="1">TODOS</option>
							<option value="2">Boleta</option>
							<option value="3">Factura</option>
						</select>
					</div>
					<div class="columns small-12 medium-4 large-4">
						<h6>ESTADO DE DOCUMENTO</h6>
						<select class="" name="txt_estado_documento">
						  <option value="1">TODOS</option>
							<option value="2">no se q otro hay</option>
							<option value="3">...</option>
						</select>
					</div>
					<div class="clearfix"></div>
          <input type="button" class="rg-btn-primary" name="name" value="Buscar" onclick="validar()" style="float:left;">
        </form>
        <?php
      }else{
        $txt_inicio = $_POST['txt_inicio'];
        $txt_fin = $_POST['txt_fin'];

         ?>
         <center>
             <h2>Reporte de Ventas</h2>

             <div class="columns small-12 medium-6 large-6">
               <h4>Desde : <?php echo $txt_inicio ?></h4>
             </div>

             <div class="columns small-12 medium-6 large-6">
               <h4>Hasta : <?php echo $txt_inicio ?></h4>
             </div>
         </center>

         <div class="columns small-12 large-6 medium-6">
           <div id="container" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
         </div>
         <div class="columns small-12 large-6 medium-6">

         </div>


<?php }  ?>

			</div>


		</div>

	</div>
<script type="text/javascript">
  function validar(){
    var txt_inicio = $("#txt_inicio").val();
    var txt_fin = $("#txt_fin").val();
    if(txt_inicio == ""){
      sweetAlert("Error", "Debe ingresar fecha de Inicio", "error");
    }else if(txt_fin == ""){
      sweetAlert("Error", "Debe ingresar fecha de Fin", "error");
    }else if(txt_fin < txt_inicio){
      sweetAlert("Error", "Fecha de Fin debe ser mayor a fecha de Inicio", "error");
    }else{
      $("form").submit();
    }
  }
</script>

<style media="screen">
  form{
    text-align: center;
  }
</style>

<script src="../js/vendor/jquery.js"></script>
<script src="../dist/sweetalert-dev.js"></script>
<script src="../js/table.min.js"></script>
<link rel="stylesheet" href="../dist/sweetalert.css">


<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>


<script>
(function($){
$(document).ready(function(){

$('#cssmenu li.active').addClass('open').children('ul').show();
	$('#cssmenu li.has-sub>a').on('click', function(){
		$(this).removeAttr('href');
		var element = $(this).parent('li');
		if (element.hasClass('open')) {
			element.removeClass('open');
			element.find('li').removeClass('open');
			element.find('ul').slideUp(200);
		}
		else {
			element.addClass('open');
			element.children('ul').slideDown(200);
			element.siblings('li').children('ul').slideUp(200);
			element.siblings('li').removeClass('open');
			element.siblings('li').find('li').removeClass('open');
			element.siblings('li').find('ul').slideUp(200);
		}
	});

});
})(jQuery);
</script>

<script>

	$(document).ready(function(){
	    $('.rg-table').DataTable();
	});

</script>

</body>
</html>
