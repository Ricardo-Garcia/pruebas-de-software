<?php
if(!isset($_SESSION)){
session_start();
}
$id_Persona = $_SESSION['id_Persona'];
include '../src/conexionBD.php';
$sql = "SELECT * FROM tipo_empleado";
$data = $db->query($sql);
$sql_persona = "SELECT * FROM persona WHERE ID_PERSONA = ".$_GET['id'];
$data_persona = $db->query($sql_persona);
while($fila = mysqli_fetch_assoc($data_persona)){
	$producto = array(
		'nombre'=> $fila['NOMBRE'],
		'apeP'=> $fila['APELLIDOPAT'],
		'apeM'=> $fila['APELLIDOMAT'],
		'direccion'=> $fila['DIRECCION'],
		);

}

?>

<!DOCTYPE html>
<html>
			<head>
    			<meta charset="utf-8">
			    <meta http-equiv="x-ua-compatible" content="ie=edge">
    			<meta name="viewport" content="width=device-width, initial-scale=1.0">
    			<title>Mary's Floreria</title>
    			<link rel="stylesheet" href="../css/foundation.css">
    			<link rel="stylesheet" href="../css/app.css" >
    			<link rel="stylesheet" href="../css/style.css" >
			</head>
<body class="rg-body">



	<div class="row fullWidth" style="height:100%">
		<div class="columns small-12 medium-3 large-3 content-left sP">
				<?php
					include('menu.php');
				?>

		</div>
		<div class="columns small-12 medium-9 large-9 content-right sP">
				<div id="saludo-inicio" class="columns small-12 large-8 medium-8" style="padding:20px">
			<span>
			Bienvenido :
			<?php
				echo $id_Persona;
					?>
				</span>
				</div>
				<div id="saludo-inicio" class="columns small-12 large-4 medium-4" style="text-align:right; padding:20px">
				<a id="cerrarSesion" href="../login.php">Cerrar sesion</a>
				</div>
			<div class="rg-container" style="padding:20px;margin-top:80px">
				<h2>Editar personal:</h2>
				<form class="rg-form">
					<div class="row">
						<div class="columns small-12 medium-6 large-6">

							<span>Nombre</span>
							<input id="txt_nombre" type="text" name="txt_nombre" value="<?php echo $producto['nombre'] ?>" placeholder="Ingrese nombre" onkeypress="return noSeaNumero(event);">
							<span>Apellido Paterno</span>
							<input id="txt_apeP" type="text" name="txt_apeP" value="<?php echo $producto['apeP']?>" placeholder="Ingrese apellido paterno" onkeypress="return noSeaNumero(event);">
							<span>Apellido Materno</span>
							<input id="txt_apeM" type="text" name="txt_apeM" value="<?php echo $producto['apeM'] ?>" placeholder="Ingrese apellido materno" onkeypress="return noSeaNumero(event);">
							<span>Dirección</span>
							<input id="txt_dir" type="text" name="txt_dir" value="<?php echo $producto['direccion']?>" placeholder="Ingrese dirección">
						</div>
						<div class="columns small-12 medium-6 large-6">
							<span>Documento</span>
							<br>
							<div class="columns small-12 medium-4 large-4 sP">
								<select id="rg-select-doc">
									<option>DNI</option>
									<option>Carné de extranjería</option>
									<option>Pasaporte</option>
								</select>
							</div>
							<div class="columns small-12 medium-8 large-8">
							<input id="txt_doc" maxlength="12" type="text" name="txt_doc" placeholder="Ingrese documento" onkeypress="return siSeaNumero(event);">
							</div>


							<span>Teléfono</span>
							<input id="txt_telefono" maxlength="9" type="text" name="txt_telefono" placeholder="Ingrese teléfono" onkeypress="return siSeaNumero(event);">
							<span>Correo</span>
							<input id="txt_correo" type="text" name="txt_correo" placeholder="Ingrese correo">
							<span>Tipo de usuario</span>
							<select name="txt_tipo_empleado">
								<?php
								while($fila = mysqli_fetch_assoc($data)){
								 ?>
								 <option value="<?php echo $fila['ID_TIPOEMPLEADO']?>"><?php echo $fila['NOMBRE']?></option>
								 <?php
								}
								 ?>
							</select>
						</div>
					<input type="button" onclick="validar()" value="Registrar" name="" class="rg-btn-primary">
					</div>
				</form>

			</div>


		</div>

	</div>


<script src="../js/vendor/jquery.js"></script>
<script src="../dist/sweetalert-dev.js"></script>
<link rel="stylesheet" href="../dist/sweetalert.css">

<script>
(function($){
$(document).ready(function(){

$('#cssmenu li.active').addClass('open').children('ul').show();
	$('#cssmenu li.has-sub>a').on('click', function(){
		$(this).removeAttr('href');
		var element = $(this).parent('li');
		if (element.hasClass('open')) {
			element.removeClass('open');
			element.find('li').removeClass('open');
			element.find('ul').slideUp(200);
		}
		else {
			element.addClass('open');
			element.children('ul').slideDown(200);
			element.siblings('li').children('ul').slideUp(200);
			element.siblings('li').removeClass('open');
			element.siblings('li').find('li').removeClass('open');
			element.siblings('li').find('ul').slideUp(200);
		}
	});

});
})(jQuery);

function validar(){
	if(document.getElementById("txt_nombre").value == '' ){
		sweetAlert("Error", "Escriba nombre del personal", "error");
	}else if(document.getElementById("txt_apeP").value == '' ){
		sweetAlert("Error", "Escriba apellido paterno del personal", "error");
	}else if(document.getElementById("txt_apeM").value == '' ){
		sweetAlert("Error", "Escriba apellido materno del personal", "error");
	}else if(document.getElementById("txt_dir").value == '' ){
		sweetAlert("Error", "Escriba dirección del personal", "error");
	}else if($("#rg-select-doc").prop('selectedIndex') == 0 && $("#txt_doc").val().length != 8 ){
		sweetAlert("Error", "Ingrese un DNI válido", "error");
	}else if($("#rg-select-doc").prop('selectedIndex') == 1 && $("#txt_doc").val().length != 12 ){
		sweetAlert("Error", "Ingrese un carné válido", "error");
	}else if($("#rg-select-doc").prop('selectedIndex') == 2 && $("#txt_doc").val().length != 7 ){
		sweetAlert("Error", "Ingrese un pasaporte válido", "error");
	}else if($("#txt_telefono").val().length  != 7 && $("#txt_telefono").val().length != 9){
		sweetAlert("Error", "Ingrese un teléfono válido", "error");
	}


}

function siSeaNumero(e) {
k = (document.all) ? e.keyCode : e.which;
if (k==8 || k==0) return true;
patron = /\d/;
n = String.fromCharCode(k);
return patron.test(n);
}

	function noSeaNumero(e) {
k = (document.all) ? e.keyCode : e.which;
if (k==8 || k==0) return true;
patron = /\D/;
n = String.fromCharCode(k);
return patron.test(n);
}




</script>



</body>
</html>
