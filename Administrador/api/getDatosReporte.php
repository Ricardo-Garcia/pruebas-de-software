<?php
//error_reporting(E_ALL);
//if($_POST){
  //@session_start();
  include('../../src/conexionBD.php');
  $desde = $_POST['from'];
  $hasta = $_POST['to'];

  $data_max = $db->query("SELECT
    pedido.FECHAEMISION,detalle_pedido.CANTIDAD, categoria.NOMBRECATEGORIA
    FROM
    detalle_pedido,pedido,producto,categoria_producto,categoria
    WHERE
    pedido.ID_PEDIDO = detalle_pedido.ID_PEDIDO AND
    detalle_pedido.ID_PRODUCTO = producto.ID_PRODUCTO AND
    producto.ID_PRODUCTO = categoria_producto.ID_PRODUCTO AND
    categoria_producto.ID_CATEGORIA = categoria.ID_CATEGORIA AND
    pedido.FECHAEMISION BETWEEN '{$desde}' AND '{$hasta}'");


      $data_max2 = $db->query("SELECT distinct
        producto.NOMBREPRODUCTO, categoria.NOMBRECATEGORIA
        FROM
        detalle_pedido,pedido,producto,categoria_producto,categoria
        WHERE
        pedido.ID_PEDIDO = detalle_pedido.ID_PEDIDO AND
        detalle_pedido.ID_PRODUCTO = producto.ID_PRODUCTO AND
        producto.ID_PRODUCTO = categoria_producto.ID_PRODUCTO AND
        categoria_producto.ID_CATEGORIA = categoria.ID_CATEGORIA AND
        pedido.FECHAEMISION BETWEEN '{$desde}' AND '{$hasta}'
        ORDER BY detalle_pedido.CANTIDAD DESC LIMIT 10");

    $alldata = mysqli_fetch_all($data_max,MYSQLI_ASSOC);
    $most = mysqli_fetch_all($data_max2,MYSQLI_ASSOC);

    $array_suma_general_aux = array();
    $array_suma_general = array();
    $obj_mes_seccionado = array();
    $suma_total = 0;

    $meses_str = array("","January","February","March","April","May","June","July","August","September","October","November","December");
    $in_f = date("n",strtotime($desde));
    $in_t = date("n",strtotime($hasta));
for ($i = $in_f; $i <= $in_t; $i++) {
  $obj_mes_seccionado[$meses_str[$i]] = array(0,0);
}


    foreach ($alldata as $key => $value) {
      if(!array_key_exists($value['NOMBRECATEGORIA'],$array_suma_general_aux)){
        $array_suma_general_aux[$value['NOMBRECATEGORIA']] = 0;
      }
      $mes = date("F",strtotime($value['FECHAEMISION']));

      $array_suma_general_aux[$value['NOMBRECATEGORIA']] += intval($value['CANTIDAD']);
      if($value['NOMBRECATEGORIA'] === "UNITARIO"){
        $obj_mes_seccionado[$mes][0] += intval($value['CANTIDAD']);
      }else{
        $obj_mes_seccionado[$mes][1] += intval($value['CANTIDAD']);
      }


      $suma_total += intval($value['CANTIDAD']);
    }

    foreach ($array_suma_general_aux as $key => $value) {
      array_push($array_suma_general,array("name"=>$key,"y"=>round($value*100/$suma_total,2)));
    }
    $array_mes_seccionado = array();
    foreach ($obj_mes_seccionado as $key => $value) {
      array_push($array_mes_seccionado,array($key,$value));
      //$obj_mes_seccionado[$value] = array(0,0);
    }
    $resp = array();
    $resp['pie'] = $array_suma_general;
    $resp['mes_seccionado'] = $array_mes_seccionado;
    $resp['ordenado'] = $most;

    echo json_encode($resp);
//}
