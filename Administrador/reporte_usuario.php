<?php
if(!isset($_SESSION)){
session_start();
}
$id_Persona = $_SESSION['id_Persona'];
include '../src/conexionBD.php';
$sql = "SELECT tp.ID_PEDIDO , tcli.ID_PERSONA , CONCAT(SERIECOMP,NUMCOMP) AS serie  , tc.FECHACOMPROBANTE as fecha , tper.NOMBRE nombre , tper.APELLIDOPAT apeP , tper.APELLIDOMAT apeM , tp.MONTOTOTAL monto , tep.ESTADO
FROM pedido tp, comprobante tc, cliente tcli, persona tper, estado_pedido tep
WHERE tc.ID_PEDIDO = tp.ID_PEDIDO
AND tp.ID_CLIENTE = tcli.ID_CLIENTE
AND tper.ID_PERSONA = tcli.ID_PERSONA
AND tp.ID_ESTADOPEDIDO = tep.ID_ESTADOPEDIDO";
if($_POST){
	$fec_inicio = $_POST['txt_inicio'];
	$fec_fin = $_POST['txt_fin'];
	$sql= $sql  ." AND tc.FECHACOMPROBANTE >= '".$fec_inicio."' AND tc.FECHACOMPROBANTE <= '".$fec_fin."'";

	$tipo_comprobante = $_POST['txt_tipo_comprobante'];
	if($tipo_comprobante == '1'){

	}else if($tipo_comprobante == '2'){
		$sql = $sql ." AND tc.TIPOCOMPROBANTE ='BOLETA'";
	}else{
		$sql = $sql ." AND tc.TIPOCOMPROBANTE ='FACTURA'";
	}

	$txt_estado_documento = $_POST['txt_estado_documento'];
	if($txt_estado_documento == '1'){

	}else if($txt_estado_documento == '2'){
		$sql = $sql . " AND tep.ESTADO ='SOLICITADO'";
	}else if($txt_estado_documento == '3'){
		$sql = $sql . " AND tep.ESTADO ='REGISTRADO'";
	}else if($txt_estado_documento == '4'){
		$sql = $sql . " AND tep.ESTADO ='TERMINADO'";
	}else if($txt_estado_documento == '5'){
		$sql = $sql . " AND tep.ESTADO ='CANCELADO'";
	}
 $txt_nombre_cliente = $_POST['txt_nombre_cliente'];
	if($txt_nombre_cliente != ""){
		$sql = $sql . " AND tper.NOMBRE  LIKE '%".$txt_nombre_cliente."%'";
	}
}



$data = $db->query($sql);
?>
<!DOCTYPE html>
<html>
			<head>
    			<meta charset="utf-8">
			    <meta http-equiv="x-ua-compatible" content="ie=edge">
    			<meta name="viewport" content="width=device-width, initial-scale=1.0">
    			<title>Mary's Floreria</title>
    			<link rel="stylesheet" href="../css/foundation.css">
    			<link rel="stylesheet" href="../css/app.css" >
    			<link rel="stylesheet" href="../css/style.css" >
					<link rel="stylesheet" href="http://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" >

    			<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
			</head>
<body class="rg-body">
	<div class="row fullWidth" style="height:100%">
		<div class="columns small-12 medium-3 large-3 content-left sP">
				<?php
					include('menu.php');
				?>

		</div>
		<div class="columns small-12 medium-9 large-9 content-right sP">
				<div id="saludo-inicio" class="columns small-12 large-8 medium-8" style="padding:20px">
			<span>
				</span>
				</div>
				<div id="saludo-inicio" class="columns small-12 large-4 medium-4" style="text-align:right; padding:20px">
				<a id="cerrarSesion" href="../login.php">Cerrar sesion</a>
				</div>
			<div class="rg-container" style="padding:20px;margin-top:80px">

        <h2>Reporte de Ventas</h2>

        <form class="" action="detalle_reporte.php" method="post">
          <div class="columns small-12 medium-6 large-6">
            <h4>Fecha de Inicio</h4>
            <input id="txt_inicio" type="date" name="txt_inicio" value="<?php echo $_POST['txt_inicio']?>">
          </div>
          <div class="columns small-12 medium-6 large-6">
            <h4>Fecha de Fin</h4>
            <input id="txt_fin" type="date" name="txt_fin" value="<?php echo $_POST['txt_fin']?>">
          </div>
					<div class="clearfix"></div>
					<div class="columns small-12 medium-4 large-4">
						<h6>Cliente</h6>
						<input type="text" id="txt_nombre_cliente" name="txt_nombre_cliente" value="<?php echo $_POST['txt_nombre_cliente']?>">
					</div>
					<div class="columns small-12 medium-4 large-4">
						<h6>TIPO DE COMPROBANTE</h6>
						<select class="" name="txt_tipo_comprobante">
						  <option value="1">TODOS</option>
							<option value="2">Boleta</option>
							<option value="3">Factura</option>
						</select>
					</div>
					<div class="columns small-12 medium-4 large-4">
						<h6>ESTADO DE DOCUMENTO</h6>
						<select class="" name="txt_estado_documento">
						  <option value="1">TODOS</option>
							<option value="2">SOLICITADO</option>
							<option value="3">REGISTRADO</option>
							<option value="4">TERMINADO</option>
							<option value="5">CANCELADO</option>
						</select>
					</div>

					<div class="clearfix">

					</div>

						<input type="button" class="rg-btn-primary" name="name" value="Buscar" onclick="validar()" style="float:left">


        </form>

				<table class="tabla_filtro">
					<thead>
		      <tr class="rg_tr_header">
						<th>Serie_No de comprobante</th>
		        <th>Tipo de comprobante</th>
		        <th>F. Emisión</th>
		        <th>Cliente</th>
		        <th>Valor Total S/.</th>
		        <th>Estado</th>
		        <th>Ver</th>

		      </tr>
				</thead>
				<tfoot>
					<tr style="font-size:0.7em;display:none">
						<th>
							<th>Serie_No de comprobante</th>
			        <th>Tipo de comprobante</th>
			        <th>F. Emisión</th>
			        <th>Cliente</th>
			        <th>Valor Total S/.</th>
			        <th>Estado</th>
			        <th>Ver</th>
						</th>
					</tr>
				</tfoot>

<tbody>

	<?php
	 $cont = 0;
	 while($fila = mysqli_fetch_assoc($data)){
		 $cont++;
		 ?>

	<tr class="rg_tr_clientes">
		<td><?php echo $fila['serie'] ?></td>
		<td><?php
		$tmp = $fila['serie'][0];
		if($tmp === 'B'){
			echo 'Boleta';
		}else{
			echo 'Factura';
		}
		 ?></td>
		<td><?php echo $fila['fecha'] ?></td>
		<td><?php echo $fila['nombre']." ".$fila['apeP']." ".$fila['apeM']; ?></td>
		<td><?php echo $fila['monto'] ?></td>
		<td><?php echo $fila['ESTADO'] ?></td>
		<td><a target="_blank" href="BoletaPago.php?id=<?php echo $fila['ID_PEDIDO']?>&id_Cliente=<?php echo $fila['ID_PERSONA']?>">Ver</a></td>
	</tr>
	<?php }
	 ?>

</tbody>


			</table>




			</div>


		</div>

	</div>
<script type="text/javascript">
  function validar(){
    var txt_inicio = $("#txt_inicio").val();
    var txt_fin = $("#txt_fin").val();
		var txt_nombre_cliente = $("#txt_nombre_cliente").val();
		var txt_nombre_empresa = $("#txt_nombre_empresa").val();

    if(txt_inicio == ""){
      sweetAlert("Error", "Debe ingresar fecha de Inicio", "error");
    }else if(txt_fin == ""){
      sweetAlert("Error", "Debe ingresar fecha de Fin", "error");
    }else if(txt_fin < txt_inicio){
      sweetAlert("Error", "Fecha de Fin debe ser mayor a fecha de Inicio", "error");
    }else if(txt_nombre_cliente == "" && txt_nombre_empresa == "" ){
			sweetAlert("Error", "Escriba nombre del cliente", "error");
		}else{
			$("form").attr('action','reporte_usuario.php');
      $("form").submit();
    }
  }
</script>

<style media="screen">
  form{
    text-align: center;
  }
	table{
		position: relative;
		top: 20px;
	}
</style>

<script src="../js/vendor/jquery.js"></script>
<script src="../dist/sweetalert-dev.js"></script>
<script src="../js/table.min.js"></script>
<link rel="stylesheet" href="../dist/sweetalert.css">
<script src="../js/table.min.js"></script>
<script src="../js/stupidtable.js?dev"></script>




<script>
(function($){
$(document).ready(function(){

$('#cssmenu li.active').addClass('open').children('ul').show();
	$('#cssmenu li.has-sub>a').on('click', function(){
		$(this).removeAttr('href');
		var element = $(this).parent('li');
		if (element.hasClass('open')) {
			element.removeClass('open');
			element.find('li').removeClass('open');
			element.find('ul').slideUp(200);
		}
		else {
			element.addClass('open');
			element.children('ul').slideDown(200);
			element.siblings('li').children('ul').slideUp(200);
			element.siblings('li').removeClass('open');
			element.siblings('li').find('li').removeClass('open');
			element.siblings('li').find('ul').slideUp(200);
		}
	});

});
})(jQuery);
</script>

<script>
var cant_per  = $("tr").size()-1;
  function registrarPersonal(){
		window.location = "registrar_personal.php";
	}
	function actualizarPersonal(){
		var value = $("input[name=id_persona]:checked").val()
		if(value == null){
			sweetAlert("Error", "Elija el personal que desee actualizar", "error");
		}else{
			window.location = "editar_personal.php?id="+value;
		}

	}
	function bloquearPersonal(){
		var value = $("input[name=id_persona]:checked").val()
		if(value == null){
			sweetAlert("Error", "Elija el personal que desee bloquear", "error");
		}else{
			var cont = 0;
			while(cont <= cant_per){
				var id = $("tr").eq(cont).data('id');
				if(id == value){
					var nom = $("tr").eq(cont).data('nombre');
					var est = $("tr").eq(cont).data('estado');
				}
				cont++;
			}
			if(est == '1'){
			swal({title: '¿Esta seguro?',   text: 'Bloqueará al personal  : '+nom,   type: 'warning',   showCancelButton: true, cancelButtonText: 'Cancelar',   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Bloquear',   closeOnConfirm: false }, function(){   swal('Hecho!', 'Se bloqueó el personal', 'success');
				setTimeout(function() {window.location.replace('BloquearPersonal.php?id='+value);},1500);});
			}else{
				sweetAlert("Error", "Este personal  ya se encuentra bloqueado", "error");
			}
				/**
				-- Metodo de modificación del producto value --
				**/
			}
	}
	function habilitarPersonal(){

		var value = $("input[name=id_persona]:checked").val()
		if(value == null){
			sweetAlert("Error", "Elija el personal que desee habilitar", "error");
		}else{
			var cont = 0;
			while(cont <= cant_per){
				var id = $("tr").eq(cont).data('id');
				if(id == value){
					var nom = $("tr").eq(cont).data('nombre');
					var est = $("tr").eq(cont).data('estado');
				}
				cont++;
			}
			if(est == '2'){
			swal({title: '¿Esta seguro?',   text: 'Hablilitará al personal  : '+nom,   type: 'warning',   showCancelButton: true, cancelButtonText: 'Cancelar',   confirmButtonColor: '#009688',   confirmButtonText: 'Habilitar',   closeOnConfirm: false }, function(){   swal('Hecho!', 'Se habilitó el personal', 'success');
				setTimeout(function() {window.location.replace('HabilitarPersonal.php?id='+value);},1500);});
			}else{
				sweetAlert("Error", "Este personal  ya se encuentra habilitado", "error");
			}
				/**
				-- Metodo de modificación del producto value --
				**/
			}

	}

	$(document).ready(function(){
	    $('.rg-table').DataTable();
	});

</script>
<style>
label{
	color: #af2124;
	font-size: 1.5em;}
}

.dataTables_info{
	color: #af2124 !important;
font-size: 1.5em !important;

}
</style>

<script type="text/javascript">
var frmGoTest = $("#goTest");
var ihfrom = $("input[name=txt_inicio]");
var ihto = $("input[name=txt_fin]");
var ihfrom1 = $("input:hidden[name=from]");
var ihto1 = $("input:hidden[name=to]");

function sendTest(){
	console.log(ihfrom,ihto);
	if(ihfrom.val() === "" || ihto.val() === ""){
		swal("Error","Te falta especificar un rango de fecha","error");
	}else if(ihfrom.val() >= ihto.val()){
		sweetAlert("Error", "Fecha de Fin debe ser mayor a fecha de Inicio", "error");
	}else{
		ihfrom1.val(ihfrom.val()+" 00:00:00");
		ihto1.val(ihto.val()+" 00:00:00");

		frmGoTest.submit();
	}
}
$(function () {


  $(document).ready(function () {


      // Build the chart
      Highcharts.chart('container', {
          chart: {
              plotBackgroundColor: null,
              plotBorderWidth: null,
              plotShadow: false,
              type: 'pie'
          },
          title: {
              text: 'Ventas por categoría'
          },
          tooltip: {
              pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
          },
          plotOptions: {
              pie: {
                  allowPointSelect: true,
                  cursor: 'pointer',
                  dataLabels: {
                      enabled: false
                  },
                  showInLegend: true
              }
          },
          series: [{
              name: 'Brands',
              colorByPoint: true,
              data: [
                <?php
                $sql_categorias = "SELECT * FROM categoria";
                $data_categorias = $db->query($sql_categorias);
                while($fila = mysqli_fetch_assoc($data_categorias)){
                  ?>
                  {
                    name: '<?php echo $fila['NOMBRECATEGORIA']?>',
                    y: 56.33
                  },
                  <?
                }
                ?>
                ]
          }]
      });
  });
});
</script>

</body>
</html>
