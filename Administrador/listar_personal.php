<?php
if(!isset($_SESSION)){
session_start();
}
$id_Persona = $_SESSION['id_Persona'];
include '../src/conexionBD.php';
$sql = "SELECT ESTADOPER, tp.NOMBRE, tc.CORREO, tt.NUMERO,  tp.APELLIDOPAT, tp.APELLIDOMAT, tp.DIRECCION, tp.ID_PERSONA, te.FECHAINGRESO, tte.NOMBRE as NOMBRE_ROL
FROM empleado te, persona tp, tipo_empleado tte, telefono tt, correo tc
WHERE te.ID_PERSONA = tp.ID_PERSONA
AND tte.ID_TIPOEMPLEADO = te.ID_TIPOEMPLEADO
AND tt.ID_PERSONA = tp.ID_PERSONA
AND tc.ID_PERSONA = tp.ID_PERSONA
ORDER BY te.FECHAINGRESO ASC";
$data = $db->query($sql);
?>
<!DOCTYPE html>
<html>
			<head>
    			<meta charset="utf-8">
			    <meta http-equiv="x-ua-compatible" content="ie=edge">
    			<meta name="viewport" content="width=device-width, initial-scale=1.0">
    			<title>Mary's Floreria</title>
    			<link rel="stylesheet" href="../css/foundation.css">
    			<link rel="stylesheet" href="../css/app.css" >
    			<link rel="stylesheet" href="../css/style.css" >
					<link rel="stylesheet" href="http://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" >

    			<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
			</head>
<body class="rg-body">
	<div class="row fullWidth" style="height:100%">
		<div class="columns small-12 medium-3 large-3 content-left sP">
				<?php
					include('menu.php');
				?>

		</div>
		<div class="columns small-12 medium-9 large-9 content-right sP">
				<div id="saludo-inicio" class="columns small-12 large-8 medium-8" style="padding:20px">
			<span>
				</span>
				</div>
				<div id="saludo-inicio" class="columns small-12 large-4 medium-4" style="text-align:right; padding:20px">
				<a id="cerrarSesion" href="../login.php">Cerrar sesion</a>
				</div>
			<div class="rg-container" style="padding:20px;margin-top:80px">
				<h2>Personal:</h2>
<table class="rg-table">
	<thead>
  <tr style="font-size:0.7em;background-color: #3c3f39;color:white;text-align:center">

    <th>Nombre</th>
    <th>Apellido Paterno</th>
		<th>Apellido Materno</th>
    <th>Dirección</th>
		<th>Teléfono</th>
		<th>Email</th>
    <th>Fecha de ingreso</th>
    <th>Tipo de empleado</th>
    <th>Elegir</th>
  </tr>
	</thead>
	<tfoot>
		<tr style="font-size:0.7em;display:none">
			<th>
				<th>Nombre</th>
		    <th>Apellido Paterno</th>
				<th>Apellido Materno</th>
		    <th>Dirección</th>
				<th>Teléfono</th>
				<th>Email</th>
		    <th>Fecha de ingreso</th>
		    <th>Tipo de empleado</th>
		    <th>Elegir</th>
			</th>
		</tr>
	</tfoot>

	<tbody>
  <?php
  while($fila = mysqli_fetch_assoc($data)){
  ?>
  <tr style="font-size:0.6em;" <?php if($fila['ESTADOPER'] == 2){ ?> class="personal_bloqueado" <?php } ?> data-nombre="<?php echo $fila['APELLIDOPAT']." ".$fila['APELLIDOMAT'].", ".$fila['NOMBRE'] ?>"  data-id="<?php echo $fila['ID_PERSONA']?>" data-estado="<?php echo $fila['ESTADOPER']?>">
    <td style="text-align:center"><?php echo $fila['NOMBRE']?></td>
    <td style="text-align:center"><?php echo $fila['APELLIDOPAT']?></td>
		<td style="text-align:center"><?php echo $fila['APELLIDOMAT']?></td>
    <td style="text-align:center"><?php echo $fila['DIRECCION']?></td>
		<td style="text-align:center"><?php echo $fila['NUMERO']?></td>
		<td style="text-align:center"><?php echo $fila['CORREO']?></td>
    <td style="text-align:center"><?php echo $fila['FECHAINGRESO']?></td>
    <td style="text-align:center"><?php echo $fila['NOMBRE_ROL']?></td>
		<td style="text-align:center">
    	<input type="radio" name="id_persona" value="<?php echo $fila['ID_PERSONA']?>">
    </td>
  </tr>
  <?php
	}
  ?>
</tbody>
</table>



<div class="row">
	<div class="columns small-3 medium-3 large-3 ">
			<input class="rg-btn-primary" type="button" name="name" value="Registrar Personal" onclick="registrarPersonal()">
	</div>
	<div class="columns small-3 medium-3 large-3 ">
			<input class="rg-btn-primary" type="button" name="name" value="Actualizar Personal" onclick="actualizarPersonal()">
	</div>
	<div class="columns small-3 medium-3 large-3 ">
			<input class="rg-btn-primary" type="button" name="name" value="Bloquear Personal" onclick="bloquearPersonal()">
	</div>
	<div class="columns small-3 medium-3 large-3 ">
			<input class="rg-btn-primary" type="button" name="name" value="Habilitar Personal" onclick="habilitarPersonal()">
	</div>
</div>






			</div>


		</div>

	</div>


<script src="../js/vendor/jquery.js"></script>
<script src="../dist/sweetalert-dev.js"></script>
<script src="../js/table.min.js"></script>
<link rel="stylesheet" href="../dist/sweetalert.css">
<script>
(function($){
$(document).ready(function(){

$('#cssmenu li.active').addClass('open').children('ul').show();
	$('#cssmenu li.has-sub>a').on('click', function(){
		$(this).removeAttr('href');
		var element = $(this).parent('li');
		if (element.hasClass('open')) {
			element.removeClass('open');
			element.find('li').removeClass('open');
			element.find('ul').slideUp(200);
		}
		else {
			element.addClass('open');
			element.children('ul').slideDown(200);
			element.siblings('li').children('ul').slideUp(200);
			element.siblings('li').removeClass('open');
			element.siblings('li').find('li').removeClass('open');
			element.siblings('li').find('ul').slideUp(200);
		}
	});

});
})(jQuery);
</script>

<script>
var cant_per  = $("tr").size()-1;
  function registrarPersonal(){
		window.location = "registrar_personal.php";
	}
	function actualizarPersonal(){
		var value = $("input[name=id_persona]:checked").val()
		if(value == null){
			sweetAlert("Error", "Elija el personal que desee actualizar", "error");
		}else{
			window.location = "editar_personal.php?id="+value;
		}

	}
	function bloquearPersonal(){
		var value = $("input[name=id_persona]:checked").val()
		if(value == null){
			sweetAlert("Error", "Elija el personal que desee bloquear", "error");
		}else{
			var cont = 0;
			while(cont <= cant_per){
				var id = $("tr").eq(cont).data('id');
				if(id == value){
					var nom = $("tr").eq(cont).data('nombre');
					var est = $("tr").eq(cont).data('estado');
				}
				cont++;
			}
			if(est == '1'){
			swal({title: '¿Esta seguro?',   text: 'Bloqueará al personal  : '+nom,   type: 'warning',   showCancelButton: true, cancelButtonText: 'Cancelar',   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Bloquear',   closeOnConfirm: false }, function(){   swal('Hecho!', 'Se bloqueó el personal', 'success');
				setTimeout(function() {window.location.replace('BloquearPersonal.php?id='+value);},1500);});
			}else{
				sweetAlert("Error", "Este personal  ya se encuentra bloqueado", "error");
			}
				/**
				-- Metodo de modificación del producto value --
				**/
			}
	}
	function habilitarPersonal(){

		var value = $("input[name=id_persona]:checked").val()
		if(value == null){
			sweetAlert("Error", "Elija el personal que desee habilitar", "error");
		}else{
			var cont = 0;
			while(cont <= cant_per){
				var id = $("tr").eq(cont).data('id');
				if(id == value){
					var nom = $("tr").eq(cont).data('nombre');
					var est = $("tr").eq(cont).data('estado');
				}
				cont++;
			}
			if(est == '2'){
			swal({title: '¿Esta seguro?',   text: 'Hablilitará al personal  : '+nom,   type: 'warning',   showCancelButton: true, cancelButtonText: 'Cancelar',   confirmButtonColor: '#009688',   confirmButtonText: 'Habilitar',   closeOnConfirm: false }, function(){   swal('Hecho!', 'Se habilitó el personal', 'success');
				setTimeout(function() {window.location.replace('HabilitarPersonal.php?id='+value);},1500);});
			}else{
				sweetAlert("Error", "Este personal  ya se encuentra habilitado", "error");
			}
				/**
				-- Metodo de modificación del producto value --
				**/
			}

	}

	$(document).ready(function(){
	    $('.rg-table').DataTable();
	});

</script>
<style>
label{
	color: #af2124;
	font-size: 1.5em;}
}

.dataTables_info{
	color: #af2124 !important;
font-size: 1.5em !important;

}
</style>

</body>
</html>
