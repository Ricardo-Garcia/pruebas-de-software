<?php
if(!isset($_SESSION)){
session_start();
}
$id_Persona = $_SESSION['id_Persona'];
include '../src/conexionBD.php';
$sql = "SELECT * FROM categoria";
$data_categorias = $db->query($sql);
?>
<!DOCTYPE html>
<html>
			<head>
    			<meta charset="utf-8">
			    <meta http-equiv="x-ua-compatible" content="ie=edge">
    			<meta name="viewport" content="width=device-width, initial-scale=1.0">
    			<title>Mary's Floreria</title>
    			<link rel="stylesheet" href="../css/foundation.css">
    			<link rel="stylesheet" href="../css/app.css" >
    			<link rel="stylesheet" href="../css/style.css" >
					<link rel="stylesheet" href="http://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" >

    			<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
			</head>
<body class="rg-body">
	<div class="row fullWidth" style="height:100%">
		<div class="columns small-12 medium-3 large-3 content-left sP">
				<?php
					include('menu.php');
				?>

		</div>
		<div class="columns small-12 medium-9 large-9 content-right sP">
				<div id="saludo-inicio" class="columns small-12 large-8 medium-8" style="padding:20px">
			<span>
				</span>
				</div>
				<div id="saludo-inicio" class="columns small-12 large-4 medium-4" style="text-align:right; padding:20px">
				<a id="cerrarSesion" href="../login.php">Cerrar sesion</a>
				</div>
			<div class="rg-container" style="padding:20px;margin-top:80px">

        <h2>Reporte de Ventas</h2>

      <center>
        <div class="columns small-12 medium-6 large-6">
          <h4>Fecha inicio</h4>
					<h4><?php echo $_POST['from']; ?></h4>
        </div>
        <div class="columns small-12 medium-6 large-6">
          <h4>Fecha de fin</h4>
					<h4><?php echo $_POST['to']; ?></h4>
        </div>
      </center>

      <div class="columns small-12 medium-12 large-12">
        <div id="container" style="min-width: 100%; height: 400px; max-width: 100%; margin: 0 auto"></div>
      </div>

      <div class="columns small-12 medium-12 large-12">
        <div id="container2" style="min-width: 100%; height: 400px; margin: 0 auto"></div>
      </div>

      <div class="columns small-12 medium-12 large-12">
        <div id="container3" style="min-width: 100%; height: 400px; margin: 0 auto"></div>
      </div>

      <div class="columns small-12 medium-12 large-12">
        <center><h4>Los 10 productos más vendidos</h4></center>
				<table id="top10" class="">
			    <thead>
			        <tr>
			            <th>N°</th>
			            <th>Nombre</th>
			            <th>Categoria</th>
			        </tr>
			    </thead>
			    <tbody>

			    </tbody>
			</table>
      </div>

      <table id="datatable" style="display:none">
    <thead>
        <tr>
            <th></th>
            <th>Unitarios</th>
            <th>Arreglos</th>
        </tr>
    </thead>
    <tbody>

    </tbody>
</table>

			</div>


		</div>

	</div>




<style media="screen">
  form{
    text-align: center;
  }
</style>

<script src="../js/vendor/jquery.js"></script>
<script src="../dist/sweetalert-dev.js"></script>
<script src="../js/table.min.js"></script>
<link rel="stylesheet" href="../dist/sweetalert.css">


<script src="../js/highcharts.js"></script>
<script src="../js/exporting.js"></script>
<script src="../js/data.js"></script>

<script>
(function($){
$(document).ready(function(){

$('#cssmenu li.active').addClass('open').children('ul').show();
	$('#cssmenu li.has-sub>a').on('click', function(){
		$(this).removeAttr('href');
		var element = $(this).parent('li');
		if (element.hasClass('open')) {
			element.removeClass('open');
			element.find('li').removeClass('open');
			element.find('ul').slideUp(200);
		}
		else {
			element.addClass('open');
			element.children('ul').slideDown(200);
			element.siblings('li').children('ul').slideUp(200);
			element.siblings('li').removeClass('open');
			element.siblings('li').find('li').removeClass('open');
			element.siblings('li').find('ul').slideUp(200);
		}
	});

});
})(jQuery);
</script>

<script>

	$(document).ready(function(){
	    $('.rg-table').DataTable();
	});

</script>



<script type="text/javascript">
$.post("api/getDatosReporte.php",{to: "<?php echo $_POST['to']; ?>",from: "<?php echo $_POST['from']; ?>"},function(data_api){

	if(typeof data_api === "string"){
		data_api = JSON.parse(data_api);
	}

	var tabla10 = "";
	for(k in data_api.ordenado){
		tabla10 += "<tr><td>"+(parseInt(k)+1)+"</td><td>"+data_api.ordenado[k].NOMBREPRODUCTO+"</td><td>"+data_api.ordenado[k].NOMBRECATEGORIA+"</td></tr>";
	}
	$("#top10 tbody").html(tabla10);

	// CONTAINER 1
	$(function () {
	    Highcharts.chart('container', {
	        chart: {
	            plotBackgroundColor: null,
	            plotBorderWidth: null,
	            plotShadow: false,
	            type: 'pie'
	        },
	        title: {
	            text: 'Ventas por categorías'
	        },
	        tooltip: {
	            pointFormat: ''
	        },
	        plotOptions: {
	            pie: {
	                allowPointSelect: true,
	                cursor: 'pointer',
	                dataLabels: {
	                    enabled: true,
	                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
	                    style: {
	                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
	                    }
	                }
	            }
	        },
	        series: [{
	            name: 'Brands',
	            colorByPoint: true,
	            data: data_api.pie
	        }]
	    });
	});

	// CONTAINER 2
	$(function () {
		var valores = "";
		for(c in data_api.mes_seccionado){
			valores += "<tr><td>"+data_api.mes_seccionado[c][0]+"</td><td>"+data_api.mes_seccionado[c][1][0]+"</td><td>"+data_api.mes_seccionado[c][1][1]+"</td></tr>"
		}
		$("#datatable tbody").html(valores);
	    Highcharts.chart('container2', {
	        data: {
	            table: 'datatable'
	        },
	        chart: {
	            type: 'column'
	        },
	        title: {
	            text: 'Ventas por tipo de producto'
	        },
	        yAxis: {
	            allowDecimals: false,
	            title: {
	                text: 'Ventas'
	            }
	        },
	        tooltip: {
	            formatter: function () {
	                return '<b>' + this.series.name + '</b><br/>' +
	                    this.point.y + ' en ' + this.point.name.toLowerCase();
	            }
	        }
	    });
	});
	 // CONTAINER 3
	 var categories_fiji = [];
	 var values_fiji = [];
	 $(function () {

		 for(r in data_api.mes_seccionado){
			 categories_fiji.push(data_api.mes_seccionado[r][0]);
			 values_fiji.push(data_api.mes_seccionado[r][1][0]+data_api.mes_seccionado[r][1][1]);
		 }
	    Highcharts.chart('container3', {
	        chart: {
	            type: 'areaspline'
	        },
	        title: {
	            text: 'Ventas por mes'
	        },
	        legend: {
	            layout: 'vertical',
	            align: 'left',
	            verticalAlign: 'top',
	            x: 150,
	            y: 100,
	            floating: true,
	            borderWidth: 1,
	            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
	        },
	        xAxis: {
	            categories: categories_fiji,
	            plotBands: [{ // visualize the weekend
	                from: 4.5,
	                to: 6.5,
	                color: 'rgba(255, 170, 213, .2)'
	            }]
	        },
	        yAxis: {
	            title: {
	                text: 'Ventas'
	            }
	        },
	        tooltip: {
	            shared: true,
	            valueSuffix: ' units'
	        },
	        credits: {
	            enabled: false
	        },
	        plotOptions: {
	            areaspline: {
	                fillOpacity: 0.5
	            }
	        },
	        series:
	        [{
	            name: 'Ventas',
	            data: values_fiji
	        }
	      ]
	    });
	});
});
</script>


</body>
</html>
