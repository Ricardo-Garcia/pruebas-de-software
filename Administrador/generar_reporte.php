<?php
if(!isset($_SESSION)){
session_start();
}
$id_Persona = $_SESSION['id_Persona'];
include '../src/conexionBD.php';
$sql = "SELECT ESTADOPER, tp.NOMBRE, tc.CORREO, tt.NUMERO,  tp.APELLIDOPAT, tp.APELLIDOMAT, tp.DIRECCION, tp.ID_PERSONA, te.FECHAINGRESO, tte.NOMBRE as NOMBRE_ROL
FROM empleado te, persona tp, tipo_empleado tte, telefono tt, correo tc
WHERE te.ID_PERSONA = tp.ID_PERSONA
AND tte.ID_TIPOEMPLEADO = te.ID_TIPOEMPLEADO
AND tt.ID_PERSONA = tp.ID_PERSONA
AND tc.ID_PERSONA = tp.ID_PERSONA
ORDER BY te.FECHAINGRESO ASC";
$data = $db->query($sql);
?>
<!DOCTYPE html>
<html>
			<head>
    			<meta charset="utf-8">
			    <meta http-equiv="x-ua-compatible" content="ie=edge">
    			<meta name="viewport" content="width=device-width, initial-scale=1.0">
    			<title>Mary's Floreria</title>
    			<link rel="stylesheet" href="../css/foundation.css">
    			<link rel="stylesheet" href="../css/app.css" >
    			<link rel="stylesheet" href="../css/style.css" >
					<link rel="stylesheet" href="http://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" >

    			<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
			</head>
<body class="rg-body">
	<div class="row fullWidth" style="height:100%">
		<div class="columns small-12 medium-3 large-3 content-left sP">
				<?php
					include('menu.php');
				?>

		</div>
		<div class="columns small-12 medium-9 large-9 content-right sP">
				<div id="saludo-inicio" class="columns small-12 large-8 medium-8" style="padding:20px">
			<span>
				</span>
				</div>
				<div id="saludo-inicio" class="columns small-12 large-4 medium-4" style="text-align:right; padding:20px">
				<a id="cerrarSesion" href="../login.php">Cerrar sesion</a>
				</div>
			<div class="rg-container" style="padding:20px;margin-top:80px">

        <h2>Reporte de Pedidos</h2>
        <?php
        if(!$_POST){
         ?>
        <form class="" action="detalle_reporte.php" method="post">
          <div class="columns small-12 medium-6 large-6">
            <h4>Fecha de Inicio</h4>
            <input id="txt_inicio" type="date" name="txt_inicio" value="">
          </div>
          <div class="columns small-12 medium-6 large-6">
            <h4>Fecha de Fin</h4>
            <input id="txt_fin" type="date" name="txt_fin" value="">
          </div>
					<div class="clearfix"></div>






        </form>

				<table>

				</table>
				<form method="post" action="test.php" id="goTest">
					<input type="hidden" name="from" value="">
					<input type="hidden" name="to" value="">
					<input type="button" class="rg-btn-primary" style="background-color: green!important;float:left" value="Ver reporte" onclick="sendTest()">
				</form>
        <?php
      }else{
        $txt_inicio = $_POST['txt_inicio'];
        $txt_fin = $_POST['txt_fin'];
         ?>
         <center>
             <h2>Reporte de Ventas</h2>

             <div class="columns small-12 medium-6 large-6">
               <h4>Desde : <?php echo $_POST['txt_inicio']; ?></h4>
             </div>

             <div class="columns small-12 medium-6 large-6">
               <h4>Hasta : <?php echo $_POST['txt_inicio']; ?></h4>
             </div>
         </center>

         <div class="columns small-12 large-6 medium-6">
           <div id="container" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
         </div>
         <div class="columns small-12 large-6 medium-6">

         </div>


<?php }  ?>

			</div>


		</div>

	</div>
<script type="text/javascript">
  function validar(){
    var txt_inicio = $("#txt_inicio").val();
    var txt_fin = $("#txt_fin").val();
		var txt_nombre_cliente = $("#txt_nombre_cliente").val();
		var txt_nombre_empresa = $("#txt_nombre_empresa").val();

    if(txt_inicio == ""){
      sweetAlert("Error", "Debe ingresar fecha de Inicio", "error");
    }else if(txt_fin == ""){
      sweetAlert("Error", "Debe ingresar fecha de Fin", "error");
    }else if(txt_fin < txt_inicio){
      sweetAlert("Error", "Fecha de Fin debe ser mayor a fecha de Inicio", "error");
    }else if(txt_nombre_cliente == "" && txt_nombre_empresa == "" ){
			sweetAlert("Error", "Escriba nombre del cliente", "error");
		}else{
			$("form").attr('action','reporte_usuario.php');
      $("form").submit();
    }
  }
</script>

<style media="screen">
  form{
    text-align: center;
  }
</style>

<script src="../js/vendor/jquery.js"></script>
<script src="../dist/sweetalert-dev.js"></script>
<script src="../js/table.min.js"></script>
<link rel="stylesheet" href="../dist/sweetalert.css">




<script>
(function($){
$(document).ready(function(){

$('#cssmenu li.active').addClass('open').children('ul').show();
	$('#cssmenu li.has-sub>a').on('click', function(){
		$(this).removeAttr('href');
		var element = $(this).parent('li');
		if (element.hasClass('open')) {
			element.removeClass('open');
			element.find('li').removeClass('open');
			element.find('ul').slideUp(200);
		}
		else {
			element.addClass('open');
			element.children('ul').slideDown(200);
			element.siblings('li').children('ul').slideUp(200);
			element.siblings('li').removeClass('open');
			element.siblings('li').find('li').removeClass('open');
			element.siblings('li').find('ul').slideUp(200);
		}
	});

});
})(jQuery);
</script>

<script>
var cant_per  = $("tr").size()-1;
  function registrarPersonal(){
		window.location = "registrar_personal.php";
	}
	function actualizarPersonal(){
		var value = $("input[name=id_persona]:checked").val()
		if(value == null){
			sweetAlert("Error", "Elija el personal que desee actualizar", "error");
		}else{
			window.location = "editar_personal.php?id="+value;
		}

	}
	function bloquearPersonal(){
		var value = $("input[name=id_persona]:checked").val()
		if(value == null){
			sweetAlert("Error", "Elija el personal que desee bloquear", "error");
		}else{
			var cont = 0;
			while(cont <= cant_per){
				var id = $("tr").eq(cont).data('id');
				if(id == value){
					var nom = $("tr").eq(cont).data('nombre');
					var est = $("tr").eq(cont).data('estado');
				}
				cont++;
			}
			if(est == '1'){
			swal({title: '¿Esta seguro?',   text: 'Bloqueará al personal  : '+nom,   type: 'warning',   showCancelButton: true, cancelButtonText: 'Cancelar',   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Bloquear',   closeOnConfirm: false }, function(){   swal('Hecho!', 'Se bloqueó el personal', 'success');
				setTimeout(function() {window.location.replace('BloquearPersonal.php?id='+value);},1500);});
			}else{
				sweetAlert("Error", "Este personal  ya se encuentra bloqueado", "error");
			}
				/**
				-- Metodo de modificación del producto value --
				**/
			}
	}
	function habilitarPersonal(){

		var value = $("input[name=id_persona]:checked").val()
		if(value == null){
			sweetAlert("Error", "Elija el personal que desee habilitar", "error");
		}else{
			var cont = 0;
			while(cont <= cant_per){
				var id = $("tr").eq(cont).data('id');
				if(id == value){
					var nom = $("tr").eq(cont).data('nombre');
					var est = $("tr").eq(cont).data('estado');
				}
				cont++;
			}
			if(est == '2'){
			swal({title: '¿Esta seguro?',   text: 'Hablilitará al personal  : '+nom,   type: 'warning',   showCancelButton: true, cancelButtonText: 'Cancelar',   confirmButtonColor: '#009688',   confirmButtonText: 'Habilitar',   closeOnConfirm: false }, function(){   swal('Hecho!', 'Se habilitó el personal', 'success');
				setTimeout(function() {window.location.replace('HabilitarPersonal.php?id='+value);},1500);});
			}else{
				sweetAlert("Error", "Este personal  ya se encuentra habilitado", "error");
			}
				/**
				-- Metodo de modificación del producto value --
				**/
			}

	}

	$(document).ready(function(){
	    $('.rg-table').DataTable();
	});

</script>
<style>
label{
	color: #af2124;
	font-size: 1.5em;}
}

.dataTables_info{
	color: #af2124 !important;
font-size: 1.5em !important;

}
</style>

<script type="text/javascript">
var frmGoTest = $("#goTest");
var ihfrom = $("input[name=txt_inicio]");
var ihto = $("input[name=txt_fin]");
var ihfrom1 = $("input:hidden[name=from]");
var ihto1 = $("input:hidden[name=to]");

function sendTest(){
	console.log(ihfrom,ihto);
	if(ihfrom.val() === "" || ihto.val() === ""){
		swal("Error","Te falta especificar un rango de fecha","error");
	}else if(ihfrom.val() >= ihto.val()){
		sweetAlert("Error", "Fecha de Fin debe ser mayor a fecha de Inicio", "error");
	}else{
		ihfrom1.val(ihfrom.val()+" 00:00:00");
		ihto1.val(ihto.val()+" 00:00:00");

		frmGoTest.submit();
	}
}
$(function () {


  $(document).ready(function () {


      // Build the chart
      Highcharts.chart('container', {
          chart: {
              plotBackgroundColor: null,
              plotBorderWidth: null,
              plotShadow: false,
              type: 'pie'
          },
          title: {
              text: 'Ventas por categoría'
          },
          tooltip: {
              pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
          },
          plotOptions: {
              pie: {
                  allowPointSelect: true,
                  cursor: 'pointer',
                  dataLabels: {
                      enabled: false
                  },
                  showInLegend: true
              }
          },
          series: [{
              name: 'Brands',
              colorByPoint: true,
              data: [
                <?php
                $sql_categorias = "SELECT * FROM categoria";
                $data_categorias = $db->query($sql_categorias);
                while($fila = mysqli_fetch_assoc($data_categorias)){
                  ?>
                  {
                    name: '<?php echo $fila['NOMBRECATEGORIA']?>',
                    y: 56.33
                  },
                  <?
                }
                ?>
                ]
          }]
      });
  });
});
</script>

</body>
</html>
