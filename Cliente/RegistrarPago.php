<?php
//error_reporting(E_ALL);
if ($_POST) {
  require '../libs/req/library/Requests.php';
  Requests::register_autoloader();
  require '../libs/culqi/lib/culqi.php';
  include ('../src/conexionBD.php');

$nombre = $_POST['txt_nombre_receptor'];
$direccion = $_POST['txt_direccion_receptor'];
$telefono = $_POST['txt_telefono_receptor'];
$correo = $_POST['txt_correo_receptor'];
$descripcion = $_POST['txt_descripcion'];
$distrito = $_POST['txt_distrito'];
$txt_fec_envio = $_POST['txt_fec_envio'];
$txt_precio_total = $_POST['txt_precio_total'];
$txt_subtotal = $_POST['txt_subtotal'];
$txt_igv = $_POST['txt_igv'];
$token_culqi = $_POST["token_culqi"];
$estado = "A";
$mensaje_disc = "A";
$fec_entrega = substr($txt_fec_envio,0,10)." ".substr($txt_fec_envio,11,5).":00";
if(!isset($_SESSION)){
session_start();
}
//print_r($_SESSION);

$SECRET_API_KEY ="F0GiBFefqGmY52tzzxrI2jg81uhqAHUWHBmo7OfbOZA=";
$culqi = new Culqi\Culqi(array('api_key' => $SECRET_API_KEY));
//print_r($culqi);
// Entorno: Integración (pruebas)
$culqi->setEnv("INTEG");

$pedidoId = time()."comercio";



// Creando Cargo
try {
  $figi = array(
          "moneda"=> "PEN",
          "monto"=> (int)$txt_precio_total*100,
          "usuario"=> $_SESSION['id_Persona']."",
          "descripcion"=> $descripcion,
          "pedido"=> $pedidoId,
          "codigo_pais"=> "PE",
          "direccion"=> $direccion,
          "ciudad"=> "LIMA",
          "telefono"=> "989583478",
          "nombres"=> $nombre,
          "apellidos"=> "CLI",
          "correo_electronico"=> $correo,
          "token"=> $token_culqi
        );
      //  print_r($figi);
    $cargo = $culqi->Cargos->create($figi);
      //      print_r($cargo);
} catch(Exception $e) {
    // ERROR: El cargo tuvo algún error o fue rechazado
    //echo $e->getMessage();

}




$id_cliente =  $_SESSION['id_Persona'];
$id_estado_pedido = 3;
$fechaemision = date('Y-m-d h:i:s', time());


$sql_insertar_pedido = "INSERT INTO pedido
(`ID_CLIENTE`, `ID_ESTADOPEDIDO`, `FECHAEMISION`, `FECHAENTREGA`, `SUBTOTAL`, `IGV`, `MONTOTOTAL`, `DIRECCIONENTREGA`, `DESCRIPCIONNOTA`, `ID_COSXDIS`, `ESTADO`, `MENSAJE_DISC`)
VALUES ('".$id_cliente."', '3', '".$fechaemision."', '".$fec_entrega."', '".$txt_subtotal."', '".$txt_igv."', '".$txt_precio_total."', '".$direccion."', '".$descripcion."', '".$distrito."', '".$estado."', '".$mensaje_disc."');";

$db->query($sql_insertar_pedido);


$sql_max_pedido = "SELECT  MAX(ID_PEDIDO) AS max from pedido;";
$data_max = $db->query($sql_max_pedido);
$max_pedido = 0;
while($fila = mysqli_fetch_assoc($data_max)){
    $max_pedido = $fila['max'];
}

$numerocomprobante = "SELECT LPAD(NUMCOMP+1,6,'0') AS resultado  FROM comprobante WHERE TIPOCOMPROBANTE = 'BOLETA' AND ID_NUMCOMPROBANTE = (SELECT MAX(ID_NUMCOMPROBANTE) FROM comprobante where TIPOCOMPROBANTE = 'BOLETA');";
$data_comprobante  = $db->query($numerocomprobante);
$numeracion = "";
while($fila = mysqli_fetch_assoc($data_comprobante)){
    $numeracion = $fila['resultado'];
}

$sql_comprobante = "INSERT INTO comprobante (`TIPOCOMPROBANTE`, `FECHACOMPROBANTE`, `ID_PEDIDO`, `SERIECOMP`, `NUMCOMP`)
VALUES ('BOLETA', '".$fechaemision."', '".$max_pedido."', 'B001', '".$numeracion."');";
$db->query($sql_comprobante);


$carrito = $_SESSION['carrito'];

$llaves = array_keys($carrito);


foreach ($llaves as &$valor) {
$tmp_sql = "SELECT * FROM producto WHERE ID_PRODUCTO = ".$valor;
$data_tmp = $db->query($tmp_sql);
while($fila = mysqli_fetch_assoc($data_tmp)){
  $cantidad = $carrito[$valor];
  $id_producto = $valor;
  $preciototalprod = $fila['PRECIOPRODUCTO'] * $carrito[$valor];
  $sql_producto = "INSERT INTO detalle_pedido (`ID_PEDIDO`,`ID_PRODUCTO`, `CANTIDAD`, `PRECIOTOTALPROD`) VALUES ('".$max_pedido."', '".$id_producto."', '".$cantidad."', '".$preciototalprod."');";
  //echo $sql_producto;
  $db->query($sql_producto);
  //echo $id_producto."<br><hr>";
  $sql_stock_actual = "SELECT STOCK FROM producto WHERE ID_PRODUCTO = ".$id_producto;
  //echo($sql_stock_actual);
  $data_stock = $db->query($sql_stock_actual);
  //print_r($data_stock);
  while($row = mysqli_fetch_assoc($data_stock)){
    //print_r($row);
    $stock = $row['STOCK'];
  }
  $stock_now = $stock - $cantidad;
  $sql_refresh_stock = "UPDATE producto SET `STOCK`='".$stock_now."' WHERE `ID_PRODUCTO`='".$valor."';";
  $db->query($sql_refresh_stock);
}

}


header("Location: registro_satisfactorio.php?id=".$max_pedido);


}
 ?>
