  <?php
  include ('../src/conexionBD.php');
  $sql_last4 = "SELECT * FROM producto WHERE TIPOPRODUCTO = 'ARREGLO' ORDER BY ID_PRODUCTO DESC";
  $data_productos = $db->query($sql_last4);
  if(!isset($_SESSION)){
  session_start();
  }
  $carrito = $_SESSION['carrito'];
  ?>
  <!doctype html>
  <html class="no-js" lang="en">
    <head>
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <title>Floreria</title>
      <link rel="stylesheet" href="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.min.css">
      <link rel="stylesheet" href="../css/style.css" >
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    </head>
    <body>


      <div class="top-bar">
        <div class="top-bar-left">
          <ul id="rg-img-logo" class="menu">
            <img style="width:150px" src="../img/logo.png" alt="">
          </ul>
        </div>
        <div class="top-bar-right">
          <ul id="rg-lista-top" class="menu">
            <li><a href="index.php">INICIO</a></li>
            <li><a href="catalogo.php">CATÁLOGO</a></li>
            <li><a href="realizarDiseno.php">REALIZAR DISEÑO</a></li>
            <li><a href="carrito.php">MI CARRITO</a></li>
          </ul>
        </div>
      </div>


      <div class="row column text-center">
      <br>
        <h2 class="rg-titulo-index">Mi carrito</h2>
        <hr>
      </div>

      <div class="row">
        <?php
          if($carrito){
            ?>

            <table>
              <tr class="tr_header">
                <th>Nombre</th>
                <th>Cantidad</th>
                <th>Precio Unitario / Con IGV</th>
                <th>Total</th>
                <th>Eliminar</th>
              </tr>




                <?php
              $cont_total = 0;

              		$llaves = array_keys($carrito);


              foreach ($llaves as &$valor) {
                  $tmp_sql = "SELECT * FROM producto WHERE ID_PRODUCTO = ".$valor;
                  $data_tmp = $db->query($tmp_sql);
                  while($fila = mysqli_fetch_assoc($data_tmp)){
                ?>
                <tr>
                  <?php
                  $cont_total+= $fila['PRECIOPRODUCTO'] * $carrito[$valor];
                  ?>
                  <td style="text-align:center"><?php echo $fila['NOMBREPRODUCTO']?></td>
                  <td style="text-align:center"><?php echo $carrito[$valor]?></td>
                  <td style="text-align:center"><?php echo $fila['PRECIOPRODUCTO']?></td>
                  <td style="text-align:center"><?php echo $fila['PRECIOPRODUCTO'] * $carrito[$valor]?></td>
                  <td style="text-align:center"><a style="color:#b83135" onclick="eliminarCarrito(<?php echo $valor?>)"><i class="material-icons">delete</i></a></td>
                </tr>
                <?php
              }
              }

                ?>



            </table>

            <h3>Total a pagar : <?php echo $cont_total?></h3>
            <?php if($_SESSION['id_Persona'] != null){ ?>
            <input type="button" id="miBoton" name="name" value="Pagar" class="rg-btn-primary " style="float:right">
            <?php
          }else{
             ?>
             <div class="">
               <input type="button" id="loggearse2" name="name" value="Pagar" class="rg-btn-primary " style="float:right">
             </div>
             <?php }  ?>
            <?
          }else{
            ?>
            <p class="rg_show_messg">
              Actualmente no tiene ningun producto seleccionado.
            </p>




            <?
          }
         ?>
      </div>

      <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>

      <script src="../dist/sweetalert-dev.js"></script>
      <link rel="stylesheet" href="../dist/sweetalert.css">





  <script>
  $('#realizarEnvio').on('click', function (e) {
      // Abre el formulario con las opciones de Culqi.configurar
      window.location.replace('realizarEnvio.php');
  });
     $('#miBoton').on('click', function (e) {
         // Abre el formulario con las opciones de Culqi.configurar
         window.location.replace('pagar.php');
     });
     $('#loggearse').on('click', function (e) {
         // Abre el formulario con las opciones de Culqi.configurar
         window.location.replace('login.php');
     });

     $('#loggearse2').on('click', function (e) {
         // Abre el formulario con las opciones de Culqi.configurar
         window.location.replace('login.php');
     });

  </script>


  <script>
  function eliminarCarrito(id){
    swal("Hecho", "Se eliminó el producto", "success");
swal({   title: "¿Estas seguro?",   text: "¿Estas seguro de eliminar el producto?",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Si",   closeOnConfirm: false }, function(){   swal("Hecho!", "Se elimino el producto del carrito", "success"); setTimeout(function() {window.location.replace('EliminarCarrito.php?id='+id);},1500); });




  }
  // Ejemplo: Tratando respuesta con AJAX (jQuery)
  function culqi() {

      if(Culqi.error){
         // Mostramos JSON de objeto error en consola





         sweetAlert("Oops...", Culqi.error.mensaje , "error");


      }
      else{
        swal("Good job!", "Se realizó la transferencia con exito", "success");
         /**$.post("/tarjeta", // Ruta hacia donde enviaremos el token vía POST
          {token: Culqi.token.id},
          function(data, status){
              if (data=='ok') {
                  alert('¡Todo en orden! Token enviado.');
              } else {
                  alert('Error');
              }
          });
          **/
         }
  };
  </script>

      <script src="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js"></script>

  <script>
    $( "#id_buscador" ).keypress(function() {
        var text = $("#id_buscador").val();
        text = text.toUpperCase();
        if(text == ''){
          $(".rg_ocultar").removeClass("rg_ocultar");
        }else{
        $(".rg_producto").addClass("rg_ocultar");
        $('*[data-nombre^='+text+']').removeClass("rg_ocultar");
        }

    });
  </script>

  <style>
  .rg_ocultar{
    display: none;
  }
  @media screen and (max-width: 500px) {
    #rg-lista-top li{
        display:inline;
        text-align: center;
    }
    #rg-lista-top li a:hover{
      background-color: #bf031c;
      color:white;
    }
    #rg-img-logo{
      text-align: center;
    }
  }
  #rg-lista-top{
    margin-top: 35px;
  }
  #rg-lista-top li a{
    color: #bf031c;
  }
  .rg_show_messg{
    color: #af2124;
    font-size: 2em;
  }
  table{
    width:100%;
  }
  .tr_header{
    background-color: #af2124 !important;
    color:white !important;
  }
  </style>




      <script>
        $(document).foundation();
      </script>
    </body>
  </html>
