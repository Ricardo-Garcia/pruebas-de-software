<?php
include ('../src/conexionBD.php');
$sql_last4 = "SELECT * FROM producto WHERE TIPOPRODUCTO = 'ARREGLO' AND ESTADO = 'A' ORDER BY ID_PRODUCTO DESC LIMIT 4";
$data_last4 = $db->query($sql_last4);
?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Floreria</title>
    <link rel="stylesheet" href="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.min.css">
    <link rel="stylesheet" href="../css/style.css" >
  </head>
  <body>


    <div class="top-bar">
      <div class="top-bar-left">
        <ul id="rg-img-logo" class="menu">
          <img style="width:150px" src="../img/logo.png" alt="">
        </ul>
      </div>
      <div class="top-bar-right">
        <ul id="rg-lista-top" class="menu">
          <li><a href="index.php">INICIO</a></li>
          <li><a href="catalogo.php">CATÁLOGO</a></li>
          <li><a href="realizarDiseno.php">REALIZAR DISEÑO</a></li>
          <li><a href="#quienes_somos">¿QUIÉNES SOMOS?</a></li>
          <li><a href="#contacto">CONTACTO</a></li>
          <li><a href="carrito.php">VER CARRITO</a></li>
        </ul>
      </div>
    </div>
    <div class="orbit" role="region" aria-label="Favorite Space Pictures" data-orbit>
      <ul class="orbit-container">
        <button class="orbit-previous" aria-label="previous"><span class="show-for-sr">Previous Slide</span>&#9664;</button>
        <button class="orbit-next" aria-label="next"><span class="show-for-sr">Next Slide</span>&#9654;</button>
        <li class="orbit-slide is-active">
          <img src="../img/banner1.jpg" style="width:100%">
        </li>
        <li class="orbit-slide">
          <img src="../img/banner1.jpg" style="width:100%">
        </li>
        <li class="orbit-slide">
          <img src="../img/banner1.jpg" style="width:100%">
        </li>
        <li class="orbit-slide">
          <img src="../img/banner1.jpg" style="width:100%">
        </li>
      </ul>
    </div>

    <div class="row column text-center">
    <br>
      <h2 class="rg-titulo-index">Nuestros nuevos productos</h2>
      <hr>
    </div>

    <div class="row small-up-2 large-up-4">
      <?php
      while($fila = mysqli_fetch_assoc($data_last4)){
      ?>
      <div class="column">
        <img class="thumbnail" src="../img/Productos/<?php echo $fila['IMAGEN']?>">
        <h5><?php echo $fila['NOMBREPRODUCTO']?></h5>
        <?php
        if($fila['DESCUENTO'] != 0){
          ?>
        <p>Antes : <strike><span class="rg-soles">s/. </span><?php echo $fila['PRECIOPRODUCTO']?></strike></p>
        <p>Ahora! : <span class="rg-soles">s/. </span><?php echo $fila['PRECIOPRODUCTO']*(100-$fila['DESCUENTO'])/100?></p>
        <?php
        }else{
          ?>
          <p>Precio regular:<strike><span></span></strike></p>
          <p><span class="rg-soles">s/.</span> <?php echo $fila['PRECIOPRODUCTO']?></p>

          <?php
        }
        ?>

        <a href="detalle_producto.php?id=<?php echo $fila['ID_PRODUCTO']?>" class="button expanded rg-boton-comprar">Ver detalle</a>
      </div>
      <?php
      }
      ?>

    </div>

    <hr>

    <div class="row column">
      <div class="callout primary rg-detalle">
        <h3>Un detalle con estilo</h3>
      </div>
    </div>

    <hr>

    <div class="row column text-center">
      <h2 id="quienes_somos">¿Quiénes sómos?</h2>
      <hr>
    </div>

    <div class="row">

<p>Marys Florería S.A.C es una empresa creada para  usted  donde encontrará diseños florales con estilo y calidad, además de combinaciones sofisticadas y sencillas que capturan texturas, líneas y formas dando como resultado la esencia de las tendencias clásicas y modernas.</p>

<p>Marys Florería SAC. Les ofrece diseños que dan importancia y elegancia a cada una de las flores y follajes de viveros especialmente seleccionados para Ud.</p>

<p>En todo momento buscamos captar sus preferencias y ofrecerle el mejor producto acompañado de un servicio eficiente. Nuestra amplia experiencia y conocimiento en el arte de las flores, nos permite ofrecerle los mejores arreglos para toda ocasión.</p>

<p>Complementamos nuestra oferta con distintos adicionales entre los que se destacan chocolates, peluches, globos, bebidas, etc… que harán de su detalle un recuerdo inolvidable.</p>

<p>Llámanos y convénzase de que la belleza y el estilo de Marys Florería S.A.C  le darán los mejores resultados a un precio sin competencia.</p>

<p>Agradecemos su confianza.</p>

<p>La Administración.</p>

    </div>

<hr>
<div class="row column text-center">
      <img  id="contacto" src="https://www.marysfloreria.com/wp-content/uploads/2016/07/encuentranos-300x82.png">
      <hr>
    </div>

<div class="row">
<center class="columns small-12 large-6 medium-6">
<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d796.8534174184142!2d-76.9414974645074!3d-12.073424620898969!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xfc83671c34044a1f!2sMarys+Florer%C3%ADa!5e0!3m2!1ses!2spe!4v1466999622164" width="90%" height="320" frameborder="0" style="border:0" allowfullscreen=""></iframe>
</center>
<center class="columns small-12 large-6 medium-6">
  <img src="https://www.marysfloreria.com/wp-content/uploads/2016/07/ENTRADA-TIENDA-19-07-16.png">
</center>

</div>

    <footer class="row">
<p>Dirección</p>
<p>Av. La Fontana 1295, La Molina Lima, Perú</p>
<p>Ponte en contacto</p>
<p>E-mail: ventas@marysfloreria.com Phone: (01) 3481601</p>
    </footer>



    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js"></script>

<style>
@media screen and (max-width: 500px) {
  #rg-lista-top li{
      display:inline;
      text-align: center;
  }
  #rg-lista-top li a:hover{
    background-color: #bf031c;
    color:white;
  }
  #rg-img-logo{
    text-align: center;
  }
}
#rg-lista-top{
  margin-top: 35px;
}
#rg-lista-top li a{
  color: #bf031c;
}
</style>




    <script>
      $(document).foundation();
    </script>
  </body>
</html>
