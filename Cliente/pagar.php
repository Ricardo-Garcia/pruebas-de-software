<?php
include ('../src/conexionBD.php');
$sql_last4 = "SELECT * FROM producto WHERE TIPOPRODUCTO = 'ARREGLO' ORDER BY ID_PRODUCTO DESC";
$data_productos = $db->query($sql_last4);
if(!isset($_SESSION)){
session_start();
}
$sql_cliente = "SELECT * FROM persona WHERE ID_PERSONA = ".$_SESSION['id_Persona'];
$data_cliente = $db->query($sql_cliente);
while($fila = mysqli_fetch_assoc($data_cliente)){
  $cliente = array(
    'nombre'=> $fila['NOMBRE'],
    'precio'=> $fila['APELLIDOPAT'],
    'stock'=> $fila['APELLIDOMAT'],
    'descuento'=> $fila['DIRECCION'],
    );
}

$sql_last_boleta = "SELECT * FROM comprobante
WHERE TIPOCOMPROBANTE = 'BOLETA'
AND ID_NUMCOMPROBANTE = (SELECT MAX(ID_NUMCOMPROBANTE) FROM comprobante WHERE TIPOCOMPROBANTE = 'BOLETA')
;";
$numcomprobante = "B001";
$data_last_boleta = $db->query($sql_last_boleta);
while($fila = mysqli_fetch_assoc($data_last_boleta)){
  $numeracion = $fila['NUMCOMP'];
}
$numeracion = $numeracion + 1;
$numeracion = str_pad($numeracion, 6, "0", STR_PAD_LEFT);
$boleta_generada =  $numcomprobante.$numeracion;

$carrito = $_SESSION['carrito'];

$sql_distritos= "SELECT tcd.ID_DISTRITO , td.NOMBREDISTRITO , tcd.COSTO FROM costo_distrito tcd, distrito td WHERE td.ID_DISTRITO=tcd.ID_DISTRITO";
$data_distritos = $db ->query($sql_distritos);


?>



<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Floreria</title>
    <link rel="stylesheet" href="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.min.css">
    <link rel="stylesheet" href="../css/style.css" >
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  </head>
  <body>


    <div class="top-bar">
      <div class="top-bar-left">
        <ul id="rg-img-logo" class="menu">
          <img style="width:150px" src="../img/logo.png" alt="">
        </ul>
      </div>
      <div class="top-bar-right">
        <ul id="rg-lista-top" class="menu">
          <li><a href="index.php">INICIO</a></li>
          <li><a href="catalogo.php">CATÁLOGO</a></li>
          <li><a href="realizarDiseno.php">REALIZAR DISEÑO</a></li>
          <li><a href="carrito.php">MI CARRITO</a></li>
        </ul>
      </div>
    </div>


  <form id="formulario" class="" action="RegistrarPago.php" method="post">

    <div class="row column text-center">
    <br>
      <h2 class="rg-titulo-index">Realizar pago</h2>
      <hr>
    </div>
    <div class=" row">
      <h3>Destinatario : </h3>
      <div class="columns small-12 medium-6 large-6">
          <h4>Receptor : </h4>
          <input type="text" name="txt_nombre_receptor" id="txt_nombre_receptor" value="" placeholder="Ingrese nombre y apellidos">
          <h4>Direccion de entrega : </h4>
          <input type="text" name="txt_direccion_receptor" id="txt_direccion_receptor" value="" placeholder="Ingrese dirección de entrega">
          <h4>Descripcion de nota : </h4>
          <textarea id="txt_descripcion"   name="txt_descripcion" rows="8" cols="10"></textarea>
      </div>
      <div class="columns small-12 medium-6 large-6">
          <h4>Telefono : </h4>
          <input type="text" name="txt_telefono_receptor" id="txt_telefono_receptor" value="" placeholder="Ingrese numero telefónico (Opcional)">
          <h4>Correo : </h4>
          <input type="text" name="txt_correo_receptor" id="txt_correo_receptor" value="" placeholder="Ingrese correo electrónico (Opcional)">
          <h4>Distrito de envio : </h4>
          <select class="" name="txt_distrito" onchange="actualizarPrecioDelivery(this)">
            <?php   while($fila = mysqli_fetch_assoc($data_distritos)){?>
              <option value="<?php echo $fila['ID_DISTRITO'] ?>" precio="<?php echo $fila['COSTO']?>"> <?php echo  $fila['NOMBREDISTRITO'] ."  S/" .$fila['COSTO']?>  </option>
            <?php  } ?>
          </select>
          <h4>Fecha de envio</h4>
          <input id="txt_fec_envio" name="txt_fec_envio" type="datetime-local" name="" value="">
      </div>
    </div>
    <div class="row">
      <?php
        if($carrito){
          ?>

          <table>
            <tr class="tr_header">
              <th>Nombre</th>
              <th>Cantidad</th>
              <th>Precio Unitario / Con IGV</th>
              <th>Total</th>
            <!--  <th>Eliminar</th> -->
            </tr>




              <?php
            $cont_total = 0;

                $llaves = array_keys($carrito);


            foreach ($llaves as &$valor) {
                $tmp_sql = "SELECT * FROM producto WHERE ID_PRODUCTO = ".$valor;
                $data_tmp = $db->query($tmp_sql);
                while($fila = mysqli_fetch_assoc($data_tmp)){
              ?>
              <tr>
                <?php
                $cont_total+= $fila['PRECIOPRODUCTO'] * $carrito[$valor];
                ?>
                <td style="text-align:center"><?php echo $fila['NOMBREPRODUCTO']?></td>
                <td style="text-align:center"><?php echo $carrito[$valor]?></td>
                <td style="text-align:center"><?php echo $fila['PRECIOPRODUCTO']?></td>
                <td style="text-align:center"><?php echo $fila['PRECIOPRODUCTO'] * $carrito[$valor]?></td>
            <!--    <td style="text-align:center"><a style="color:#b83135" href="EliminarCarrito.php?id=<?php // echo $valor?>"><i class="material-icons">delete</i></a></td> -->
              </tr>
              <?php
              }
            }
              ?>
          </table>
          <div class="" style="float:right;text-align:right">
            <h3>IGV : <?php echo round($cont_total * 0.18,2) ?></h3>
            <h3>Delivery : <span id="rg_precio">60.00</span> </h3>
            <h3>Sub-Total : <span id="rg_precio_subtotal"> <?php echo round($cont_total -  ($cont_total * 0.18) , 2 ) ?>    </span></h3>
            <h3>Total a pagar : <span id="rg_precio_total"><?php echo $cont_total + 60.00?></span></h3>
          </div>
          <div class="clearfix">

          </div>
          <?php if($_SESSION['id_Persona'] != null){ ?>

          <input type="button" id="miBoton" name="name" value="Pagar" class="rg-btn-primary " style="float:right">
          <?php
        }else{
           ?>
           <div class="">
             <input type="button" id="loggearse" name="name" value="Realizar envio" class="rg-btn-primary " style="float:right">
             <input type="button" id="loggearse2" name="name" value="Pagar" class="rg-btn-primary " style="float:right">
           </div>
           <?php }  ?>
          <?
        }else{
          ?>
          <p class="rg_show_messg">
            Actualmente no tiene ningun producto seleccionado.
          </p>
          <?
        }
       ?>
    </div>
    <input type="hidden" name="txt_subtotal" value="<?php echo round($cont_total -  ($cont_total * 0.18) , 2 ) ?>">
    <input type="hidden" name="txt_igv" value="<?php echo round($cont_total * 0.18,2) ?>">
    <input type="hidden" name="token_culqi" id="token_culqi" value="">
    <input type="hidden" id="txt_precio_total" name="txt_precio_total" value="<?php echo ($cont_total + 60 )?>">
  </form>


    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>


    <script>

    var fec_actual;
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();



    if(mm<10) {
        mm='0'+mm
    }
    dd = parseInt(dd)+2;
    if(dd<10) {
        dd='0'+dd
    }

    fec_actual = yyyy+'-'+mm+'-'+dd;
</script>




<script>
$('#realizarEnvio').on('click', function (e) {
    window.location.replace('realizarEnvio.php');
});
   $('#miBoton').on('click', function (e) {
       var txt_nombre_receptor = $("#txt_nombre_receptor").val();
       var txt_direccion_receptor = $("#txt_direccion_receptor").val();
       var txt_telefono_receptor = $("#txt_telefono_receptor").val();
       var txt_correo_receptor = $("#txt_correo_receptor").val();
       var txt_descripcion = $("#txt_descripcion").val();

       if(txt_nombre_receptor == ""){
        sweetAlert("Error", "Ingrese nombre" , "error");
       }else if(txt_direccion_receptor ==""){
         sweetAlert("Error", "Ingrese direccion" , "error");
       }else if (txt_correo_receptor =="") {
         sweetAlert("Error", "Ingrese correo" , "error");
       }else if (/^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i.test(txt_correo_receptor) === false) {
         sweetAlert("Error", "Ingrese correo válido" , "error");
       }else if(txt_descripcion ==""){
         sweetAlert("Error", "Ingrese descripcion" , "error");
       }else if ($("#txt_fec_envio").val() =="") {
        sweetAlert("Error", "Ingrese fecha de envio" , "error");
      }else if (  $("#txt_fec_envio").val().substring(0,10) < fec_actual ) {
         sweetAlert("Error", "Reserva se realiza con 2 días de anticipación" , "error");
       }else if (txt_telefono_receptor !="") {
         if( txt_telefono_receptor.length != 7  && txt_telefono_receptor.length != 9 ){
           sweetAlert("Error", "Ingrese de telefono entre  7 a 9 digitos" , "error");
         }else{
        //$("#formulario").submit();
        Culqi.abrir();
         }

       }else{
           //$("#formulario").submit();
           Culqi.abrir();
       }

   });
   $('#loggearse').on('click', function (e) {
       window.location.replace('login.php');
   });

   $('#loggearse2').on('click', function (e) {
       window.location.replace('login.php');
   });

</script>



    <script src="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js"></script>

<script>
var actual_delivery = 60;
var precio_final = <?php echo $cont_total ?> + 60;
function actualizarPrecioDelivery(e){
  var precio_nuevo = e.options[e.selectedIndex].getAttribute('precio');
  $("#rg_precio").html(precio_nuevo);
  var precio_disminuir = parseInt($("#rg_precio_total").text()) - parseInt(actual_delivery);
  $("#rg_precio_total").html(precio_disminuir);
  var precio_total_nuevo = parseInt($("#rg_precio_total").text()) + parseInt(precio_nuevo);
  $("#rg_precio_total").html(precio_total_nuevo);
  $("#txt_precio_total").val(precio_total_nuevo);
  precio_final = precio_total_nuevo;
  actual_delivery = precio_nuevo;
}
  $( "#id_buscador" ).keypress(function() {
      var text = $("#id_buscador").val();
      text = text.toUpperCase();
      if(text == ''){
        $(".rg_ocultar").removeClass("rg_ocultar");
      }else{
      $(".rg_producto").addClass("rg_ocultar");
      $('*[data-nombre^='+text+']').removeClass("rg_ocultar");
      }

  });
</script>

<style>
.rg_ocultar{
  display: none;
}
@media screen and (max-width: 500px) {
  #rg-lista-top li{
      display:inline;
      text-align: center;
  }
  #rg-lista-top li a:hover{
    background-color: #bf031c;
    color:white;
  }
  #rg-img-logo{
    text-align: center;
  }
}
#rg-lista-top{
  margin-top: 35px;
}
#rg-lista-top li a{
  color: #bf031c;
}
.rg_show_messg{
  color: #af2124;
  font-size: 2em;
}
table{
  width:100%;
}
.tr_header{
  background-color: #af2124 !important;
  color:white !important;
}
</style>
<script src="../dist/sweetalert-dev.js"></script>
<link rel="stylesheet" href="../dist/sweetalert.css">




    <script>
      $(document).foundation();
    </script>
    <script src="https://integ-pago.culqi.com/js/v1"></script>
    <script>
   Culqi.codigoComercio = 'test_fxr7pNvyA98L';
</script>
<script>
    var conf_123 = {
        nombre: 'Floreria Marys',
        orden: '<?php echo $boleta_generada?>',
        moneda: 'PEN',
        descripcion: 'Pago de arreglo',
        monto: precio_final * 100,
        guardar: false
    };
    Culqi.configurar(conf_123);

</script>

<script>
// Recibimos Token del Culqi.js
function culqi() {

    if (Culqi.token) {
      // Imprimir Token

      console.log(Culqi.token.id);
      $("#token_culqi").val(Culqi.token.id);
      $("#formulario").submit();
    }

    else{
      // Hubo un problema...
      // Mostramos JSON de objeto error en consola
      console.log(Culqi.error);
      alert(Culqi.error.mensaje);
    }

};
</script>
  </body>
</html>
