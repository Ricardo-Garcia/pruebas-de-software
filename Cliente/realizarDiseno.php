<?php
include ('../src/conexionBD.php');


?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Floreria</title>
    <link rel="stylesheet" href="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.min.css">
    <link rel="stylesheet" href="../css/style.css" >
  </head>
  <body>


    <div class="top-bar">
      <div class="top-bar-left">
        <ul id="rg-img-logo" class="menu">
          <img style="width:150px" src="../img/logo.png" alt="">
        </ul>
      </div>
      <div class="top-bar-right">
        <ul id="rg-lista-top" class="menu">
          <li><a href="index.php">INICIO</a></li>
          <li><a href="catalogo.php">CATÁLOGO</a></li>
          <li><a href="realizarDiseno.php">REALIZAR DISEÑO</a></li>
            <li><a href="carrito.php">VER CARRITO</a></li>
        </ul>
      </div>
    </div>


    <div class="row column text-center">
    <br>
      <h2 class="rg-titulo-index">Realizar Diseño</h2>
      <hr>
    </div>

    <div class="container">

      <div class="columns small-12 medium-5 large-5">
        <h3>Elige el modelo que desees!</h3>
        <h5>Modelos : </h5>
        <div class="row">
          <form id="formulario" class="" action="registrarDiseno.php" method="post">
            <input type="hidden" name="txt_json" id="txt_json" value="">

          <?php
          $sql_modelos = "SELECT * FROM producto WHERE DESCRIPCION = 'MODELO'";
          $data_modelos = $db->query($sql_modelos);
          while($fila = mysqli_fetch_assoc($data_modelos)){
            ?>
            <div class="columns small-12 medium-4 large-4 rg_modelo">
                <img style="height:200px;" src="../img/Productos/<?php echo $fila['IMAGEN']?>" alt="<?php echo $fila['NOMBREPRODUCTO']?>" />
                <p><?php echo $fila['PRECIOPRODUCTO'] ?></p>
                <input onclick="seleccionarModelo(this)" data-nombre="<?php echo $fila['NOMBREPRODUCTO']?>" data-precio="<?php echo $fila['PRECIOPRODUCTO']?>" type="radio" name="modelo" value="<?php echo $fila['ID_PRODUCTO']?>"> <?php echo $fila['NOMBREPRODUCTO']?>
            </div>
            <?
          }
           ?>
           </form>
        </div>
      </div>
      <div class="columns small-12 medium-7 large-7">
        <h3>Elige los productos que desees!</h3>
        <div class="row">
          <?php
          $sql_categorias = "SELECT * FROM categoria WHERE ID_CATEGORIA = 1 OR ID_CATEGORIA = 3 OR ID_CATEGORIA = 11 OR ID_CATEGORIA = 12";
          $data_categorias  = $db->query($sql_categorias);

           ?>



           <ul class="tabs" data-tabs id="example-tabs">

             <?php
             $cont = 0;
             while($fila = mysqli_fetch_assoc($data_categorias)){
                  if($cont++ == 0){
                    ?>
                    <li class="tabs-title is-active"><a href="#panel<?php echo $fila['ID_CATEGORIA']?>" aria-selected="true"><?php echo $fila['NOMBRECATEGORIA']?></a></li>
                    <?
                  }else{
                    ?>
                    <li class="tabs-title"><a href="#panel<?php echo $fila['ID_CATEGORIA']?>"><?php echo $fila['NOMBRECATEGORIA']?></a></li>
                    <?php
                  }
               ?>

               <?
             }
              ?>


            </ul>
            <div class="tabs-content" data-tabs-content="example-tabs">
              <?php
              $data_categorias2 = $db->query($sql_categorias);

              $cont = 0;
              while($fila = mysqli_fetch_assoc($data_categorias2)){
                $sql_producto = "SELECT * FROM producto
                WHERE TIPOPRODUCTO = 'UNITARIO'
                AND STOCK > 0
                AND DESCRIPCION LIKE '%".$fila['NOMBRECATEGORIA']."%'
                ";
                $data_prod = $db->query($sql_producto);
                $data_prod1 = $db->query($sql_producto);
                $data_prod2 = $db->query($sql_producto);
                  $cont_prod = 0 ;
                    while($rowa = mysqli_fetch_assoc($data_prod)){
                   if($cont++ === 0){
                     $cont_prod = $cont_prod + 1;
                     ?>
                     <div class="tabs-panel is-active clearfix" id="panel<?php echo $fila['ID_CATEGORIA']?>">
                       <?php
                       while($row = mysqli_fetch_assoc($data_prod1)){
                        ?>
                       <center class="columns small-12 medium-4 large-4 item" data-itemcontainer="<?php echo $row['ID_PRODUCTO']?>">
                          <div onclick="mostrarCantidad(this);" class="rg_item" data-producto="<?php echo $row['ID_PRODUCTO']?>" >
                          <img src="../img/Productos/<?php echo $row['IMAGEN']?>" alt="<?php $row['NOMBREPRODUCTO']?>" />
                          <span class="rg_precio">S/. <?php echo $row['PRECIOPRODUCTO']?></span>
                          <p class="rg_nombre"><?php echo $row['NOMBREPRODUCTO']?></p>
                          </div>
                          <div class="rg_seleccionar" data-item="<?php echo $row['ID_PRODUCTO']?>">
                            <div class="columns small-12 medium-6 large-6">
                                Cantidad :
                            </div>
                            <div class="columns small-12 medium-6 large-6">
                                <input onkeyup="validarStock(this);return siSeaNumero(event);" data-producto="<?php echo $row['ID_PRODUCTO']?>" data-nombre="<?php echo $row['NOMBREPRODUCTO']?>" class="rg_input_precio" data-precio = "<?php echo $row['PRECIOPRODUCTO']?>" data-stock="<?php echo $row['STOCK']?>" type="number" data-id="<?php echo $row['ID_PRODUCTO']?>" name="" value="0">
                            </div>
                            <input onclick="cancelarProducto(<?php echo $row['ID_PRODUCTO']?>)" type="button" class="rg-btn-primary" name="name" value="Cancelar">
                          </div>
                       </center>

                       <?php
                      }
                       ?>

                     </div>
                     <?
                   }else if($cont_prod != 1){
                     $cont_prod = $cont_prod + 1;
                     ?>

                     <div class="tabs-panel clearfix" id="panel<?php echo $fila['ID_CATEGORIA']?>">

                       <?php
                       while($row = mysqli_fetch_assoc($data_prod2)){
                       ?>
                       <center class="columns small-12 medium-4 large-4 item" data-itemcontainer="<?php echo $row['ID_PRODUCTO']?>">
                         <div onclick="mostrarCantidad(this);" class="rg_item" data-producto="<?php echo $row['ID_PRODUCTO']?>" >
                           <img src="../img/Productos/<?php echo $row['IMAGEN']?>" alt="<?php $row['NOMBREPRODUCTO']?>" />
                           <span class="rg_precio">S/. <?php echo $row['PRECIOPRODUCTO']?></span>
                           <p class="rg_nombre"><?php echo $row['NOMBREPRODUCTO']?></p>
                         </div>
                         <div class="rg_seleccionar" data-item="<?php echo $row['ID_PRODUCTO']?>">
                          <div class="columns small-12 medium-6 large-6">
                              Cantidad :
                          </div>
                          <div class="columns small-12 medium-6 large-6">
                              <input data-id="<?php echo $row['ID_PRODUCTO']?>" data-nombre="<?php echo $row['NOMBREPRODUCTO']?>" onkeyup="validarStock(this);return siSeaNumero(event);" data-producto="<?php echo $row['ID_PRODUCTO']?>" class="rg_input_precio" data-precio = "<?php echo $row['PRECIOPRODUCTO']?>" data-stock="<?php echo $row['STOCK']?>" type="number" name="" value="0">
                              </div>
                          <input onclick="cancelarProducto(<?php echo $row['ID_PRODUCTO']?>)" type="button" class="rg-btn-primary" name="name" value="Cancelar">
                        </div>
                       </center>
                      <?php
                    }
                      ?>

                      </div>
                     <?php

                   }
                 }
                 if($cont_prod === 0){
                      ?>
                      <div class="tabs-panel" id="panel<?php echo $fila['ID_CATEGORIA']?>">
                       <h3>Por el momento no contamos con productos de esta categoría disponible</h3>
                       </div>
                      <?
                 }



              }
               ?>



</div>

        </div>
      <div class="row">
        <div class="columns small-12 medium-6 large-6">
          <h3>Total</h3>
          <div class="rg_precio_total">
             s/. <span id="rg_precio_ajax"></span>
          </div>
        </div>
        <div class="columns small-12 medium-6 large-6">
          <input onclick="validar()" class="btn_comprar rg-btn-primary" type="button" name="name" value="Comprar">
        </div>
      </div>
      </div>

      </div>
    <div class="clearfix"></div>
    <div class="clearfix" style="height:100px;">

    </div>


    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js"></script>
    <script src="../dist/sweetalert-dev.js"></script>
    <link rel="stylesheet" href="../dist/sweetalert.css">


<style>
.rg_ocultar{
  display: none;
}
.container{
  padding:20px;
}
@media screen and (max-width: 500px) {
  #rg-lista-top li{
      display:inline;
      text-align: center;
  }
  #rg-lista-top li a:hover{
    background-color: #bf031c;
    color:white;
  }
  #rg-img-logo{
    text-align: center;
  }
}
#rg-lista-top{
  margin-top: 35px;
}
#rg-lista-top li a{
  color: #bf031c;
}
.item{
  float:left !important;
}
.rg_input_precio{
  height:22px;
}
.rg_modelo{
  float:left !important;
}
.rg_precio_total{
      border: 1px solid black;
      width: 150px;
      text-align: center;
      padding: 25px 26px 20px 10px;
}
.btn_comprar{
  margin-top:30px;
}
</style>




    <script>
    var rg_precio_ajax = 0;
    var precio_modelo_seleccionado = 0;
    var precio_unit_seleccionados = 0;
    var modelo_selected_nombre = "";
    var modelo_selected_price  = 0;
      $(document).ready(function(){
          $(document).foundation();
    });
    $("#rg_precio_ajax").text(rg_precio_ajax);
    function validar(){
      var r = document.getElementsByName("modelo")
      var c = -1

      for(var i=0; i < r.length; i++){
        if(r[i].checked) {
          c = i;
          modelo_selected_nombre = $(r[i]).data('nombre');
          modelo_selected_price = $(r[i]).data('precio');
        }
      }
      if (c == -1){
        sweetAlert("Error", "Seleccione modelo", "error");
      }else if(precio_unit_seleccionados == 0){
        sweetAlert("Error", "Sólo ha agregado el producto modelo, por favor ingrese los productos que desea agregarle", "error");

      }else{
        var table_header = "<table id='rg_detalle_table'><tr><th>Descripcion</th><th>Precio</th></tr>";
        var table_content_modelo = "<tr><td>1 "+modelo_selected_nombre+"</td><td>"+parseInt(modelo_selected_price)+"</td></tr>";
        var table_content_unitarios = "";
        var inputs = $(".rg_input_precio");
        for (var i=0; i<inputs.length; i++){
            if(inputs[i].value > 0){
                table_content_unitarios+="<tr><td>"+ parseInt(inputs[i].value) + "  " + $(inputs[i]).data('nombre')+"</td><td>"+ parseInt(inputs[i].value * $(inputs[i]).data('precio')) +"</td></tr>";

            }
        }
        var total = parseInt(precio_modelo_seleccionado) + parseInt(precio_unit_seleccionados);
        var footer = "<br><br><h3>Total : S/."+total+"</h3>";
        var table_cierre = "</table>";
        var table = table_header + table_content_modelo  + table_content_unitarios + table_cierre;
        swal({
  title: "Resumen de compra",
  text: table+footer,
  html: true,
  showCancelButton : true,
  confirmButtonText: "Cancelar",
  cancelButtonText : "Aceptar",
  cancelButtonColor : "#AEDEF4",
  confirmButtonColor : "#b83135",
});
$( ".cancel" ).click(function() {
  // Aceptar
  submitear();
});
      }
    }
    function submitear(){
      var json_articulos = '{"articulos":[';
      var inputs = $(".rg_input_precio");
      for (var i=0; i<inputs.length; i++){
          if(inputs[i].value > 0){
              var id_producto = $(inputs[i]).data('id');
              var cant_producto = parseInt(inputs[i].value);
              json_articulos+='{"idArt":"'+id_producto+'","cantArt":"'+cant_producto+'"},';
          }

      }
      var size = json_articulos.length;
      var cortar = size-3;
      json_articulos = json_articulos.substring(0,cortar);
      json_articulos+='"}]}';
      $("#txt_json").attr("value",json_articulos);
      $("#formulario").submit();
  //      setTimeout(function() {swal("Hecho!", "Se registró el arreglo", "success");},1500);

    }
    function seleccionarModelo(e){
        precio_modelo_seleccionado = $(e).data('precio');
        actualizarTotal();
    }
    function actualizarTotal(){

        var total = parseInt(precio_unit_seleccionados) + parseInt(precio_modelo_seleccionado);
        $("#rg_precio_ajax").text(total);
    }
    function siSeaNumero(e) {
    k = (document.all) ? e.keyCode : e.which;
    if (k==8 || k==0) return true;
    patron = /\d/;
    n = String.fromCharCode(k);
    return patron.test(n);
    }
    function validarStock(e){
      var qSelec =  $(e).val();
      var stock  =  $(e).data('stock');
      if(qSelec > stock){
      if(qSelec == 999){
        swal({
title: "",
text: "Si desea hacer un pedido especial por favor contáctese con nosotros al 4325545. Estaremos gustosos de atenderlo",
imageUrl: "../img/logo.png"
});



$(document.querySelectorAll("[data-itemcontainer='"+$(e).data('producto')+"']")).find(".rg_input_precio").val(stock);
actualizarPrecio();


      }else{
        swal({
title: "",
text: "Cantidad ingresada excede el stock actual",
imageUrl: "../img/logo.png"
});

$(document.querySelectorAll("[data-itemcontainer='"+$(e).data('producto')+"']")).find(".rg_input_precio").val(stock);
actualizarPrecio();
      }



      }else{
        actualizarPrecio();
      }
    }
    function actualizarPrecio(){
      var tmp_total = 0;
      $( ".rg_input_precio" ).each(function( index ) {
          if(!isNaN($(this).val())){
          tmp_total = tmp_total + $(this).val() * $(this).data('precio');

          }
        });
        precio_unit_seleccionados = tmp_total;
        actualizarTotal();
    }
    function mostrarCantidad(e){
      var prod = $(e).data('producto');
      var prod_mostrar = document.querySelectorAll("[data-item='"+prod+"']");
      $(prod_mostrar).show();
    }
    function cancelarProducto(id_prod){
      $(document.querySelectorAll("[data-itemcontainer='"+id_prod+"']")).find(".rg_input_precio").val("0");
      var prod_div = document.querySelectorAll("[data-producto='"+id_prod+"']");
      var input_canceled = $(prod_div).find("input");
       $(input_canceled).val("0");
      var prod_hide = document.querySelectorAll("[data-item='"+id_prod+"']");
      $(prod_hide).hide();
      actualizarPrecio();
    }

    function grabarArreglo(){
      var inputs = $(".rg_input_precio");
      for (var i=0; i<inputs.length; i++){
          if(inputs[i].value > 0){
              console.log(inputs[i].value +" " +$(inputs[i]).data('nombre'));
          }
      }
    }




    </script>
    <style media="screen">
      .tabs-title a{
            color: #bf3234 !important;
      }
      .tabs-title{
        width:25%;
        text-align: center;
      }
      .rg_seleccionar{
        display: none;
      }
      .rg_item{
        cursor:pointer;
      }
      #rg_detalle_table{
          width:100%;
      }
    </style>
  </body>
</html>
