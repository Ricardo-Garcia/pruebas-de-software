<?php
include ('../src/conexionBD.php');
$sql_last4 = "SELECT * FROM producto WHERE TIPOPRODUCTO = 'ARREGLO' ORDER BY ID_PRODUCTO DESC";
$data_productos = $db->query($sql_last4);
?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Floreria</title>
    <link rel="stylesheet" href="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.min.css">
    <link rel="stylesheet" href="../css/style.css" >
  </head>
  <body>


    <div class="top-bar">
      <div class="top-bar-left">
        <ul id="rg-img-logo" class="menu">
          <img style="width:150px" src="../img/logo.png" alt="">
        </ul>
      </div>
      <div class="top-bar-right">
        <ul id="rg-lista-top" class="menu">
          <li><a href="index.php">INICIO</a></li>
          <li><a href="catalogo.php">CATÁLOGO</a></li>
          <li><a href="realizarDiseno.php">REALIZAR DISEÑO</a></li>
          <li><a href="carrito.php">VER CARRITO</a></li>
        </ul>
      </div>
    </div>


    <div class="row column text-center">
    <br>
      <h2 class="rg-titulo-index">Catálogo</h2>
      <hr>
    </div>

    <div class="row">
    <div class="columns small-12 large-3 medium-">
      <input type="text" id="id_buscador" placeholder="Escriba nombre del producto">
    </div>
    <div class="columns small-12 large-3 medium-">
      <input type="submit" value="Buscar" class="btn_buscar">
      <input onclick="realizarDiseno();" type="button" value="Realizar Diseño" class="btn_buscar">
    </div>

    <div class="clearfix"></div>
      <?php
        while($fila = mysqli_fetch_assoc($data_productos)){
      ?>
        <div class="columns small-12 medium-4 large-4 rg_producto" data-nombre="<?php echo strtoupper($fila['NOMBREPRODUCTO'])?>">
          <span class="rg_cat_nom_prod">
          <?php
          echo $fila['NOMBREPRODUCTO'];
          ?>
          </span>
          <img style="width:360px;height:200px;" src="../img/Productos/<?php echo $fila['IMAGEN']?>">
          <br>
          <span class="rg_cat_precio">s/. <?php echo $fila['PRECIOPRODUCTO']?> </span>
          <br>
          <a class="rg_btn_ver_detalle" href="detalle_producto.php?id=<?php echo $fila['ID_PRODUCTO']?>">Ver detalle</a>

        </div>
        <?php
      }
        ?>


    </div>

    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js"></script>

<script>
  $( "#id_buscador" ).keyup(function() {
      var text = $("#id_buscador").val();
      text = text.toUpperCase();
      if(text == ''){
        $(".rg_ocultar").removeClass("rg_ocultar");
      }else{
      $(".rg_producto").addClass("rg_ocultar");
      $('*[data-nombre^='+text+']').removeClass("rg_ocultar");
      }

  });
  function realizarDiseno(){
    location.replace('realizarDiseno.php');
  }
</script>

<style>
.rg_ocultar{
  display: none;
}
@media screen and (max-width: 500px) {
  #rg-lista-top li{
      display:inline;
      text-align: center;
  }
  #rg-lista-top li a:hover{
    background-color: #bf031c;
    color:white;
  }
  #rg-img-logo{
    text-align: center;
  }
}
#rg-lista-top{
  margin-top: 35px;
}
#rg-lista-top li a{
  color: #bf031c;
}
</style>




    <script>
      $(document).foundation();
    </script>
  </body>
</html>
