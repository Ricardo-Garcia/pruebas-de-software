<?php
if(!isset($_SESSION)){
session_start();
}
require('../fpdf.php');
include '../src/conexionBD.php';
$sql_pedido = "SELECT * FROM pedido tp, detalle_pedido tdp, producto tpro
WHERE tp.ID_PEDIDO = tdp.ID_PEDIDO
AND tpro.ID_PRODUCTO = tdp.ID_PRODUCTO
AND tp.ID_PEDIDO = ".$_GET['id'];
$data = $db->query($sql_pedido);

class PDF extends FPDF
{
// Page header
function Header()
{
    // Logo
    $this->Image('../img/logo.png',10,6,30);
    // Arial bold 15
    $this->SetFont('Arial','B',13);
    // Move to the right
    $this->Cell(63);
    // Title
    $this->Cell(30,10,'MARYS FLORERIA S.A.C',0,0,'C');
    $this->Cell(300,10,'R.U.C 20549662961',0,0,'C');

    // Line break
    $this->Ln(8);
    $this->Cell(70);
    $this->SetFont('Arial','B',10);
    $this->Cell(30,10,'Av. La Fontana Nro. 1295 Urb. La Fontana',0,0,'C');
    $this->Cell(280,10,'BOLETA DE VENTA ELECTRONICA',0,0,'C');
    $this->Ln(8);
    $this->Cell(55);
    $this->Cell(30,10,'Lima - Lima - La Molina',0,0,'C');

    include '../src/conexionBD.php';
    $sql_comprobante  = "SELECT CONCAT(CONCAT(SERIECOMP,'-'),NUMCOMP) AS codigo FROM comprobante WHERE ID_PEDIDO = ".$_GET['id'];
    $data_comprobante = $db->query($sql_comprobante);
    while($fila = mysqli_fetch_assoc($data_comprobante)){
        $codigo = $fila['codigo'];
    }

    $this->Cell(315,10,$codigo,0,0,'C');


    $this->Ln(5);
}

// Page footer
function Footer()
{
    // Position at 1.5 cm from bottom
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Page number
    $this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
}


function FancyTable($header, $data)
{
    // Colores, ancho de línea y fuente en negrita
    $this->SetFillColor(207,50,55);
    $this->SetTextColor(255);
    $this->SetDrawColor(0,0,0);
    $this->SetLineWidth(.3);

    $this->SetFont('Arial','B',10);
    // Cabecera
    $w = array(20,70,25,28,28,35,25,25,30);
    for($i=0;$i<count($header);$i++)
        $this->Cell($w[$i],7,$header[$i],1,0,'C',true);
    $this->Ln();
    // Restauración de colores y fuentes
    $this->SetFillColor(255,151,154);
    $this->SetTextColor(0);
    $this->SetFont('');
    // Datos
    $fill = false;
    $contador = 0;
    $monto_total = 0;

    $igv = $monto_total * 0.18;
    $sub_total = $monto_total - $igv;


        while($fila = mysqli_fetch_assoc($data)){
          $monto_total = $fila['MONTOTOTAL'];
          $this->SetFont('Arial','',10);
          $this->Cell($w[0],6,$fila["ID_PRODUCTO"],'LR',0,'L',$fill);
          $this->Cell($w[1],6,$fila['NOMBREPRODUCTO'],'LR',0,'L',$fill);
          $this->Cell($w[2],6,$fila['CANTIDAD'],'LR',0,'R',$fill);
          $this->Cell($w[3],6,round($fila['PRECIOPRODUCTO'] / 1.18,2),'LR',0,'L',$fill);
          $this->Cell($w[4],6,round($fila['DESCUENTO'] / 1.18,2),'LR',0,'L',$fill);
          $this->Cell($w[5],6, round(($fila['PRECIOPRODUCTO'] - $fila['DESCUENTO']) / 1.18,2),'LR',0,'L',$fill);
          $this->Cell($w[6],6,0,'LR',0,'L',$fill);
          $this->Cell($w[7],6, round(( $fila['PRECIOPRODUCTO'] - $fila['DESCUENTO'] ) - ($fila['PRECIOPRODUCTO'] - $fila['DESCUENTO'] ) / 1.18,2)       ,'LR',0,'L',$fill);
          $this->Cell($w[8],6, $fila['PRECIOPRODUCTO'] * $fila['CANTIDAD']    ,'LR',0,'L',$fill);
          $this->Ln();
          $fill = !$fill;
        }





        $this->Cell(array_sum($w),0,'','T');
}

}

// Instanciation of inherited class
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage('L');
$pdf->SetFont('Times','B',12);
$pdf->Ln(15);

$sql_cliente = "SELECT * FROM persona tp, documento td
WHERE tp.ID_PERSONA = td.ID_DOCUMENTO
AND tp.ID_PERSONA = ".$_SESSION['id_Persona'];

$data_cliente = $db->query($sql_cliente);
while($cliente_dato = mysqli_fetch_assoc($data_cliente)){
  $cliente = array(
    'nombre'=> $cliente_dato['NOMBRE'] . " " .$cliente_dato['APELLIDOPAT']." ".$cliente_dato['APELLIDOMAT'],
    'doc'=> $cliente_dato['NUMDOCUMENTO'],
    'direccion'=> $cliente_dato['DIRECCION']
    );

}


$sql_pedido2 = "SELECT * FROM pedido tp, detalle_pedido tdp, producto tpro
WHERE tp.ID_PEDIDO = tdp.ID_PEDIDO
AND tpro.ID_PRODUCTO = tdp.ID_PRODUCTO
AND tp.ID_PEDIDO = ".$_GET['id'];
$data2 = $db->query($sql_pedido2);
while($fila2 = mysqli_fetch_assoc($data2)){
  $txt_dir_entrega = $fila2['DIRECCIONENTREGA'];
}

$pdf->Cell(0,5,'Cliente                      :            '.$cliente['nombre'],0,1);
$pdf->Ln(5);
$pdf->Cell(0,5,'Doc. Identidad         :            '.$cliente['doc'],0,1);
$pdf->Ln(5);
$pdf->Cell(0,5,'Direccion                  :            '.$cliente['direccion'],0,1);
$pdf->Ln(5);
if($txt_dir_entrega){
  $pdf->Cell(0,5,'Entregar en              :            '.$txt_dir_entrega,0,1);
  $pdf->Ln(20);
}

$header = array('Codigo','Descripcion del articulo','Cant / Unit ','Valor vta unit','Dscto Unit','Valor venta total','Delivery','Impuestos','Precio vta unit');
$pdf->FancyTable($header,$data);
$pdf->Ln(20);

include 'NumeroALetra.php';
$convertir = new NumberToLetterConverter();


$sql_pedido = "SELECT * FROM pedido tp, detalle_pedido tdp, producto tpro
WHERE tp.ID_PEDIDO = tdp.ID_PEDIDO
AND tpro.ID_PRODUCTO = tdp.ID_PRODUCTO
AND tp.ID_PEDIDO = ".$_GET['id'];
$data = $db->query($sql_pedido);
while($fila = mysqli_fetch_assoc($data)){
  $monto_total = $fila['MONTOTOTAL'];
}

$pdf->SetFont('Arial','B',13);
$pdf->Cell(0,5,'SON : ' .$convertir->to_word($monto_total,"PEN") ,0,1);
$pdf->Cell(480,10,'VALOR DE VENTA : '.round($monto_total/1.18,2)  ,0,0,'C');
$pdf->Ln(8);
$pdf->Cell(513,10,'IGV : '.round($monto_total - ($monto_total/1.18),2)  ,0,0,'C');
$pdf->Ln(8);
$pdf->Cell(483,10,'IMPORTE TOTAL : '.$monto_total  ,0,0,'C');

$pdf->Output();







?>
