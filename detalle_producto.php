<?php
$id_producto = $_GET['id'];
include ('src/conexionBD.php');
$sql = "SELECT * FROM producto WHERE ID_PRODUCTO = ".$id_producto;
$data_prod = $db->query($sql);
while($fila = mysqli_fetch_assoc($data_prod)){
  $producto = array(
    'nombre'=> $fila['NOMBREPRODUCTO'],
    'precio'=> $fila['PRECIOPRODUCTO'],
    'stock'=> $fila['STOCK'],
    'descuento'=> $fila['DESCUENTO'],
    'descripcion'=> $fila['DESCRIPCION'],
    'imagen'=> $fila['IMAGEN'],
    );

$sql = "SELECT * FROM producto WHERE ID_PRODUCTO != ".$id_producto." ORDER BY RAND() LIMIT 5 ";
$data_random = $db->query($sql);


}
?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Detalle</title>
    <link rel="stylesheet" href="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.min.css">
    <link rel="stylesheet" href="css/style.css">

  </head>
  <body>


   <div class="top-bar">
      <div class="top-bar-left">
        <ul id="rg-img-logo" class="menu">
          <img style="width:150px" src="img/logo.png" alt="">
        </ul>
      </div>
      <div class="top-bar-right">
        <ul id="rg-lista-top" class="menu">
          <li><a href="index.php">INICIO</a></li>
          <li><a href="catalogo.php">CATÁLOGO</a></li>
          <li><a href="login.php">ACCEDER</a></li>
        </ul>
      </div>
    </div>

<br><br>
    <div class="row">
      <div class="medium-6 columns">

        <img class="thumbnail" src="img/Productos/<?php echo $producto['imagen']?>">
      </div>
      <div class="medium-6 large-5 columns">
        <h3><?php echo $producto['nombre']?></h3>
        <p><?php echo utf8_encode($producto['descripcion'])?></p>



        <div class="row">
          <div class="small-3 columns">
            <label for="middle-label" class="middle">Cantidad</label>
          </div>
          <div class="small-9 columns">
            <input type="text" id="middle-label" placeholder="Ingrese la cantidad">
          </div>
        </div>

        <a href="#" class="button large expanded btn_comprar">Agregar al carrito</a>



        </div>
    </div>

<div class="row">
  <h3>Otros productos</h3>
            <div class="row medium-up-3 large-up-5">

            <?php
            while($fila = mysqli_fetch_assoc($data_random)){

            ?>
              <div class="column">
                <img style="height:200px;width:320px" class="thumbnail" src="img/Productos/<?php echo $fila['IMAGEN']?>">
                <h5><?php echo $fila['NOMBREPRODUCTO']?> <small>s/. <?php echo $fila['PRECIOPRODUCTO']?></small></h5>
                <p><?php echo $fila['DESCRIPCION']?></p>
                <a href="detalle_producto.php?id=<?php echo $fila['ID_PRODUCTO'] ?>" class="button hollow tiny expanded">Ver detalle</a>
              </div>
            <?php
              }
            ?>
          </div>
</div>






    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js"></script>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>
