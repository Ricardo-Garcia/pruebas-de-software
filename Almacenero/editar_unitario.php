<?php
if(!isset($_SESSION)){
session_start();
}
$id_Persona = $_SESSION['id_Persona'];
include '../src/conexionBD.php';

$sql = "SELECT * FROM categoria";
$data = $db->query($sql);
$sql = "SELECT * FROM producto WHERE ID_PRODUCTO = ".$_GET['id'];
$data_product = $db->query($sql);

while($fila = mysqli_fetch_assoc($data_product)){
	$producto = array(
		'nombre'=> $fila['NOMBREPRODUCTO'],
		'precio'=> $fila['PRECIOPRODUCTO'],
		'stock'=> $fila['STOCK'],
		'descuento'=> $fila['DESCUENTO'],
		'descripcion'=> $fila['DESCRIPCION'],
		);

}
?>

<!DOCTYPE html>
<html>
			<head>
    			<meta charset="utf-8">
			    <meta http-equiv="x-ua-compatible" content="ie=edge">
    			<meta name="viewport" content="width=device-width, initial-scale=1.0">
    			<title>Mary's Floreria</title>
    			<link rel="stylesheet" href="../css/foundation.css">
    			<link rel="stylesheet" href="../css/app.css" >
    			<link rel="stylesheet" href="../css/style.css" >
			</head>
<body class="rg-body">



	<div class="row fullWidth" style="height:100%">
		<div class="columns small-12 medium-3 large-3 content-left sP">
				<?php
					include('menu.php');
				?>

		</div>
		<div class="columns small-12 medium-9 large-9 content-right sP">
				<div id="saludo-inicio" class="columns small-12 large-8 medium-8" style="padding:20px">
			<span>
			Bienvenido :
			<?php
				echo $id_Persona;
					?>
				</span>
				</div>
				<div id="saludo-inicio" class="columns small-12 large-4 medium-4" style="text-align:right; padding:20px">
				<a id="cerrarSesion" href="../login.php">Cerrar sesion</a>
				</div>
			<div class="rg-container" style="padding:20px;margin-top:80px">
				<h2>Editar artículo:</h2>
					<div class="row">


						<div class="clearfix"></div>
						<div id="rg_container_1" class="container rg_container rg_cont_active">
							<form action="ActualizarArticulo.php?id=<?php echo $_GET['id']?>" method="post" id="rg_form_art" enctype="multipart/form-data">
								<div class="columns small-12 medium-6 large-6">
									<h6>Nombre producto</h6>
									<input id="txt_nombre_art" type="text" name="txt_nombre_art" placeholder="<?php echo $producto['nombre'] ?>" value="<?php echo $producto['nombre'] ?>">
									<span>Precio</span>
									<input type="text" name="txt_precio_art" id="txt_precio_art" placeholder="<?php echo $producto['precio'] ?>" value="<?php echo $producto['precio'] ?>">
									<span>Imagen</span>
									<input type="file" name="txt_img_art" id="txt_img_art" accept="image/*">
									<span>Categoria</span>
									<select name="txt_categoria_Art">
										<?php
										while($fila = mysqli_fetch_assoc($data)){
										?>
										<option value="<?php echo $fila['ID_CATEGORIA']?>"><?php echo $fila['NOMBRECATEGORIA']?></option>
										<?php
										}
										?>
									</select>
								</div>
								<div class="columns small-12 medium-6 large-6">
									<span>Stock</span>
									<input type="text" name="txt_stock_art" id="txt_stock_art" placeholder="<?php echo $producto['stock']?>" value="<?php echo $producto['stock']?>">
									<span>Descuento</span>
									<input type="text" name="txt_desc_art" id="txt_desc_art" placeholder="<?php echo $producto['descuento']?>" value="<?php echo $producto['descuento']?>">
									<span>Descripcion</span>
									<textarea name="txt_descrip_art" id="txt_descrip_art"><?php echo $producto['descripcion']?></textarea>
								</div>
								<div class="clearfix"></div>
								<input class="rg-btn-primary" type="button"  onclick="validarArticulo()" value="Registrar">

							</form>
						</div>


					<!--<input type="button" onclick="validar()" value="Registrar" name="" class="rg-btn-primary">-->
					</div>

			</div>


		</div>

	</div>


<script src="../js/vendor/jquery.js"></script>
<script src="../dist/sweetalert-dev.js"></script>
<link rel="stylesheet" href="../dist/sweetalert.css">

<style type="text/css">
.rg_seccion{
	cursor: pointer;
}
.rg_container{
	display: none;
}
.rg_cont_active{
	display: inherit !important;
}
</style>

<script>
var tipoProducto = 0;
(function($){
$(document).ready(function(){

$('#cssmenu li.active').addClass('open').children('ul').show();
	$('#cssmenu li.has-sub>a').on('click', function(){
		$(this).removeAttr('href');
		var element = $(this).parent('li');
		if (element.hasClass('open')) {
			element.removeClass('open');
			element.find('li').removeClass('open');
			element.find('ul').slideUp(200);
		}
		else {
			element.addClass('open');
			element.children('ul').slideDown(200);
			element.siblings('li').children('ul').slideUp(200);
			element.siblings('li').removeClass('open');
			element.siblings('li').find('li').removeClass('open');
			element.siblings('li').find('ul').slideUp(200);
		}
	});

});
})(jQuery);

function seleccionarTipo(attr,num){
	$('.rg_seccion').removeClass('rg_seccion_ative');
	$(attr).addClass('rg_seccion_ative');
	tipoProducto=num;
	$(".rg_container").removeClass('rg_cont_active');
	$("#rg_container_"+(num+1)).addClass('rg_cont_active');
}

function siSeaNumero(e) {
k = (document.all) ? e.keyCode : e.which;
if (k==8 || k==0) return true;
patron = /\d/;
n = String.fromCharCode(k);
return patron.test(n);
}

	function noSeaNumero(e) {
k = (document.all) ? e.keyCode : e.which;
if (k==8 || k==0) return true;
patron = /\D/;
n = String.fromCharCode(k);
return patron.test(n);
}
function validarArticulo(){
	if(document.getElementById("txt_nombre_art").value == '' ){
		sweetAlert("Error", "Escriba nombre del artículo", "error");
	}else if(document.getElementById("txt_precio_art").value == '' ){
		sweetAlert("Error", "Escriba precio del artículo", "error");
	}else if($("#txt_precio_art").val()<=0){
		sweetAlert("Error", "Ingrese precio válido para artículo (mayor a 0)", "error");
	}else if(document.getElementById("txt_stock_art").value == '' ){
		sweetAlert("Error", "Escriba stock del artículo", "error");
	}else if($("#txt_stock_art").val()<=0){
		sweetAlert("Error", "Ingrese stock válido para artículo (mayor a 0)", "error");
	}else if($("#txt_descrip_art").val().length == 0){
		sweetAlert("Error", "Ingrese descripción del artículo", "error");
	}else if($("#txt_img_art").val() == ""){
		sweetAlert("Error", "Ingrese imagen del artículo", "error");
	}else{
		document.getElementById("rg_form_art").submit();
	}



}



</script>



</body>
</html>
