<?php
if(!isset($_SESSION)){
session_start();
}
date_default_timezone_set('America/Lima');
$id_Persona = $_SESSION['id_Persona'];
include '../src/conexionBD.php';
$sql = "SELECT * FROM orden_compra";
$data = $db->query($sql);
?><!DOCTYPE html>
<html>
			<head>
    			<meta charset="utf-8">
			    <meta http-equiv="x-ua-compatible" content="ie=edge">
    			<meta name="viewport" content="width=device-width, initial-scale=1.0">
    			<title>Mary's Floreria</title>
    			<link rel="stylesheet" href="../css/foundation.css">
    			<link rel="stylesheet" href="../css/app.css" >
    			<link rel="stylesheet" href="../css/style.css" >
    			<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      			rel="stylesheet">
			</head>
<body class="rg-body">



	<div class="row fullWidth" style="height:100%">
		<div class="columns small-12 medium-3 large-3 content-left sP">
				<?php
					include('menu.php');
				?>

		</div>
		<div class="columns small-12 medium-9 large-9 content-right sP">
				<div id="saludo-inicio" class="columns small-12 large-8 medium-8" style="padding:20px">
			<span>

			<?php
				// Persona
					?>
				</span>
				</div>
				<div id="saludo-inicio" class="columns small-12 large-4 medium-4" style="text-align:right; padding:20px">
				<a id="cerrarSesion" href="../logout.php">Cerrar sesion</a>
				</div>
			<div class="rg-container" style="padding:20px;margin-top:80px">


<table class="rg-table">
  <tr>
    <th>Nº de Pedido</th>
    <th>Detalle</th>
    <th>Fecha</th>
    <th>Estado</th>
    <th>Elegir</th>
  </tr>
  <?php
  while($fila = mysqli_fetch_assoc($data)){
  ?>
  	  <tr data-id="<?php echo $fila['ID_ORDENCOMPRA']?>">
				<td>
					<?php echo $fila["ID_ORDENCOMPRA"]; ?>
				</td>
    	<td><div style="width: 360px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;"><?php echo utf8_encode($fila['DETALLEORDEN'])?></div></td>
    	<td><?php echo date("Y-m-d h:i:s",strtotime($fila['FECHAORDEN'])); ?></td>
    	<td><?php echo utf8_encode($fila['ESTADOORDEN'])?></td>
			<td>
				<?php if($fila['ESTADOORDEN'] !== "RECIBIDO" && $fila['ESTADOORDEN'] !== "CANCELADO"): ?>
				<input type="radio" name="orden_compra_row" id="orden_compra_row" value="<?php echo $fila["ID_ORDENCOMPRA"]; ?>">
			<?php endif; ?>
				&nbsp;&nbsp;&nbsp;
			 	<a href="#" onclick="modalDetalle(<?php echo $fila['ID_ORDENCOMPRA']?>);"><i class="material-icons">remove_red_eye</i></a>
			</td>
  </tr>
  <?php
	}
  ?>
	<tr>
		<td colspan="5">
			<button type="button" class="rg-btn-primary" onclick="modalGenerar()">Nueva orden</button>
			<button type="button" class="rg-btn-primary" onclick="modalModificar()">Modificar selec.</button>
		</td>
	</tr>
</table>


<div class="clearfix">

</div>
<!--
<div class="row">
	<div class="columns small-3 medium-3 large-3 ">
			<input class="rg-btn-primary" type="button" name="name" value="Registrar Producto" onclick="registrarProducto()">
	</div>
	<div class="columns small-3 medium-3 large-3 ">
			<input class="rg-btn-primary" type="button" name="name" value="Actualizar Producto" onclick="actualizarProducto()">
	</div>
	<div class="columns small-3 medium-3 large-3 ">
			<input class="rg-btn-primary" type="button" name="name" value="Bloquear Producto" onclick="bloquearProducto()">
	</div>
	<div class="columns small-3 medium-3 large-3 ">
			<input class="rg-btn-primary" type="button" name="name" value="Habilitar Producto" onclick="habilitarProducto()">
	</div>

-->
</div>


			</div>





		</div>
<!--
		<div class="columns small-3 medium-3 large-3 " style="float: right !important;position: fixed;right: -12%;bottom: 0;">
				<input class="rg-btn-primary" type="button" name="name" value="Subir" onclick="subir()">
		</div>
-->

	</div>

	<div id="modal_order_detail" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	  <h2 id="modalTitle">Orden de compra: <span id="mod_id"></span></h2>
	  <p class="lead">Estado: <span id="mod_estado"></span><span style="float: right;">Fecha: <span id="mod_fecha"></span></span></p>
	  <h4>Detalle Orden</h4>
		<table class="rg-table">
			<thead>
				<tr>
					<th>
						Código
					</th>
					<th>
						Producto
					</th>
					<th>
						Cantidad
					</th>
					<th>
						Fecha de pedido
					</th>
					<th>
						Fecha de entrega
					</th>
					<th>
						Proveedor
					</th>
				</tr>
			</thead>
			<tbody id="mod_productos">

			</tbody>
		</table>
		<h4>Datos del proveedor</h4>
		<table>
			<thead>
				<tr>
					<th>
						Razon social
					</th>
					<th>
						Direccion
					</th>
					<th>
						Correo
					</th>
					<th>
						Telefono
					</th>
				</tr>
			</thead>
			<tbody id="mod_proveedor">

			</tbody>
		</table>
	  <button class="rg-btn-primary" style="float: right;" onclick="modal_order_detail.close();">Salir</button>
	</div>

	<div id="modal_order_modify" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
		<h2 id="modalTitle">Actualizar orden de compra</h2>
		<h4>Datos del proveedor</h4>
		<table>
			<thead>
				<tr>
					<th>
						Razon social
					</th>
					<th>
						Direccion
					</th>
					<th>
						Correo
					</th>
					<th>
						Telefono
					</th>
				</tr>
			</thead>
			<tbody id="mom_proveedor">

			</tbody>
		</table>
		<h4>Detalle Orden</h4>
		<table class="rg-table">
			<thead>
				<tr>
					<th>
						Código
					</th>
					<th>
						Producto
					</th>
					<th>
						Cantidad
					</th>
					<th>
						Fecha de pedido
					</th>
					<th>
						Fecha de entrega
					</th>
					<th>
						Proveedor
					</th>
				</tr>
			</thead>
			<tbody id="mom_productos">

			</tbody>
		</table>
		<h4>Detalle: <span id="mom_detail"></span></h4>
		<div class="column">
			<label>ESTADO
				<select id="mom_status">
					<option value="PEDIDO">PEDIDO</option>
					<option value="ENVIADO">ENVIADO</option>
					<option value="RECIBIDO">RECIBIDO</option>
					<option value="CANCELADO">CANCELADO</option>
				</select>
			</label>
		</div>
		<div class="column">
			<button type="button" class="rg-btn-primary" onclick="modificar()">Actualizar</button>
			<button type="button" class="rg-btn-primary" onclick="modal_order_modify.close();">Salir</button>
		</div>
	</div>

	<?php
	$sql = "SELECT * FROM producto WHERE ESTADO = 'A' ORDER BY NOMBREPRODUCTO ASC";
	$data = $db->query($sql);
	?>

	<div id="modal_order_generate" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
		<h2 id="modalTitle">Generar orden de compra</span></h2>
		<div class="row">
	    <div class="large-5 columns">
				<label>Producto
	        <select id="mog_input_producto">
						<option selected dissabled value="">----</option>
						<?php while($prod = mysqli_fetch_assoc($data)){
					  ?>
						<option value="<?php echo $prod['ID_PRODUCTO']; ?>"><?php echo $prod['NOMBREPRODUCTO']; ?></option>
					  <?php
						}
					  ?>
	        </select>
	      </label>
	    </div>
	    <div class="large-5 columns">
	      <label>Cantidad
	        <input type="number" id="mog_input_cantidad" />
	      </label>
	    </div>
	    <div class="large-2 columns">
	      <button type="button" class="button success" onclick="agregarProducto();"><i class="material-icons">add_circle</i></button>
	    </div>
	  </div>


		<br><br>
		<form id="mog_formulario_producto">


		<table class="rg-table" id="mog_oc_table">
			<thead>
				<tr>
					<th>
						Código
					</th>
					<th>
						Nombre
					</th>
					<th>
						Cantidad
					</th>
					<th>
						Accion
					</th>
				</tr>
			</thead>
			<tbody id="mog_productos">

			</tbody>
		</table>
		</form>
		<hr>
		<div class="row">
			<div class="large-12 columns">
				<label>Proveedor
					<select id="mog_input_proveedor">
						<option selected dissabled value="">----</option>
					<?php
					$sql = "SELECT proveedor.ID_PROVEEDOR,empresa.RAZONSOCIAL FROM proveedor,empresa WHERE proveedor.ID_EMPRESA = empresa.ID_EMPRESA AND proveedor.ESTADO = 0";
					$data = $db->query($sql);
					?>
					<?php while($prov = mysqli_fetch_assoc($data)){
					?>
					<option value="<?php echo $prov['ID_PROVEEDOR']; ?>"><?php echo $prov['RAZONSOCIAL']; ?></option>
					<?php
					}
					?>
				</select>
				</label>
				<label>Detalle
					<input type="text" id="mog_input_detalle">
				</label>
			</div>
		</div>
		<div class="row">
			<div class="large-12 columns">
				<button style="float: right;" type="button" class="rg-btn-primary" onclick="modal_order_generate.close()">Salir</button>
				<button style="float: right;" type="button" id="btnGenerarOC" class="rg-btn-primary" onclick="generarOC();";>Generar</button>
			</div>
		</div>
	</div>


<script src="../js/vendor/jquery.js"></script>
<script src="../js/vendor/foundation.min.js"></script>
<script src="../dist/sweetalert-dev.js"></script>
<link rel="stylesheet" href="../dist/sweetalert.css">
<script type="text/javascript">
	var cant_prod = $("tr").size()-1;

function subir(){
	$('html,body').scrollTop(0);
}
	function actualizarProducto(){
		var value = $("input[name=id_producto]:checked").val()

		if(value == null){
			sweetAlert("Error", "Elija el producto que desee actualizar", "error");
		}else{


			var tipo = $('*[data-id="'+value+'"]').data("tipo");
			window.location.replace('editar_'+tipo.toLowerCase()+'.php?id='+value);
		}
	}


	function habilitarProducto(){
		var value = $("input[name=id_producto]:checked").val()
		if(value == null){
			sweetAlert("Error", "Elija el producto que desee habilitar", "error");
		}else{
			var cont = 0;
			while(cont <= cant_prod){
				var id = $("tr").eq(cont).data('id');
				if(id == value){
					var nom = $("tr").eq(cont).data('nombre');
					var est = $("tr").eq(cont).data('estado');
				}
				cont++;
			}


			if(est == 'I'){
				swal({title: '¿Esta seguro?',   text: 'Habilitará el producto : '+nom,   type: 'warning',   showCancelButton: true, cancelButtonText: 'Cancelar',   confirmButtonColor: '#009688',   confirmButtonText: 'Habilitar',   closeOnConfirm: false }, function(){   swal('Hecho!', 'Se habilitó el producto', 'success');
					setTimeout(function() {window.location.replace('HabilitarProducto.php?id='+value);},1500);});
			}else{
				sweetAlert("Error", "Este producto ya se encuentra habilitado", "error");
			}

		}

	}

	function registrarProducto(){
		window.location = "registrar_producto.php";
	}
	function bloquearProducto(){
		var value = $("input[name=id_producto]:checked").val()
		if(value == null){
			sweetAlert("Error", "Elija el producto que desee bloquear", "error");
		}else{
			var cont = 0;
			while(cont <= cant_prod){
				var id = $("tr").eq(cont).data('id');
				if(id == value){
					var nom = $("tr").eq(cont).data('nombre');
					var est = $("tr").eq(cont).data('estado');
				}
				cont++;
			}
			if(est == 'A'){
			swal({title: '¿Esta seguro?',   text: 'Bloqueará el producto : '+nom,   type: 'warning',   showCancelButton: true, cancelButtonText: 'Cancelar',   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Bloquear',   closeOnConfirm: false }, function(){   swal('Hecho!', 'Se bloqueó el producto', 'success');
				setTimeout(function() {window.location.replace('BloquearProducto.php?id='+value);},1500);});
			}else{
				sweetAlert("Error", "Este producto ya se encuentra bloqueado", "error");
			}
				/**
				-- Metodo de modificación del producto value --
				**/
			}
	}
	function buscarProductoEnter(){
        buscarProducto();

	}
	function buscarProducto(){
		var cont = 0;
		var ning = 0;
		var prod = $("#btn_buscar").val().toUpperCase();
		if(prod == ''){
		$(".rg-table tr").show();
		}else{
			while(cont<cant_prod){
				if($(".rg-table tr").eq(++cont).data("nombre").toUpperCase().includes(prod)){
						$(".rg-table tr").eq(cont).show();
				}else{
						$(".rg-table tr").eq(cont).hide();
						ning++;
				}
			}
		}
		if(ning == cont){

		}
	}
</script>
<script>
$(document).foundation();
var modal_order_detail = new Foundation.Reveal($('#modal_order_detail'));
var modal_order_generate = new Foundation.Reveal($('#modal_order_generate'));
var modal_order_modify = new Foundation.Reveal($('#modal_order_modify'));

var mod_id = $("#mod_id");
var mod_estado = $("#mod_estado");
var mod_fecha = $("#mod_fecha");
var mod_productos = $("#mod_productos");
var mod_proveedor = $("#mod_proveedor");
var mog_productos = $("#mog_productos");
var mog_input_producto = $("#mog_input_producto");
var mog_input_cantidad = $("#mog_input_cantidad");
var mog_input_proveedor = $("#mog_input_proveedor");
var mog_input_detalle = $("#mog_input_detalle");


var mom_proveedor = $("#mom_proveedor");
var mom_productos = $("#mom_productos");
var mom_detail = $("#mom_detail");
var mom_status = $("#mom_status");

var btnGenerarOC = $("#btnGenerarOC");

function modalGenerar(){
	/*var id = $("input:radio[name=orden_compra_row]:checked").val();
	if(typeof id !== "undefined"){
		console.log(id);*/
		modal_order_generate.open()
	/*}else{
		swal("Error","No haz seleccionado ningun producto.","error");
	}*/
}

function modalDetalle(id){
	$.post('api/getDetailsOC.php',{id: id},function(data){
		if (typeof data === "string") {
			data = JSON.parse(data);
		}
		mod_id.text(id);
		mod_estado.text(data['ESTADOORDEN']);
		mod_fecha.text(data['FECHAORDEN']);
		var data_table = "";
		for(x in data.productos){
			data_table += "<tr><td>"+data.productos[x].ID_PRODUCTO+"</td><td>"+data.productos[x].NOMBREPRODUCTO+"</td><td>"+data.productos[x].CANTIDAD+"</td><td>"+data.FECHAORDEN+"</td><td>"+data.FECHARECIBIDO+"</td><td>"+data.proveedor.RAZONSOCIAL+"</td></tr>";
		}
		mod_productos.html(data_table);
		var data_provider = "<tr><td>"+data.proveedor.RAZONSOCIAL+"</td><td>"+data.proveedor.DIRECCION+"</td><td>"+data.proveedor.CORREO+"</td><td>"+data.proveedor.TELEFONO+"</td></tr>";
		mod_proveedor.html(data_provider);
		modal_order_detail.open();
	});
};
function agregarProducto(){
	if(mog_input_producto.val() === ""){
		swal("Error","Debes seleccionar un producto","error");
	}else if(mog_input_cantidad.val() === "" || parseInt(mog_input_cantidad.val()) <=0){
		swal("Error","Debes poner una cantidad valida","error");
	}else{
		$.post("api/getProductDetails.php",{id:mog_input_producto.val()},function(data){
			if (typeof data === "string") {
				data = JSON.parse(data);
			}

			var row = "<tr>";
			row += "<td><input type='hidden' name='precio[]' value='"+(parseFloat(data.PRECIOPRODUCTO)*parseInt(mog_input_cantidad.val()))+"'><input type='hidden' name='codigo[]' value='"+mog_input_producto.val()+"'>"+mog_input_producto.val()+"</td>";
			row += "<td>"+data.NOMBREPRODUCTO+"</td>";
			row += "<td><input type='hidden' name='cantidad[]' value='"+mog_input_cantidad.val()+"'>"+mog_input_cantidad.val()+"</td>";
			row += "<td><button onclick='removeRow(this);event.stopPropagation();'><i class='material-icons'>delete</i></button></td>";
			row += "</tr>";
			resetearInputs();
			mog_productos.append(row);

		});
	}


}

function removeRow(domdobj){
	$(domdobj).parent().parent().remove();
}
function resetearInputs(){
	mog_input_producto.prop('selectedIndex', 0);
	mog_input_cantidad.val('a');
}

function generarOC(){
if(mog_productos.html().trim() === ""){
	swal("Error","No haz agregado ningun producto!","error");
	return;
}
if(mog_input_proveedor.val() === ""){
	swal("Error","No haz seleccionado ningun proveedor!","error");
	return;
}
if(mog_input_detalle.val() === ""){
	swal("Error","No haz puesto detalle!","error");
	return;
}

	btnGenerarOC.prop('disabled', true);

	swal({title: "Confirmacion",text:"Quieres generar Orden de Compra?",type: "warning",showCancelButton: true},function(isConfirm){
		if(isConfirm){
			var obj = {};
			obj.data = {};
			obj.data.codigo = $("input[name='codigo[]']").map(function(){return $(this).val();}).get();;
			obj.data.cantidad = $("input[name='cantidad[]']").map(function(){return $(this).val();}).get();

			var precios = $("input[name='precio[]']").map(function(){return $(this).val();}).get();
			obj.precio_final = 0;
			for(y in precios){
				obj.precio_final += parseFloat(precios[y]);
			}

			obj.proveedor = mog_input_proveedor.val();
			obj.detalle = mog_input_detalle.val();
			obj.empleado = <?php echo $_SESSION['id_Persona']; ?>;
			$.post("api/addOC.php",obj,function(data){
				btnGenerarOC.prop('disabled', false);
				resetearInputs();
				modal_order_generate.close();
				swal({title: "Genial!",text: "¡Orden de compra registrada!",type: "success"},function(){location.reload();});
				console.log(data);
			});
		}else{
			btnGenerarOC.prop('disabled', false);
		}
	})
}

var modif_id_hj = null;
function modalModificar(){
	var id = $("input:radio[name=orden_compra_row]:checked").val();
	if(typeof id !== "undefined"){
		$.post('api/getDetailsOC.php',{id: id},function(data){
			if (typeof data === "string") {
				data = JSON.parse(data);
			}
			modif_id_hj = id;
			mom_status.find("option:selected").prop("selected",false).parent().find("option[value="+data['ESTADOORDEN']+"]").prop("selected",true);

			mom_detail.html(data['DETALLEORDEN'])

			var data_table = "";
			for(x in data.productos){
				data_table += "<tr><td>"+data.productos[x].ID_PRODUCTO+"</td><td>"+data.productos[x].NOMBREPRODUCTO+"</td><td>"+data.productos[x].CANTIDAD+"</td><td>"+data.FECHAORDEN+"</td><td>"+data.FECHARECIBIDO+"</td><td>"+data.proveedor.RAZONSOCIAL+"</td></tr>";
			}
			mom_productos.html(data_table);
			var data_provider = "<tr><td>"+data.proveedor.RAZONSOCIAL+"</td><td>"+data.proveedor.DIRECCION+"</td><td>"+data.proveedor.CORREO+"</td><td>"+data.proveedor.TELEFONO+"</td></tr>";
			mom_proveedor.html(data_provider);
			modal_order_modify.open()
		});
	}else{
		swal("Error","No haz seleccionado ningun producto.","error");
	}
}
function modificar(){
	swal({title: "Confirmacion", text: "Quieres cambiar el status a "+mom_status.val(), type: "warning",showCancelButton: true},function(d){
		if(d){
			$.post("api/updateOC.php",{id:modif_id_hj,status:mom_status.val()},function(data){
				swal({title: "Genial!", text: "Status modificado!", type: "success"},function(){location.reload();});
			});
		}
	});

}
(function($){

$(document).ready(function(){

$('#cssmenu li.active').addClass('open').children('ul').show();
	$('#cssmenu li.has-sub>a').on('click', function(){
		$(this).removeAttr('href');
		var element = $(this).parent('li');
		if (element.hasClass('open')) {
			element.removeClass('open');
			element.find('li').removeClass('open');
			element.find('ul').slideUp(200);
		}
		else {
			element.addClass('open');
			element.children('ul').slideDown(200);
			element.siblings('li').children('ul').slideUp(200);
			element.siblings('li').removeClass('open');
			element.siblings('li').find('li').removeClass('open');
			element.siblings('li').find('ul').slideUp(200);
		}
	});

});
})(jQuery);
</script>



</body>
</html>
