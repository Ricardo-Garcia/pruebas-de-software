<?php
if(!isset($_SESSION)){
session_start();
}
if($_GET){
  $id_Arreglo  = $_GET['id'];

}
$id_Persona = $_SESSION['id_Persona'];
include '../src/conexionBD.php';

$sql = "SELECT * FROM categoria";
$data_categoria = $db->query($sql);

$sql_productos = "SELECT * FROM producto WHERE TIPOPRODUCTO = 'Unitario' AND STOCK > 0";
$data_productos = $db->query($sql_productos);



$sql_arreglo = "SELECT * FROM producto WHERE ID_PRODUCTO =".$id_Arreglo;
$data_arreglo = $db->query($sql_arreglo);

while ($fila = mysqli_fetch_assoc($data_arreglo)) {

  $producto = array(
    'NOMBREPRODUCTO'=> $fila['NOMBREPRODUCTO'],
    'PRECIOPRODUCTO'=> $fila['PRECIOPRODUCTO'],
    'STOCK'=> $fila['STOCK'],
    'DESCRIPCION'=> $fila['DESCRIPCION'],
    'DESCUENTO'=> $fila['DESCUENTO'],
    );
}


$sql_articulos = "select  * from producto_articulo tpa , producto tp WHERE ID_PRODUCTOPADRE = ".$id_Arreglo."  AND tpa.ID_PRODUCTOPADRE = tp.ID_PRODUCTO";
$data_articulos = $db->query($sql_articulos);

?>

<!DOCTYPE html>
<html>
			<head>
    			<meta charset="utf-8">
			    <meta http-equiv="x-ua-compatible" content="ie=edge">
    			<meta name="viewport" content="width=device-width, initial-scale=1.0">
    			<title>Mary's Floreria</title>
    			<link rel="stylesheet" href="../css/foundation.css">
    			<link rel="stylesheet" href="../css/app.css" >
    			<link rel="stylesheet" href="../css/style.css" >
					<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
			</head>
<body class="rg-body">



	<div class="row fullWidth" style="height:100%">
		<div class="columns small-12 medium-3 large-3 content-left sP">
				<?php
					include('menu.php');
				?>

		</div>
		<div class="columns small-12 medium-9 large-9 content-right sP">
				<div id="saludo-inicio" class="columns small-12 large-8 medium-8" style="padding:20px">
			<span>
			Bienvenido :
			<?php
				echo $id_Persona;
					?>
				</span>
				</div>
				<div id="saludo-inicio" class="columns small-12 large-4 medium-4" style="text-align:right; padding:20px">
				<a id="cerrarSesion" href="../login.php">Cerrar sesion</a>
				</div>
			<div class="rg-container" style="padding:20px;margin-top:80px">
				<center>
					<h4>Editar producto</h4>

					<div class="columns small-12 medium-12 large-12">
						<h5>Tipo de producto : </h5>
						<select id="tipoProducto" name="">
								<option value="1">Arreglo</option>
						</select>
					</div>

				</center>
					<div class="row">

						<div class="clearfix"></div>

						<div id="rg_container_2" class="container rg_container" style="display:block !important" >
							<form id="rg_form_arr" action="RegistrarArreglo.php" method="post" enctype="multipart/form-data" style="display:block !important">
								<input id="txt_json" type="hidden"  name="txt_json" value="">
								<div class="container">
									<div class="columns small-12 medium-12 large-12">
										<h6>Nombre de arreglo</h6>
										<input type="text" id="txt_nom_arreglo" name="txt_nom_arreglo" placeholder="Ingrese nombre del arreglo" value="<?php echo $producto['NOMBREPRODUCTO']?>">
										<h6>Categoría</h6>
										<select class="rg_select" name="txt_categoria_Arr">
												<?php while ($res = mysqli_fetch_assoc($data_categoria)) { ?>
													<?php if($res['NOMBRECATEGORIA'] != 'Unitario'){ ?>
												<option value="<?php echo $res['ID_CATEGORIA']?>"><?php echo $res['NOMBRECATEGORIA'] ?></option>
													<?php } ?>
												<?php } ?>
										</select>

										<h6>Imagen</h6>
										<input type="file" id="txt_img_arreglo" name="txt_img_arreglo" class="rg-btn-primary">

										<h6>Precio</h6>
										<input type="text" id="txt_precio_arreglo" name="txt_precio_arreglo" placeholder="Ingrese precio" value="<?php echo $producto['PRECIOPRODUCTO']?>" onkeypress="return siSeaNumero(event);">


										<h6>Stock</h6>
										<input value="<?php echo $producto['STOCK'] ?>" type="text" id="txt_stock_arreglo" name="txt_stock_arreglo" value="1" placeholder="Ingrese stock del arreglo" onkeypress="return siSeaNumero(event);">
										<h6>Descuento</h6>
										<input  value="<?php echo $producto['DESCUENTO'] ?>" type="text" id="txt_desc_arreglo" name="txt_desc_arreglo" placeholder="Opcional" onkeypress="return siSeaNumero(event);">
										<h6>Descripcion</h6>
										<textarea name="txt_descrip_arr" id="txt_descrip_arr"><?php echo $producto['DESCRIPCION'] ?></textarea>


										<h4>Detalles Arreglo:</h4>


										<button data-reveal-id="buscarArticulo" onclick="buscarArticulo.open()" type="button" class="rg-btn-primary">Buscar artículo</button>
										<table class="rg-table" style="margin-top:15px">


											<div id="buscarArticulo" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
												<a onclick="buscarArticulo.close()" class="close-reveal-modal" aria-label="Close">&#215;</a>
											  <h2 id="modalTitle">Buscar artículo</h2>
													<div  class="row">
														<div class="columns small-12 medium-8 large-8" >
															<h6>Ingrese nombre del artículo</h6>
													  	<input id="txt_busc_text" type="text" value="" placeholder="Ingrese nombre de artículo" >
														</div>
														<div class="columns small-12 medium-4 large-4" >
															<input type="button" name="name" value="Buscar" class="rg-btn-primary" onclick="buscarProd()">
														</div>
													</div>
													<div style="background-color:white">
													<?php while ($res = mysqli_fetch_assoc($data_productos)) { ?>
														<center onclick="elegirArticulo(this)" style="background-color:white;height:400px;float:left;cursor:pointer" data-id="<?php echo $res['ID_PRODUCTO']?>" data-stock="<?php echo $res['STOCK']?>"  data-nombre = "<?php echo $res['NOMBREPRODUCTO'] ?>" class="rg_item columns small-6 medium-4 large-4">
																<h4><?php echo $res['NOMBREPRODUCTO'] ?></h4>
																<img src="../img/Productos/<?php echo $res['IMAGEN'] ?>" alt="<?php echo $res['NOMBREPRODUCTO']?>" />
														</center>
													<?php } ?>
													</div>

											</div>


											<style>
												#buscarArticulo{
													background-color: white !important;
												}
											</style>



  <tr>
    <th>Código Artículo</th>
    <th>Nombre Artículo</th>
    <th>Stock Arreglo</th>
    <th>Cantidad Artículo</th>
    <th>Total Artículo</th>
    <th>Quitar</th>
  </tr>


  <?php
  while ($fila = mysqli_fetch_assoc($data_articulos)) {


   ?>
<tr>

  <td style="text-align:center"> <?php  echo $fila['ID_PRODUCTOHIJO']  ?> </td>
    <td style="text-align:center">
      <?php $sql_tmp = "SELECT NOMBREPRODUCTO FROM producto WHERE ID_PRODUCTO = ".$fila['ID_PRODUCTOHIJO'];
      $data_tmp = $db->query($sql_tmp);
        while ($fila2 = mysqli_fetch_assoc($data_tmp)) {
            echo $fila2['NOMBREPRODUCTO'];
        }
       ?>
     </td>
  <td style="text-align:center"> <?php echo $producto['STOCK'] ?> </td>
    <td style="text-align:center"> <?php  echo $fila['CANTIDADARTICULO']  ?> </td>
      <td style="text-align:center"> <?php  echo $fila['CANTIDADARTICULO'] * $producto['STOCK']   ?> </td>
        <td style="text-align:center"> <?php  ?> </td>
</tr>

<?php

}
 ?>

</table>
<br>
<input class="rg-btn-primary" onclick="validarArreglo()" type="button" name="name" value="Actualizar">


									</div>

								</div>
							</form>

						</div>

					<!--<input type="button" onclick="validar()" value="Registrar" name="" class="rg-btn-primary">-->
					</div>

			</div>


		</div>

	</div>


<script src="../js/vendor/jquery.js"></script>
<script src="../js/vendor/foundation.min.js"></script>
<script src="../dist/sweetalert-dev.js"></script>
<link rel="stylesheet" href="../dist/sweetalert.css">

<style type="text/css">
.rg_seccion{
	cursor: pointer;
}

</style>


<script>
var buscarArticulo = new Foundation.Reveal($('#buscarArticulo'));
var cont_art = 0;
</script>

<script>
  $(document).foundation();
</script>

<script>
var tipoProducto = 0;
var stock_elegido = 0;
(function($){
$(document).ready(function(){

$('#cssmenu li.active').addClass('open').children('ul').show();
	$('#cssmenu li.has-sub>a').on('click', function(){
		$(this).removeAttr('href');
		var element = $(this).parent('li');
		if (element.hasClass('open')) {
			element.removeClass('open');
			element.find('li').removeClass('open');
			element.find('ul').slideUp(200);
		}
		else {
			element.addClass('open');
			element.children('ul').slideDown(200);
			element.siblings('li').children('ul').slideUp(200);
			element.siblings('li').removeClass('open');
			element.siblings('li').find('li').removeClass('open');
			element.siblings('li').find('ul').slideUp(200);
		}
	});

});
})(jQuery);

function seleccionarTipo(attr,num){
	$('.rg_seccion').removeClass('rg_seccion_ative');
	$(attr).addClass('rg_seccion_ative');
	tipoProducto=num;
	$(".rg_container").removeClass('rg_cont_active');
	$("#rg_container_"+(num+1)).addClass('rg_cont_active');
}


function cambiarTipo(e){
		if($("#tipoProducto").val() == 0){
				$("form").first().show();
				$("form").last().hide();
		}else{
			$("form").last().show();
			$("form").first().hide();
		}
}

function siSeaNumero(e) {
k = (document.all) ? e.keyCode : e.which;
if (k==8 || k==0) return true;
patron = /\d/;
n = String.fromCharCode(k);
return patron.test(n);
}

	function noSeaNumero(e) {
k = (document.all) ? e.keyCode : e.which;
if (k==8 || k==0) return true;
patron = /\D/;
n = String.fromCharCode(k);
return patron.test(n);
}
function validarArticulo(){
	if(document.getElementById("txt_nombre_art").value == '' ){
		sweetAlert("Error", "Escriba nombre del artículo", "error");
	}else if(document.getElementById("txt_precio_art").value == '' ){
		sweetAlert("Error", "Escriba precio del artículo", "error");
	}else if($("#txt_precio_art").val()<=0){
		sweetAlert("Error", "Ingrese precio válido para artículo (mayor a 0)", "error");
	}else if(document.getElementById("txt_stock_art").value == '' ){
		sweetAlert("Error", "Escriba stock del artículo", "error");
	}else if($("#txt_stock_art").val()<=0){
		sweetAlert("Error", "Ingrese stock válido para artículo (mayor a 0)", "error");
	}else if($("#txt_descrip_art").val().length == 0){
		sweetAlert("Error", "Ingrese descripción del artículo", "error");
	}else if($("#txt_img_art").val() == ""){
		sweetAlert("Error", "Ingrese imagen del artículo", "error");
	}else{
		$.post('VerificarProducto.php', {txt_nom_prod: $("#txt_nombre_art").val().toUpperCase().trim()}, function(data, textStatus, xhr) {
							json_resultado = JSON.parse(data);
							if(json_resultado.resultado == 1){
									sweetAlert("Error", "Ya se encuentra registrado este unitario (Elija otro nombre)", "error");
							}else{

									document.getElementById("rg_form_art").submit();
							}

						});
	}
}

function validarArreglo(){

if(document.getElementById("txt_nom_arreglo").value == '' ){
	sweetAlert("Error", "Escriba nombre del arreglo", "error");
}else if(document.getElementById("txt_precio_arreglo").value == '' ){
	sweetAlert("Error", "Escriba precio del arreglo", "error");
}else if($("#txt_precio_arreglo").val()<=0){
	sweetAlert("Error", "Ingrese precio válido para artículo (mayor a 0)", "error");
}else if(document.getElementById("txt_stock_arreglo").value == '' ){
	sweetAlert("Error", "Escriba stock del arreglo", "error");
}else if($("#txt_stock_arreglo").val()<=0){
	sweetAlert("Error", "Ingrese stock válido para arreglo (mayor a 0)", "error");
}else if($("#txt_img_arreglo").val() == ""){
	sweetAlert("Error", "Ingrese imagen del arreglo", "error");
}else if(cont_art < 1){
	sweetAlert("Error", "El arreglo debe tener al menos un artículo para ser registrado", "error");
}else{
	var json_articulos = '{"articulos":[';
		for (var i = 0 ; i < cont_art; i++) {
				var idArt   = $(".rg_art_selected").eq(i).data("idart");
				var cantArt = $(".rg_art_selected").eq(i).data("cantart");
				if(i+1 != cont_art){
				json_articulos+='{"idArt":"'+idArt+'","cantArt":"'+cantArt+'"},';
				}else{
				json_articulos+='{"idArt":"'+idArt+'","cantArt":"'+cantArt+'"}';
				}
		}
		json_articulos+="]}";
		$("#txt_json").attr("value",json_articulos);


		$.post('VerificarProducto.php', {txt_nom_prod: $("#txt_nombre_art").val().toUpperCase().trim()}, function(data, textStatus, xhr) {
							json_resultado = JSON.parse(data);
							if(json_resultado.resultado == 1){
									sweetAlert("Error", "Ya se encuentra registrado este arreglo (Elija otro nombre)", "error");
							}else{

									document.getElementById("rg_form_arr").submit();
							}

						});




	}
}
function eliminarArticulo(art){
	swal({title: '¿Esta seguro?',   text: 'Eliminará el artículo',   type: 'warning',   showCancelButton: true, cancelButtonText: 'Cancelar',   confirmButtonColor: '#af2124',   confirmButtonText: 'Eliminar',   closeOnConfirm: false }, function(){   swal('Hecho!', 'Se eliminó el artículo', 'success');
		setTimeout(function() {
			$('[data-idart="'+art+'"]').remove();
			cont_art--;
		},1500);});
}

function buscarProd(){
		$("#txt_busc_text").val();
}

function elegirArticulo(e){
		stock_elegido = $("#txt_stock_arreglo").val();
		if(stock_elegido == 0){
			$("#txt_stock_arreglo").attr("value","1");
			stock_elegido = 1;
		}
		swal({   title: $(e).data("nombre"),   text: "Escriba la cantidad que usará:",   type: "input",   showCancelButton: true,   closeOnConfirm: false,   animation: "slide-from-top",   inputPlaceholder: "Escriba cantidad" },
		function(inputValue){
			if(inputValue){
			inputValue = inputValue.trim();
			}
			if (inputValue === false) return false;
			if (inputValue === "") {
				     swal.showInputError("Escriba número");
						      return false
			 }else if(inputValue * stock_elegido  > $(e).data("stock")){
				 swal.showInputError("Cantidad excede el stock actual ("+$(e).data('stock')+")");
				 return false;
			 }else if(isNaN(inputValue)){
				 swal.showInputError("Ingrese número válido");
				 return false;
			 }else if(inputValue < 1){
				 swal.showInputError("Cantidad debe ser mayor a 0");
				 return false;
			 }

			 swal("Registro satisfactorio", "Se agregó produto unitario");buscarArticulo.close();agregarArticulo($(e).data("id"),$(e).data("nombre"),stock_elegido,inputValue,0);$('[data-id="'+$(e).data("id")+'"]').hide();cont_art++;document.getElementById("txt_stock_arreglo").disabled = true;});
}
function agregarArticulo(id,nom,stock,cant,total){
		var e = '<tr class="rg_art_selected" data-idart="'+id+'" data-cantart="'+cant+'"  ><td>'+id+'</td><td>'+nom+'</td><td>'+stock+'</td><td>'+cant+'</td><td>'+(stock*cant)+'</td><td><i style="color:#af2124" onclick="eliminarArticulo('+id+')" class="material-icons">delete</i></td></tr>';
		$("table").append(e);
}
</script>
</body>
</html>
