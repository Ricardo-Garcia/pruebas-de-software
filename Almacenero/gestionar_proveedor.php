<?php
if(!isset($_SESSION)){
session_start();
}
$id_Persona = $_SESSION['id_Persona'];
include '../src/conexionBD.php';
$sql = "SELECT * FROM proveedor tpro, empresa temp
WHERE tpro.ID_EMPRESA = temp.ID_EMPRESA AND tpro.ESTADO = '0' ORDER BY RAZONSOCIAL";
$data = $db->query($sql);

?>

<!DOCTYPE html>
<html>
			<head>
    			<meta charset="utf-8">
			    <meta http-equiv="x-ua-compatible" content="ie=edge">
    			<meta name="viewport" content="width=device-width, initial-scale=1.0">
    			<title>Mary's Floreria</title>
    			<link rel="stylesheet" href="../css/foundation.css">
    			<link rel="stylesheet" href="../css/app.css" >
    			<link rel="stylesheet" href="../css/style.css" >
    			<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      			rel="stylesheet">
						<link rel="stylesheet" href="http://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" >
			</head>
<body class="rg-body">



	<div class="row fullWidth" style="height:100%">
		<div class="columns small-12 medium-3 large-3 content-left sP">
				<?php
					include('menu.php');
				?>

		</div>
		<div class="columns small-12 medium-9 large-9 content-right sP">
				<div id="saludo-inicio" class="columns small-12 large-8 medium-8" style="padding:20px">
			<span>

			<?php
				// Persona
					?>
				</span>
				</div>
				<div id="saludo-inicio" class="columns small-12 large-4 medium-4" style="text-align:right; padding:20px">
				<a id="cerrarSesion" href="../logout.php">Cerrar sesion</a>
				</div>
			<div class="rg-container" style="padding:20px;margin-top:80px">

<div class="columns small-12 medium-6 large-6">
<h2>Proveedor:</h2>
</div>
<div class="columns small-12 medium-6 large-6 rg_right">
	<a class="rg_btn_ver_detalle" href="agregar_proveedor.php">Agregar nuevo proveedor</a>
</div>


        <table class="rg-table">
        	<thead>
          <tr style="font-size:0.7em;background-color: #3c3f39;color:white;text-align:center">

            <th>Razon social</th>
            <th>Direccion</th>
        		<th>Fecha de registro</th>
            <th>Tipo de proveedor</th>
        		<th>Teléfono</th>
        		<th>Correo</th>
            <th>Eliminar</th>
            <th>Actualizar</th>
          </tr>
        	</thead>
        	<tfoot>
        		<tr style="font-size:0.7em;display:none">
							<th>Razon social</th>
	            <th>Direccion</th>
	        		<th>Fecha de registro</th>
	            <th>Tipo de proveedor</th>
	        		<th>Teléfono</th>
	        		<th>Correo</th>
	            <th>Eliminar</th>
	            <th>Actualizar</th>
        		</tr>
        	</tfoot>

        	<tbody>
						<?php
						while ($fila = mysqli_fetch_assoc($data)) {
						?>
						<tr style="font-size:0.7em">
							<td><?php echo $fila['RAZONSOCIAL']?></td>
							<td><?php echo $fila['DIRECCION']?></td>
							<td><?php echo $fila['FECHAREGISTRO']?></td>
							<td><?php echo $fila['TIPOPROVEEDOR']?></td>
							<td><?php echo $fila['TELEFONO']?></td>
							<td><?php echo $fila['CORREO']?></td>
							<td><i style="color:#bc3237;cursor:pointer;text-align:center" onclick="eliminar(<?php echo $fila['ID_PROVEEDOR']?>)" class="material-icons">delete</i></td>
							<td><i style="color:#bc3237;cursor:pointer;text-align:center" onclick="editar(<?php echo $fila['ID_PROVEEDOR']?>)" class="material-icons">border_color</i></td>
						</tr>
						<?php
					}
						?>

        </tbody>
        </table>







			</div>





		</div>
<!--
		<div class="columns small-3 medium-3 large-3 " style="float: right !important;position: fixed;right: -12%;bottom: 0;">
				<input class="rg-btn-primary" type="button" name="name" value="Subir" onclick="subir()">
		</div>
-->

	</div>




<script src="../js/vendor/jquery.js"></script>
<script src="../dist/sweetalert-dev.js"></script>
<script src="../js/table.min.js"></script>
<link rel="stylesheet" href="../dist/sweetalert.css">
<script type="text/javascript">
	var cant_prod = $("tr").size()-1;

function subir(){
	$('html,body').scrollTop(0);
}
	function actualizarProducto(){
		var value = $("input[name=id_producto]:checked").val()

		if(value == null){
			sweetAlert("Error", "Elija el producto que desee actualizar", "error");
		}else{


			var tipo = $('*[data-id="'+value+'"]').data("tipo");
			window.location.replace('editar_'+tipo.toLowerCase()+'.php?id='+value);
		}
	}


	function habilitarProducto(){
		var value = $("input[name=id_producto]:checked").val()
		if(value == null){
			sweetAlert("Error", "Elija el producto que desee habilitar", "error");
		}else{
			var cont = 0;
			while(cont <= cant_prod){
				var id = $("tr").eq(cont).data('id');
				if(id == value){
					var nom = $("tr").eq(cont).data('nombre');
					var est = $("tr").eq(cont).data('estado');
				}
				cont++;
			}


			if(est == 'I'){
				swal({title: '¿Esta seguro?',   text: 'Habilitará el producto : '+nom,   type: 'warning',   showCancelButton: true, cancelButtonText: 'Cancelar',   confirmButtonColor: '#009688',   confirmButtonText: 'Habilitar',   closeOnConfirm: false }, function(){   swal('Hecho!', 'Se habilitó el producto', 'success');
					setTimeout(function() {window.location.replace('HabilitarProducto.php?id='+value);},1500);});
			}else{
				sweetAlert("Error", "Este producto ya se encuentra habilitado", "error");
			}

		}

	}

	function registrarProducto(){
		window.location = "registrar_producto.php";
	}
	function bloquearProducto(){
		var value = $("input[name=id_producto]:checked").val()
		if(value == null){
			sweetAlert("Error", "Elija el producto que desee bloquear", "error");
		}else{
			var cont = 0;
			while(cont <= cant_prod){
				var id = $("tr").eq(cont).data('id');
				if(id == value){
					var nom = $("tr").eq(cont).data('nombre');
					var est = $("tr").eq(cont).data('estado');
				}
				cont++;
			}
			if(est == 'A'){
			swal({title: '¿Esta seguro?',   text: 'Bloqueará el producto : '+nom,   type: 'warning',   showCancelButton: true, cancelButtonText: 'Cancelar',   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Bloquear',   closeOnConfirm: false }, function(){   swal('Hecho!', 'Se bloqueó el producto', 'success');
				setTimeout(function() {window.location.replace('BloquearProducto.php?id='+value);},1500);});
			}else{
				sweetAlert("Error", "Este producto ya se encuentra bloqueado", "error");
			}
				/**
				-- Metodo de modificación del producto value --
				**/
			}
	}
	function buscarProductoEnter(r){
		if (r.keyCode == 13) {
        buscarProducto();
    }
	}
	function buscarProducto(){
		var cont = 0;
		var ning = 0;
		var prod = $("#btn_buscar").val().toUpperCase();
		if(prod == ''){
		sweetAlert("Error", "Ingresa nombre del producto ", "error");
		}else{
			while(cont<cant_prod){
				if($(".rg-table tr").eq(++cont).data("nombre").toUpperCase().includes(prod)){
						$(".rg-table tr").eq(cont).show();
				}else{
						$(".rg-table tr").eq(cont).hide();
						ning++;
				}
			}
		}
		if(ning == cont){
			sweetAlert("Error", "Su búsqueda no coincide con ningún producto", "error");
			$(".rg-table tr").show();
		}
	}
</script>
<script>
(function($){
$(document).ready(function(){

$('#cssmenu li.active').addClass('open').children('ul').show();
	$('#cssmenu li.has-sub>a').on('click', function(){
		$(this).removeAttr('href');
		var element = $(this).parent('li');
		if (element.hasClass('open')) {
			element.removeClass('open');
			element.find('li').removeClass('open');
			element.find('ul').slideUp(200);
		}
		else {
			element.addClass('open');
			element.children('ul').slideDown(200);
			element.siblings('li').children('ul').slideUp(200);
			element.siblings('li').removeClass('open');
			element.siblings('li').find('li').removeClass('open');
			element.siblings('li').find('ul').slideUp(200);
		}
	});

});
})(jQuery);
</script>

<script>
$(document).ready(function(){
		$('.rg-table').DataTable();
});
</script>

<script type="text/javascript">
	function eliminar(e){
		swal({title: '¿Esta seguro?',   text: 'Eliminará al proovedor seleccionado',   type: 'warning',   showCancelButton: true, cancelButtonText: 'Cancelar',   confirmButtonColor: '#009688',   confirmButtonText: 'Eliminar',   closeOnConfirm: false }, function(){   swal('Hecho!', 'Se eliminó el proovedor', 'success');
			setTimeout(function() {window.location.replace('EliminarProveedor.php?id='+e);},1500);});
	}
	function editar(e){
			window.location.replace('editar_proveedor.php?id='+e);
	}
</script>

<style>
label{
color: #af2124;
font-size: 1.5em;}
}

.dataTables_info{
color: #af2124 !important;
font-size: 1.5em !important;
}
</style>






</body>
</html>
