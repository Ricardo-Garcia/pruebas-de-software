<?php
if(!isset($_SESSION)){
session_start();
}
$id_Persona = $_SESSION['id_Persona'];
include '../src/conexionBD.php';
$sql = "SELECT * FROM proveedor tpro, empresa temp
WHERE tpro.ID_EMPRESA = temp.ID_EMPRESA AND tpro.ID_PROVEEDOR = ".$_GET['id']."  ORDER BY RAZONSOCIAL";
$data = $db->query($sql);

while($fila = mysqli_fetch_assoc($data)){
  $proveedor = array(
    'nombre'=> $fila['RAZONSOCIAL'],
    'direccion'=> $fila['DIRECCION'],
    'tipo'=> $fila['TIPOPROVEEDOR'],
    'correo'=> $fila['CORREO'],
    'numero'=> $fila['TELEFONO']
    );
}
?>

<!DOCTYPE html>
<html>
			<head>
    			<meta charset="utf-8">
			    <meta http-equiv="x-ua-compatible" content="ie=edge">
    			<meta name="viewport" content="width=device-width, initial-scale=1.0">
    			<title>Mary's Floreria</title>
    			<link rel="stylesheet" href="../css/foundation.css">
    			<link rel="stylesheet" href="../css/app.css" >
    			<link rel="stylesheet" href="../css/style.css" >
    			<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      			rel="stylesheet">
			</head>
<body class="rg-body">



	<div class="row fullWidth" style="height:100%">
		<div class="columns small-12 medium-3 large-3 content-left sP">
				<?php
					include('menu.php');
				?>

		</div>
		<div class="columns small-12 medium-9 large-9 content-right sP">
				<div id="saludo-inicio" class="columns small-12 large-8 medium-8" style="padding:20px">
			<span>

			<?php
				// Persona
					?>
				</span>
				</div>
				<div id="saludo-inicio" class="columns small-12 large-4 medium-4" style="text-align:right; padding:20px">
				<a id="cerrarSesion" href="../logout.php">Cerrar sesion</a>
				</div>
			<div class="rg-container" style="padding:20px;margin-top:80px">


        <form id="formulario" class="" action="EditarProveedor.php" method="post">
          <input type="hidden" name="id_proveedor" value="<?php echo $_GET['id']?>">
          <h4>Razón social : </h4>
          <input type="text" name="txt_razon_social" id="txt_razon_social" value="<?php echo $proveedor['nombre']?>">
          <h4>Dirección :</h4>
          <input type="text" name="txt_direccion" id="txt_direccion" value="<?php echo $proveedor['direccion']?>">
          <h4>Tipo de Proveedor : </h4>
          <input type="text" name="txt_tipo_proveedor" id="txt_tipo_proveedor" value="<?php echo $proveedor['tipo']?>" onkeypress="return noSeaNumero(event);">
          <h4>Teléfono :</h4>
          <input type="text" name="txt_telefono" id="txt_telefono" value="<?php echo $proveedor['numero']?>" onkeypress="return siSeaNumero(event);">
          <h4>Correo :</h4>
          <input type="text" name="txt_correo" id="txt_correo" value="<?php echo $proveedor['correo']?>">
          <input class="rg_btn_ver_detalle" type="button" onclick="validar()" name="name" value="Guardar">
          <a class="rg_btn_ver_detalle" href="gestionar_proveedor.php">Cancelar</a>
        </form>


	     </div>




<script src="../js/vendor/jquery.js"></script>
<script src="../dist/sweetalert-dev.js"></script>
<link rel="stylesheet" href="../dist/sweetalert.css">
<script type="text/javascript">
	var cant_prod = $("tr").size()-1;



function subir(){
	$('html,body').scrollTop(0);
}
	function actualizarProducto(){
		var value = $("input[name=id_producto]:checked").val()

		if(value == null){
			sweetAlert("Error", "Elija el producto que desee actualizar", "error");
		}else{


			var tipo = $('*[data-id="'+value+'"]').data("tipo");
			window.location.replace('editar_'+tipo.toLowerCase()+'.php?id='+value);
		}
	}


	function habilitarProducto(){
		var value = $("input[name=id_producto]:checked").val()
		if(value == null){
			sweetAlert("Error", "Elija el producto que desee habilitar", "error");
		}else{
			var cont = 0;
			while(cont <= cant_prod){
				var id = $("tr").eq(cont).data('id');
				if(id == value){
					var nom = $("tr").eq(cont).data('nombre');
					var est = $("tr").eq(cont).data('estado');
				}
				cont++;
			}


			if(est == 'I'){
				swal({title: '¿Esta seguro?',   text: 'Habilitará el producto : '+nom,   type: 'warning',   showCancelButton: true, cancelButtonText: 'Cancelar',   confirmButtonColor: '#009688',   confirmButtonText: 'Habilitar',   closeOnConfirm: false }, function(){   swal('Hecho!', 'Se habilitó el producto', 'success');
					setTimeout(function() {window.location.replace('HabilitarProducto.php?id='+value);},1500);});
			}else{
				sweetAlert("Error", "Este producto ya se encuentra habilitado", "error");
			}

		}

	}

	function registrarProducto(){
		window.location = "registrar_producto.php";
	}
	function bloquearProducto(){
		var value = $("input[name=id_producto]:checked").val()
		if(value == null){
			sweetAlert("Error", "Elija el producto que desee bloquear", "error");
		}else{
			var cont = 0;
			while(cont <= cant_prod){
				var id = $("tr").eq(cont).data('id');
				if(id == value){
					var nom = $("tr").eq(cont).data('nombre');
					var est = $("tr").eq(cont).data('estado');
				}
				cont++;
			}
			if(est == 'A'){
			swal({title: '¿Esta seguro?',   text: 'Bloqueará el producto : '+nom,   type: 'warning',   showCancelButton: true, cancelButtonText: 'Cancelar',   confirmButtonColor: '#DD6B55',   confirmButtonText: 'Bloquear',   closeOnConfirm: false }, function(){   swal('Hecho!', 'Se bloqueó el producto', 'success');
				setTimeout(function() {window.location.replace('BloquearProducto.php?id='+value);},1500);});
			}else{
				sweetAlert("Error", "Este producto ya se encuentra bloqueado", "error");
			}
				/**
				-- Metodo de modificación del producto value --
				**/
			}
	}
	function buscarProductoEnter(r){
		if (r.keyCode == 13) {
        buscarProducto();
    }
	}
	function buscarProducto(){
		var cont = 0;
		var ning = 0;
		var prod = $("#btn_buscar").val().toUpperCase();
		if(prod == ''){
		sweetAlert("Error", "Ingresa nombre del producto ", "error");
		}else{
			while(cont<cant_prod){
				if($(".rg-table tr").eq(++cont).data("nombre").toUpperCase().includes(prod)){
						$(".rg-table tr").eq(cont).show();
				}else{
						$(".rg-table tr").eq(cont).hide();
						ning++;
				}
			}
		}
		if(ning == cont){
			sweetAlert("Error", "Su búsqueda no coincide con ningún producto", "error");
			$(".rg-table tr").show();
		}
	}
</script>
<script>
(function($){
$(document).ready(function(){

$('#cssmenu li.active').addClass('open').children('ul').show();
	$('#cssmenu li.has-sub>a').on('click', function(){
		$(this).removeAttr('href');
		var element = $(this).parent('li');
		if (element.hasClass('open')) {
			element.removeClass('open');
			element.find('li').removeClass('open');
			element.find('ul').slideUp(200);
		}
		else {
			element.addClass('open');
			element.children('ul').slideDown(200);
			element.siblings('li').children('ul').slideUp(200);
			element.siblings('li').removeClass('open');
			element.siblings('li').find('li').removeClass('open');
			element.siblings('li').find('ul').slideUp(200);
		}
	});

});
})(jQuery);
</script>


<script type="text/javascript">
  function validar(){
 var txt_razon_social = $("#txt_razon_social").val();
 var txt_direccion = $("#txt_direccion").val();
 var txt_tipo_proveedor = $("#txt_tipo_proveedor").val();
 var txt_telefono = $("#txt_telefono").val();
 var txt_correo = $("#txt_correo").val();

    if(txt_razon_social == ""){
        sweetAlert("Error", "Escriba razón social del proveedor", "error");
    }else if(txt_direccion == ""){
      sweetAlert("Error", "Escriba dirección del proveedor", "error");
    }else if(txt_tipo_proveedor == ""){
      sweetAlert("Error", "Escriba tipo de proveedor", "error");
    }else if(txt_telefono == ""){
      sweetAlert("Error", "Escriba telefono del proveedor", "error");
    }else if(txt_telefono.length != 7 && txt_telefono.length!= 9){
      sweetAlert("Error", "Escriba teléfono valido", "error");
    }else if(txt_correo == ""){
      sweetAlert("Error", "Escriba correo del proveedor", "error");
    }else if(validarCorreo(txt_correo)){
				document.getElementById("formulario").submit();
    }else{
      sweetAlert("Error", "Escriba correo válido del proveedor", "error");
    }
  }
</script>


<script type="text/javascript">
function siSeaNumero(e) {
k = (document.all) ? e.keyCode : e.which;
if (k==8 || k==0) return true;
patron = /\d/;
n = String.fromCharCode(k);
return patron.test(n);
}

  function noSeaNumero(e) {
k = (document.all) ? e.keyCode : e.which;
if (k==8 || k==0) return true;
patron = /\D/;
n = String.fromCharCode(k);
return patron.test(n);
}

function validarCorreo(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
</script>


</body>
</html>
